{
    "status": true,
    "data": [
        {
            "channels_id": 257,
            "channel_name": "TNN",
            "period": "00:20:01-00:21:00",
            "reach_device": 35,
            "gender": {
                "male": 5,
                "female": 0,
                "none": 30
            },
            "provinces": [
                {
                    "province_code": "10",
                    "province_name": "กรุงเทพมหานคร   ",
                    "latitude": "13.7538189",
                    "longitude": "100.4996349",
                    "reach_device": 23
                },
                {
                    "province_code": "22",
                    "province_name": "จันทบุรี   ",
                    "latitude": "12.6104079",
                    "longitude": "102.1006586",
                    "reach_device": 1
                },
                {
                    "province_code": "26",
                    "province_name": "นครนายก   ",
                    "latitude": "14.2121195",
                    "longitude": "101.199244",
                    "reach_device": 0
                },
                {
                    "province_code": "27",
                    "province_name": "สระแก้ว   ",
                    "latitude": "13.7967562",
                    "longitude": "102.1362962",
                    "reach_device": 0
                },
                {
                    "province_code": "40",
                    "province_name": "ขอนแก่น   ",
                    "latitude": "16.4423433",
                    "longitude": "102.8337918",
                    "reach_device": 0
                },
                {
                    "province_code": "57",
                    "province_name": "เชียงราย   ",
                    "latitude": "19.9203034",
                    "longitude": "99.8117656",
                    "reach_device": 0
                },
                {
                    "province_code": "62",
                    "province_name": "กำแพงเพชร   ",
                    "latitude": "16.5550171",
                    "longitude": "99.5092956",
                    "reach_device": 0
                },
                {
                    "province_code": "74",
                    "province_name": "สมุทรสาคร   ",
                    "latitude": "13.5473841",
                    "longitude": "100.2710567",
                    "reach_device": 0
                },
                {
                    "province_code": "76",
                    "province_name": "เพชรบุรี   ",
                    "latitude": "13.1107004",
                    "longitude": "99.9441363",
                    "reach_device": 0
                },
                {
                    "province_code": "85",
                    "province_name": "ระนอง   ",
                    "latitude": "9.9318504",
                    "longitude": "98.5952741",
                    "reach_device": 0
                },
                {
                    "province_code": "93",
                    "province_name": "พัทลุง   ",
                    "latitude": "7.6169494",
                    "longitude": "100.0706333",
                    "reach_device": 0
                },
                {
                    "province_code": "42",
                    "province_name": "เลย   ",
                    "latitude": "17.4873658",
                    "longitude": "101.7179794",
                    "reach_device": 0
                },
                {
                    "province_code": "48",
                    "province_name": "นครพนม   ",
                    "latitude": "17.4083358",
                    "longitude": "104.7764807",
                    "reach_device": 0
                },
                {
                    "province_code": "65",
                    "province_name": "พิษณุโลก   ",
                    "latitude": "16.8262202",
                    "longitude": "100.2579273",
                    "reach_device": 0
                },
                {
                    "province_code": "14",
                    "province_name": "พระนครศรีอยุธยา   ",
                    "latitude": "14.3418352",
                    "longitude": "100.5679019",
                    "reach_device": 0
                },
                {
                    "province_code": "23",
                    "province_name": "ตราด   ",
                    "latitude": "12.243338",
                    "longitude": "102.5159198",
                    "reach_device": 0
                },
                {
                    "province_code": "33",
                    "province_name": "ศรีสะเกษ   ",
                    "latitude": "15.1202058",
                    "longitude": "104.3193875",
                    "reach_device": 0
                },
                {
                    "province_code": "50",
                    "province_name": "เชียงใหม่   ",
                    "latitude": "18.8372842",
                    "longitude": "98.9691778",
                    "reach_device": 0
                },
                {
                    "province_code": "55",
                    "province_name": "น่าน   ",
                    "latitude": "18.7871571",
                    "longitude": "100.7357454",
                    "reach_device": 0
                },
                {
                    "province_code": "56",
                    "province_name": "พะเยา   ",
                    "latitude": "19.1912606",
                    "longitude": "99.876879",
                    "reach_device": 0
                },
                {
                    "province_code": "71",
                    "province_name": "กาญจนบุรี   ",
                    "latitude": "14.0040947",
                    "longitude": "99.5469059",
                    "reach_device": 0
                },
                {
                    "province_code": "81",
                    "province_name": "กระบี่   ",
                    "latitude": "8.0591343",
                    "longitude": "98.9152295",
                    "reach_device": 0
                },
                {
                    "province_code": "92",
                    "province_name": "ตรัง   ",
                    "latitude": "7.5578483",
                    "longitude": "99.6092894",
                    "reach_device": 0
                },
                {
                    "province_code": "96",
                    "province_name": "นราธิวาส   ",
                    "latitude": "6.4303443",
                    "longitude": "101.799545",
                    "reach_device": 0
                },
                {
                    "province_code": "19",
                    "province_name": "สระบุรี",
                    "latitude": "14.5281393",
                    "longitude": "100.9076211",
                    "reach_device": 1
                },
                {
                    "province_code": "36",
                    "province_name": "ชัยภูมิ   ",
                    "latitude": "15.8080698",
                    "longitude": "102.030778",
                    "reach_device": 0
                },
                {
                    "province_code": "51",
                    "province_name": "ลำพูน   ",
                    "latitude": "18.5788749",
                    "longitude": "99.0047979",
                    "reach_device": 0
                },
                {
                    "province_code": "70",
                    "province_name": "ราชบุรี   ",
                    "latitude": "13.528759",
                    "longitude": "99.8111155",
                    "reach_device": 0
                },
                {
                    "province_code": "94",
                    "province_name": "ปัตตานี   ",
                    "latitude": "6.867817",
                    "longitude": "101.2480108",
                    "reach_device": 0
                },
                {
                    "province_code": "11",
                    "province_name": "สมุทรปราการ   ",
                    "latitude": "13.5973714",
                    "longitude": "100.5944567",
                    "reach_device": 0
                },
                {
                    "province_code": "12",
                    "province_name": "นนทบุรี   ",
                    "latitude": "13.8602685",
                    "longitude": "100.5125561",
                    "reach_device": 6
                },
                {
                    "province_code": "18",
                    "province_name": "ชัยนาท   ",
                    "latitude": "15.1852851",
                    "longitude": "100.1204413",
                    "reach_device": 0
                },
                {
                    "province_code": "24",
                    "province_name": "ฉะเชิงเทรา   ",
                    "latitude": "13.687705",
                    "longitude": "101.0679491",
                    "reach_device": 0
                },
                {
                    "province_code": "34",
                    "province_name": "อุบลราชธานี   ",
                    "latitude": "15.2767697",
                    "longitude": "104.8021057",
                    "reach_device": 0
                },
                {
                    "province_code": "41",
                    "province_name": "อุดรธานี   ",
                    "latitude": "17.4150855",
                    "longitude": "102.7851586",
                    "reach_device": 0
                },
                {
                    "province_code": "43",
                    "province_name": "หนองคาย   ",
                    "latitude": "17.8679759",
                    "longitude": "102.7423901",
                    "reach_device": 0
                },
                {
                    "province_code": "54",
                    "province_name": "แพร่   ",
                    "latitude": "18.1455312",
                    "longitude": "100.1382926",
                    "reach_device": 0
                },
                {
                    "province_code": "63",
                    "province_name": "ตาก   ",
                    "latitude": "16.8842269",
                    "longitude": "99.1231478",
                    "reach_device": 0
                },
                {
                    "province_code": "86",
                    "province_name": "ชุมพร   ",
                    "latitude": "10.5215575",
                    "longitude": "99.1903987",
                    "reach_device": 0
                },
                {
                    "province_code": "16",
                    "province_name": "ลพบุรี   ",
                    "latitude": "14.8006662",
                    "longitude": "100.6493253",
                    "reach_device": 0
                },
                {
                    "province_code": "17",
                    "province_name": "สิงห์บุรี   ",
                    "latitude": "14.8967613",
                    "longitude": "100.408933",
                    "reach_device": 0
                },
                {
                    "province_code": "39",
                    "province_name": "หนองบัวลำภู   ",
                    "latitude": "17.2225598",
                    "longitude": "101.3053906",
                    "reach_device": 0
                },
                {
                    "province_code": "52",
                    "province_name": "ลำปาง   ",
                    "latitude": "18.274355",
                    "longitude": "99.5064793",
                    "reach_device": 0
                },
                {
                    "province_code": "60",
                    "province_name": "นครสวรรค์   ",
                    "latitude": "15.6905327",
                    "longitude": "100.1116717",
                    "reach_device": 0
                },
                {
                    "province_code": "73",
                    "province_name": "นครปฐม   ",
                    "latitude": "13.7667989",
                    "longitude": "100.0564742",
                    "reach_device": 0
                },
                {
                    "province_code": "90",
                    "province_name": "สงขลา   ",
                    "latitude": "7.2052572",
                    "longitude": "100.5946716",
                    "reach_device": 0
                },
                {
                    "province_code": "20",
                    "province_name": "ชลบุรี   ",
                    "latitude": "13.3619454",
                    "longitude": "100.9812561",
                    "reach_device": 1
                },
                {
                    "province_code": "25",
                    "province_name": "ปราจีนบุรี   ",
                    "latitude": "14.1097658",
                    "longitude": "101.3274739",
                    "reach_device": 0
                },
                {
                    "province_code": "30",
                    "province_name": "นครราชสีมา   ",
                    "latitude": "14.9723599",
                    "longitude": "102.0979841",
                    "reach_device": 0
                },
                {
                    "province_code": "31",
                    "province_name": "บุรีรัมย์   ",
                    "latitude": "14.9441768",
                    "longitude": "103.1043868",
                    "reach_device": 0
                },
                {
                    "province_code": "32",
                    "province_name": "สุรินทร์   ",
                    "latitude": "14.8822919",
                    "longitude": "103.4900911",
                    "reach_device": 0
                },
                {
                    "province_code": "45",
                    "province_name": "ร้อยเอ็ด   ",
                    "latitude": "16.0531679",
                    "longitude": "103.6502522",
                    "reach_device": 0
                },
                {
                    "province_code": "64",
                    "province_name": "สุโขทัย   ",
                    "latitude": "17.0057456",
                    "longitude": "99.8242644",
                    "reach_device": 0
                },
                {
                    "province_code": "67",
                    "province_name": "เพชรบูรณ์   ",
                    "latitude": "16.4396398",
                    "longitude": "101.1526536",
                    "reach_device": 0
                },
                {
                    "province_code": "72",
                    "province_name": "สุพรรณบุรี   ",
                    "latitude": "14.5137891",
                    "longitude": "100.1279803",
                    "reach_device": 0
                },
                {
                    "province_code": "75",
                    "province_name": "สมุทรสงคราม   ",
                    "latitude": "13.4129727",
                    "longitude": "99.9994864",
                    "reach_device": 0
                },
                {
                    "province_code": "77",
                    "province_name": "ประจวบคีรีขันธ์   ",
                    "latitude": "11.81216",
                    "longitude": "99.7960062",
                    "reach_device": 0
                },
                {
                    "province_code": "80",
                    "province_name": "นครศรีธรรมราช   ",
                    "latitude": "8.4185082",
                    "longitude": "99.9612938",
                    "reach_device": 0
                },
                {
                    "province_code": "83",
                    "province_name": "ภูเก็ต   ",
                    "latitude": "7.8901141",
                    "longitude": "98.3964434",
                    "reach_device": 0
                },
                {
                    "province_code": "84",
                    "province_name": "สุราษฎร์ธานี   ",
                    "latitude": "9.1323042",
                    "longitude": "99.3300335",
                    "reach_device": 0
                },
                {
                    "province_code": "13",
                    "province_name": "ปทุมธานี   ",
                    "latitude": "14.0207997",
                    "longitude": "100.5228494",
                    "reach_device": 0
                },
                {
                    "province_code": "15",
                    "province_name": "อ่างทอง   ",
                    "latitude": "14.5888452",
                    "longitude": "100.4528061",
                    "reach_device": 0
                },
                {
                    "province_code": "21",
                    "province_name": "ระยอง   ",
                    "latitude": "12.7069119",
                    "longitude": "101.1809187",
                    "reach_device": 0
                },
                {
                    "province_code": "44",
                    "province_name": "มหาสารคาม   ",
                    "latitude": "16.1548626",
                    "longitude": "103.3040615",
                    "reach_device": 0
                },
                {
                    "province_code": "46",
                    "province_name": "กาฬสินธุ์   ",
                    "latitude": "16.4066339",
                    "longitude": "103.4779545",
                    "reach_device": 0
                },
                {
                    "province_code": "47",
                    "province_name": "สกลนคร   ",
                    "latitude": "17.1544652",
                    "longitude": "104.1325606",
                    "reach_device": 0
                },
                {
                    "province_code": "53",
                    "province_name": "อุตรดิตถ์   ",
                    "latitude": "17.6297625",
                    "longitude": "100.0937709",
                    "reach_device": 0
                },
                {
                    "province_code": "66",
                    "province_name": "พิจิตร   ",
                    "latitude": "16.4197423",
                    "longitude": "100.3594536",
                    "reach_device": 0
                },
                {
                    "province_code": "91",
                    "province_name": "สตูล   ",
                    "latitude": "6.6238873",
                    "longitude": "100.0649554",
                    "reach_device": 0
                },
                {
                    "province_code": "95",
                    "province_name": "ยะลา   ",
                    "latitude": "6.5400649",
                    "longitude": "101.2790389",
                    "reach_device": 0
                }
            ],
            "age": [
                {
                    "age": 5,
                    "reach_device": 0
                },
                {
                    "age": 7,
                    "reach_device": 0
                },
                {
                    "age": 8,
                    "reach_device": 0
                },
                {
                    "age": 10,
                    "reach_device": 0
                },
                {
                    "age": 11,
                    "reach_device": 0
                },
                {
                    "age": 12,
                    "reach_device": 0
                },
                {
                    "age": 13,
                    "reach_device": 0
                },
                {
                    "age": 14,
                    "reach_device": 0
                },
                {
                    "age": 15,
                    "reach_device": 0
                },
                {
                    "age": 16,
                    "reach_device": 0
                },
                {
                    "age": 17,
                    "reach_device": 0
                },
                {
                    "age": 18,
                    "reach_device": 0
                },
                {
                    "age": 19,
                    "reach_device": 0
                },
                {
                    "age": 20,
                    "reach_device": 0
                },
                {
                    "age": 21,
                    "reach_device": 0
                },
                {
                    "age": 22,
                    "reach_device": 0
                },
                {
                    "age": 23,
                    "reach_device": 0
                },
                {
                    "age": 24,
                    "reach_device": 0
                },
                {
                    "age": 25,
                    "reach_device": 0
                },
                {
                    "age": 26,
                    "reach_device": 0
                },
                {
                    "age": 27,
                    "reach_device": 0
                },
                {
                    "age": 28,
                    "reach_device": 1
                },
                {
                    "age": 29,
                    "reach_device": 0
                },
                {
                    "age": 30,
                    "reach_device": 0
                },
                {
                    "age": 31,
                    "reach_device": 1
                },
                {
                    "age": 32,
                    "reach_device": 0
                },
                {
                    "age": 33,
                    "reach_device": 0
                },
                {
                    "age": 34,
                    "reach_device": 0
                },
                {
                    "age": 35,
                    "reach_device": 1
                },
                {
                    "age": 36,
                    "reach_device": 0
                },
                {
                    "age": 37,
                    "reach_device": 0
                },
                {
                    "age": 38,
                    "reach_device": 0
                },
                {
                    "age": 39,
                    "reach_device": 0
                },
                {
                    "age": 40,
                    "reach_device": 0
                },
                {
                    "age": 41,
                    "reach_device": 0
                },
                {
                    "age": 42,
                    "reach_device": 1
                },
                {
                    "age": 43,
                    "reach_device": 0
                },
                {
                    "age": 44,
                    "reach_device": 0
                },
                {
                    "age": 45,
                    "reach_device": 0
                },
                {
                    "age": 46,
                    "reach_device": 0
                },
                {
                    "age": 47,
                    "reach_device": 0
                },
                {
                    "age": 48,
                    "reach_device": 0
                },
                {
                    "age": 49,
                    "reach_device": 0
                },
                {
                    "age": 50,
                    "reach_device": 1
                },
                {
                    "age": 51,
                    "reach_device": 0
                },
                {
                    "age": 52,
                    "reach_device": 0
                },
                {
                    "age": 53,
                    "reach_device": 0
                },
                {
                    "age": 54,
                    "reach_device": 0
                },
                {
                    "age": 55,
                    "reach_device": 0
                },
                {
                    "age": 56,
                    "reach_device": 0
                },
                {
                    "age": 57,
                    "reach_device": 0
                },
                {
                    "age": 58,
                    "reach_device": 0
                },
                {
                    "age": 59,
                    "reach_device": 0
                },
                {
                    "age": 60,
                    "reach_device": 0
                },
                {
                    "age": 61,
                    "reach_device": 0
                },
                {
                    "age": 62,
                    "reach_device": 0
                },
                {
                    "age": 63,
                    "reach_device": 0
                },
                {
                    "age": 64,
                    "reach_device": 0
                },
                {
                    "age": 65,
                    "reach_device": 0
                },
                {
                    "age": 66,
                    "reach_device": 0
                },
                {
                    "age": 67,
                    "reach_device": 0
                },
                {
                    "age": 68,
                    "reach_device": 0
                },
                {
                    "age": 69,
                    "reach_device": 0
                },
                {
                    "age": 70,
                    "reach_device": 0
                },
                {
                    "age": 71,
                    "reach_device": 0
                },
                {
                    "age": 72,
                    "reach_device": 0
                },
                {
                    "age": 73,
                    "reach_device": 0
                },
                {
                    "age": 74,
                    "reach_device": 0
                },
                {
                    "age": 78,
                    "reach_device": 0
                },
                {
                    "age": 80,
                    "reach_device": 0
                },
                {
                    "age": 81,
                    "reach_device": 0
                },
                {
                    "age": 89,
                    "reach_device": 0
                },
                {
                    "age": 91,
                    "reach_device": 0
                },
                {
                    "age": 92,
                    "reach_device": 0
                },
                {
                    "age": 95,
                    "reach_device": 0
                },
                {
                    "age": 96,
                    "reach_device": 0
                },
                {
                    "age": 108,
                    "reach_device": 0
                },
                {
                    "age": 111,
                    "reach_device": 0
                },
                {
                    "age": 112,
                    "reach_device": 0
                },
                {
                    "age": 113,
                    "reach_device": 0
                },
                {
                    "age": 6,
                    "reach_device": 0
                },
                {
                    "age": 105,
                    "reach_device": 0
                },
                {
                    "age": "ระบุไม่ได้",
                    "reach_device": 30
                }
            ],
            "total_devices": 35
        },
        {
            "channels_id": 263,
            "channel_name": "Nation",
            "period": "00:20:01-00:21:00",
            "reach_device": 50,
            "gender": {
                "male": 3,
                "female": 4,
                "none": 43
            },
            "provinces": [
                {
                    "province_code": "16",
                    "province_name": "ลพบุรี   ",
                    "latitude": "14.8006662",
                    "longitude": "100.6493253",
                    "reach_device": 3
                },
                {
                    "province_code": "41",
                    "province_name": "อุดรธานี   ",
                    "latitude": "17.4150855",
                    "longitude": "102.7851586",
                    "reach_device": 1
                },
                {
                    "province_code": "44",
                    "province_name": "มหาสารคาม   ",
                    "latitude": "16.1548626",
                    "longitude": "103.3040615",
                    "reach_device": 0
                },
                {
                    "province_code": "50",
                    "province_name": "เชียงใหม่   ",
                    "latitude": "18.8372842",
                    "longitude": "98.9691778",
                    "reach_device": 0
                },
                {
                    "province_code": "52",
                    "province_name": "ลำปาง   ",
                    "latitude": "18.274355",
                    "longitude": "99.5064793",
                    "reach_device": 0
                },
                {
                    "province_code": "53",
                    "province_name": "อุตรดิตถ์   ",
                    "latitude": "17.6297625",
                    "longitude": "100.0937709",
                    "reach_device": 1
                },
                {
                    "province_code": "61",
                    "province_name": "อุทัยธานี   ",
                    "latitude": "15.373362",
                    "longitude": "100.0344366",
                    "reach_device": 0
                },
                {
                    "province_code": "70",
                    "province_name": "ราชบุรี   ",
                    "latitude": "13.528759",
                    "longitude": "99.8111155",
                    "reach_device": 0
                },
                {
                    "province_code": "92",
                    "province_name": "ตรัง   ",
                    "latitude": "7.5578483",
                    "longitude": "99.6092894",
                    "reach_device": 0
                },
                {
                    "province_code": "95",
                    "province_name": "ยะลา   ",
                    "latitude": "6.5400649",
                    "longitude": "101.2790389",
                    "reach_device": 0
                },
                {
                    "province_code": "25",
                    "province_name": "ปราจีนบุรี   ",
                    "latitude": "14.1097658",
                    "longitude": "101.3274739",
                    "reach_device": 0
                },
                {
                    "province_code": "33",
                    "province_name": "ศรีสะเกษ   ",
                    "latitude": "15.1202058",
                    "longitude": "104.3193875",
                    "reach_device": 0
                },
                {
                    "province_code": "56",
                    "province_name": "พะเยา   ",
                    "latitude": "19.1912606",
                    "longitude": "99.876879",
                    "reach_device": 0
                },
                {
                    "province_code": "64",
                    "province_name": "สุโขทัย   ",
                    "latitude": "17.0057456",
                    "longitude": "99.8242644",
                    "reach_device": 0
                },
                {
                    "province_code": "73",
                    "province_name": "นครปฐม   ",
                    "latitude": "13.7667989",
                    "longitude": "100.0564742",
                    "reach_device": 0
                },
                {
                    "province_code": "80",
                    "province_name": "นครศรีธรรมราช   ",
                    "latitude": "8.4185082",
                    "longitude": "99.9612938",
                    "reach_device": 0
                },
                {
                    "province_code": "83",
                    "province_name": "ภูเก็ต   ",
                    "latitude": "7.8901141",
                    "longitude": "98.3964434",
                    "reach_device": 0
                },
                {
                    "province_code": "94",
                    "province_name": "ปัตตานี   ",
                    "latitude": "6.867817",
                    "longitude": "101.2480108",
                    "reach_device": 0
                },
                {
                    "province_code": "96",
                    "province_name": "นราธิวาส   ",
                    "latitude": "6.4303443",
                    "longitude": "101.799545",
                    "reach_device": 0
                },
                {
                    "province_code": "13",
                    "province_name": "ปทุมธานี   ",
                    "latitude": "14.0207997",
                    "longitude": "100.5228494",
                    "reach_device": 0
                },
                {
                    "province_code": "21",
                    "province_name": "ระยอง   ",
                    "latitude": "12.7069119",
                    "longitude": "101.1809187",
                    "reach_device": 0
                },
                {
                    "province_code": "32",
                    "province_name": "สุรินทร์   ",
                    "latitude": "14.8822919",
                    "longitude": "103.4900911",
                    "reach_device": 0
                },
                {
                    "province_code": "54",
                    "province_name": "แพร่   ",
                    "latitude": "18.1455312",
                    "longitude": "100.1382926",
                    "reach_device": 0
                },
                {
                    "province_code": "72",
                    "province_name": "สุพรรณบุรี   ",
                    "latitude": "14.5137891",
                    "longitude": "100.1279803",
                    "reach_device": 0
                },
                {
                    "province_code": "93",
                    "province_name": "พัทลุง   ",
                    "latitude": "7.6169494",
                    "longitude": "100.0706333",
                    "reach_device": 0
                },
                {
                    "province_code": "15",
                    "province_name": "อ่างทอง   ",
                    "latitude": "14.5888452",
                    "longitude": "100.4528061",
                    "reach_device": 0
                },
                {
                    "province_code": "22",
                    "province_name": "จันทบุรี   ",
                    "latitude": "12.6104079",
                    "longitude": "102.1006586",
                    "reach_device": 0
                },
                {
                    "province_code": "30",
                    "province_name": "นครราชสีมา   ",
                    "latitude": "14.9723599",
                    "longitude": "102.0979841",
                    "reach_device": 1
                },
                {
                    "province_code": "31",
                    "province_name": "บุรีรัมย์   ",
                    "latitude": "14.9441768",
                    "longitude": "103.1043868",
                    "reach_device": 0
                },
                {
                    "province_code": "37",
                    "province_name": "อำนาจเจริญ   ",
                    "latitude": "15.9130803",
                    "longitude": "104.6068462",
                    "reach_device": 0
                },
                {
                    "province_code": "39",
                    "province_name": "หนองบัวลำภู   ",
                    "latitude": "17.2225598",
                    "longitude": "101.3053906",
                    "reach_device": 0
                },
                {
                    "province_code": "62",
                    "province_name": "กำแพงเพชร   ",
                    "latitude": "16.5550171",
                    "longitude": "99.5092956",
                    "reach_device": 0
                },
                {
                    "province_code": "67",
                    "province_name": "เพชรบูรณ์   ",
                    "latitude": "16.4396398",
                    "longitude": "101.1526536",
                    "reach_device": 0
                },
                {
                    "province_code": "84",
                    "province_name": "สุราษฎร์ธานี   ",
                    "latitude": "9.1323042",
                    "longitude": "99.3300335",
                    "reach_device": 1
                },
                {
                    "province_code": "91",
                    "province_name": "สตูล   ",
                    "latitude": "6.6238873",
                    "longitude": "100.0649554",
                    "reach_device": 0
                },
                {
                    "province_code": "10",
                    "province_name": "กรุงเทพมหานคร   ",
                    "latitude": "13.7538189",
                    "longitude": "100.4996349",
                    "reach_device": 33
                },
                {
                    "province_code": "23",
                    "province_name": "ตราด   ",
                    "latitude": "12.243338",
                    "longitude": "102.5159198",
                    "reach_device": 0
                },
                {
                    "province_code": "40",
                    "province_name": "ขอนแก่น   ",
                    "latitude": "16.4423433",
                    "longitude": "102.8337918",
                    "reach_device": 0
                },
                {
                    "province_code": "45",
                    "province_name": "ร้อยเอ็ด   ",
                    "latitude": "16.0531679",
                    "longitude": "103.6502522",
                    "reach_device": 0
                },
                {
                    "province_code": "46",
                    "province_name": "กาฬสินธุ์   ",
                    "latitude": "16.4066339",
                    "longitude": "103.4779545",
                    "reach_device": 0
                },
                {
                    "province_code": "48",
                    "province_name": "นครพนม   ",
                    "latitude": "17.4083358",
                    "longitude": "104.7764807",
                    "reach_device": 0
                },
                {
                    "province_code": "49",
                    "province_name": "มุกดาหาร   ",
                    "latitude": "16.5456152",
                    "longitude": "104.7238214",
                    "reach_device": 0
                },
                {
                    "province_code": "55",
                    "province_name": "น่าน   ",
                    "latitude": "18.7871571",
                    "longitude": "100.7357454",
                    "reach_device": 0
                },
                {
                    "province_code": "66",
                    "province_name": "พิจิตร   ",
                    "latitude": "16.4197423",
                    "longitude": "100.3594536",
                    "reach_device": 0
                },
                {
                    "province_code": "77",
                    "province_name": "ประจวบคีรีขันธ์   ",
                    "latitude": "11.81216",
                    "longitude": "99.7960062",
                    "reach_device": 1
                },
                {
                    "province_code": "82",
                    "province_name": "พังงา   ",
                    "latitude": "8.4260761",
                    "longitude": "98.5272303",
                    "reach_device": 0
                },
                {
                    "province_code": "85",
                    "province_name": "ระนอง   ",
                    "latitude": "9.9318504",
                    "longitude": "98.5952741",
                    "reach_device": 0
                },
                {
                    "province_code": "20",
                    "province_name": "ชลบุรี   ",
                    "latitude": "13.3619454",
                    "longitude": "100.9812561",
                    "reach_device": 0
                },
                {
                    "province_code": "42",
                    "province_name": "เลย   ",
                    "latitude": "17.4873658",
                    "longitude": "101.7179794",
                    "reach_device": 0
                },
                {
                    "province_code": "47",
                    "province_name": "สกลนคร   ",
                    "latitude": "17.1544652",
                    "longitude": "104.1325606",
                    "reach_device": 0
                },
                {
                    "province_code": "75",
                    "province_name": "สมุทรสงคราม   ",
                    "latitude": "13.4129727",
                    "longitude": "99.9994864",
                    "reach_device": 0
                },
                {
                    "province_code": "14",
                    "province_name": "พระนครศรีอยุธยา   ",
                    "latitude": "14.3418352",
                    "longitude": "100.5679019",
                    "reach_device": 0
                },
                {
                    "province_code": "17",
                    "province_name": "สิงห์บุรี   ",
                    "latitude": "14.8967613",
                    "longitude": "100.408933",
                    "reach_device": 0
                },
                {
                    "province_code": "18",
                    "province_name": "ชัยนาท   ",
                    "latitude": "15.1852851",
                    "longitude": "100.1204413",
                    "reach_device": 0
                },
                {
                    "province_code": "24",
                    "province_name": "ฉะเชิงเทรา   ",
                    "latitude": "13.687705",
                    "longitude": "101.0679491",
                    "reach_device": 0
                },
                {
                    "province_code": "34",
                    "province_name": "อุบลราชธานี   ",
                    "latitude": "15.2767697",
                    "longitude": "104.8021057",
                    "reach_device": 0
                },
                {
                    "province_code": "35",
                    "province_name": "ยโสธร   ",
                    "latitude": "15.8021972",
                    "longitude": "104.1374875",
                    "reach_device": 0
                },
                {
                    "province_code": "36",
                    "province_name": "ชัยภูมิ   ",
                    "latitude": "15.8080698",
                    "longitude": "102.030778",
                    "reach_device": 0
                },
                {
                    "province_code": "43",
                    "province_name": "หนองคาย   ",
                    "latitude": "17.8679759",
                    "longitude": "102.7423901",
                    "reach_device": 0
                },
                {
                    "province_code": "51",
                    "province_name": "ลำพูน   ",
                    "latitude": "18.5788749",
                    "longitude": "99.0047979",
                    "reach_device": 0
                },
                {
                    "province_code": "60",
                    "province_name": "นครสวรรค์   ",
                    "latitude": "15.6905327",
                    "longitude": "100.1116717",
                    "reach_device": 0
                },
                {
                    "province_code": "63",
                    "province_name": "ตาก   ",
                    "latitude": "16.8842269",
                    "longitude": "99.1231478",
                    "reach_device": 0
                },
                {
                    "province_code": "65",
                    "province_name": "พิษณุโลก   ",
                    "latitude": "16.8262202",
                    "longitude": "100.2579273",
                    "reach_device": 0
                },
                {
                    "province_code": "71",
                    "province_name": "กาญจนบุรี   ",
                    "latitude": "14.0040947",
                    "longitude": "99.5469059",
                    "reach_device": 0
                },
                {
                    "province_code": "86",
                    "province_name": "ชุมพร   ",
                    "latitude": "10.5215575",
                    "longitude": "99.1903987",
                    "reach_device": 0
                },
                {
                    "province_code": "11",
                    "province_name": "สมุทรปราการ   ",
                    "latitude": "13.5973714",
                    "longitude": "100.5944567",
                    "reach_device": 0
                },
                {
                    "province_code": "12",
                    "province_name": "นนทบุรี   ",
                    "latitude": "13.8602685",
                    "longitude": "100.5125561",
                    "reach_device": 3
                },
                {
                    "province_code": "19",
                    "province_name": "สระบุรี",
                    "latitude": "14.5281393",
                    "longitude": "100.9076211",
                    "reach_device": 0
                },
                {
                    "province_code": "26",
                    "province_name": "นครนายก   ",
                    "latitude": "14.2121195",
                    "longitude": "101.199244",
                    "reach_device": 0
                },
                {
                    "province_code": "27",
                    "province_name": "สระแก้ว   ",
                    "latitude": "13.7967562",
                    "longitude": "102.1362962",
                    "reach_device": 0
                },
                {
                    "province_code": "57",
                    "province_name": "เชียงราย   ",
                    "latitude": "19.9203034",
                    "longitude": "99.8117656",
                    "reach_device": 0
                },
                {
                    "province_code": "74",
                    "province_name": "สมุทรสาคร   ",
                    "latitude": "13.5473841",
                    "longitude": "100.2710567",
                    "reach_device": 0
                },
                {
                    "province_code": "76",
                    "province_name": "เพชรบุรี   ",
                    "latitude": "13.1107004",
                    "longitude": "99.9441363",
                    "reach_device": 0
                },
                {
                    "province_code": "81",
                    "province_name": "กระบี่   ",
                    "latitude": "8.0591343",
                    "longitude": "98.9152295",
                    "reach_device": 0
                },
                {
                    "province_code": "90",
                    "province_name": "สงขลา   ",
                    "latitude": "7.2052572",
                    "longitude": "100.5946716",
                    "reach_device": 0
                }
            ],
            "age": [
                {
                    "age": 5,
                    "reach_device": 0
                },
                {
                    "age": 7,
                    "reach_device": 0
                },
                {
                    "age": 8,
                    "reach_device": 0
                },
                {
                    "age": 9,
                    "reach_device": 0
                },
                {
                    "age": 10,
                    "reach_device": 0
                },
                {
                    "age": 11,
                    "reach_device": 0
                },
                {
                    "age": 12,
                    "reach_device": 0
                },
                {
                    "age": 13,
                    "reach_device": 0
                },
                {
                    "age": 14,
                    "reach_device": 0
                },
                {
                    "age": 15,
                    "reach_device": 0
                },
                {
                    "age": 16,
                    "reach_device": 0
                },
                {
                    "age": 17,
                    "reach_device": 0
                },
                {
                    "age": 18,
                    "reach_device": 0
                },
                {
                    "age": 19,
                    "reach_device": 0
                },
                {
                    "age": 20,
                    "reach_device": 0
                },
                {
                    "age": 21,
                    "reach_device": 0
                },
                {
                    "age": 22,
                    "reach_device": 0
                },
                {
                    "age": 23,
                    "reach_device": 1
                },
                {
                    "age": 24,
                    "reach_device": 0
                },
                {
                    "age": 25,
                    "reach_device": 1
                },
                {
                    "age": 26,
                    "reach_device": 1
                },
                {
                    "age": 27,
                    "reach_device": 0
                },
                {
                    "age": 28,
                    "reach_device": 0
                },
                {
                    "age": 29,
                    "reach_device": 0
                },
                {
                    "age": 30,
                    "reach_device": 0
                },
                {
                    "age": 31,
                    "reach_device": 0
                },
                {
                    "age": 32,
                    "reach_device": 0
                },
                {
                    "age": 33,
                    "reach_device": 0
                },
                {
                    "age": 34,
                    "reach_device": 0
                },
                {
                    "age": 35,
                    "reach_device": 0
                },
                {
                    "age": 36,
                    "reach_device": 0
                },
                {
                    "age": 37,
                    "reach_device": 0
                },
                {
                    "age": 38,
                    "reach_device": 0
                },
                {
                    "age": 39,
                    "reach_device": 0
                },
                {
                    "age": 40,
                    "reach_device": 0
                },
                {
                    "age": 41,
                    "reach_device": 2
                },
                {
                    "age": 42,
                    "reach_device": 0
                },
                {
                    "age": 43,
                    "reach_device": 0
                },
                {
                    "age": 44,
                    "reach_device": 0
                },
                {
                    "age": 45,
                    "reach_device": 0
                },
                {
                    "age": 46,
                    "reach_device": 0
                },
                {
                    "age": 47,
                    "reach_device": 0
                },
                {
                    "age": 48,
                    "reach_device": 0
                },
                {
                    "age": 49,
                    "reach_device": 0
                },
                {
                    "age": 50,
                    "reach_device": 0
                },
                {
                    "age": 51,
                    "reach_device": 0
                },
                {
                    "age": 52,
                    "reach_device": 0
                },
                {
                    "age": 53,
                    "reach_device": 0
                },
                {
                    "age": 54,
                    "reach_device": 0
                },
                {
                    "age": 55,
                    "reach_device": 0
                },
                {
                    "age": 56,
                    "reach_device": 0
                },
                {
                    "age": 57,
                    "reach_device": 0
                },
                {
                    "age": 58,
                    "reach_device": 0
                },
                {
                    "age": 59,
                    "reach_device": 0
                },
                {
                    "age": 60,
                    "reach_device": 0
                },
                {
                    "age": 61,
                    "reach_device": 1
                },
                {
                    "age": 62,
                    "reach_device": 0
                },
                {
                    "age": 63,
                    "reach_device": 1
                },
                {
                    "age": 64,
                    "reach_device": 0
                },
                {
                    "age": 65,
                    "reach_device": 0
                },
                {
                    "age": 66,
                    "reach_device": 0
                },
                {
                    "age": 67,
                    "reach_device": 0
                },
                {
                    "age": 68,
                    "reach_device": 0
                },
                {
                    "age": 69,
                    "reach_device": 0
                },
                {
                    "age": 70,
                    "reach_device": 0
                },
                {
                    "age": 71,
                    "reach_device": 0
                },
                {
                    "age": 72,
                    "reach_device": 0
                },
                {
                    "age": 73,
                    "reach_device": 0
                },
                {
                    "age": 74,
                    "reach_device": 0
                },
                {
                    "age": 75,
                    "reach_device": 0
                },
                {
                    "age": 76,
                    "reach_device": 0
                },
                {
                    "age": 78,
                    "reach_device": 0
                },
                {
                    "age": 80,
                    "reach_device": 0
                },
                {
                    "age": 81,
                    "reach_device": 0
                },
                {
                    "age": 88,
                    "reach_device": 0
                },
                {
                    "age": 91,
                    "reach_device": 0
                },
                {
                    "age": 93,
                    "reach_device": 0
                },
                {
                    "age": 94,
                    "reach_device": 0
                },
                {
                    "age": 95,
                    "reach_device": 0
                },
                {
                    "age": 96,
                    "reach_device": 0
                },
                {
                    "age": 100,
                    "reach_device": 0
                },
                {
                    "age": 102,
                    "reach_device": 0
                },
                {
                    "age": 106,
                    "reach_device": 0
                },
                {
                    "age": 107,
                    "reach_device": 0
                },
                {
                    "age": 108,
                    "reach_device": 0
                },
                {
                    "age": 109,
                    "reach_device": 0
                },
                {
                    "age": 112,
                    "reach_device": 0
                },
                {
                    "age": 113,
                    "reach_device": 0
                },
                {
                    "age": 119,
                    "reach_device": 0
                },
                {
                    "age": "ระบุไม่ได้",
                    "reach_device": 43
                }
            ],
            "total_devices": 51
        },
        {
            "channels_id": 271,
            "channel_name": "MCOT",
            "period": "00:20:01-00:21:00",
            "reach_device": 103,
            "gender": {
                "male": 17,
                "female": 7,
                "none": 79
            },
            "provinces": [
                {
                    "province_code": "20",
                    "province_name": "ชลบุรี   ",
                    "latitude": "13.3619454",
                    "longitude": "100.9812561",
                    "reach_device": 0
                },
                {
                    "province_code": "32",
                    "province_name": "สุรินทร์   ",
                    "latitude": "14.8822919",
                    "longitude": "103.4900911",
                    "reach_device": 0
                },
                {
                    "province_code": "42",
                    "province_name": "เลย   ",
                    "latitude": "17.4873658",
                    "longitude": "101.7179794",
                    "reach_device": 0
                },
                {
                    "province_code": "54",
                    "province_name": "แพร่   ",
                    "latitude": "18.1455312",
                    "longitude": "100.1382926",
                    "reach_device": 0
                },
                {
                    "province_code": "72",
                    "province_name": "สุพรรณบุรี   ",
                    "latitude": "14.5137891",
                    "longitude": "100.1279803",
                    "reach_device": 0
                },
                {
                    "province_code": "75",
                    "province_name": "สมุทรสงคราม   ",
                    "latitude": "13.4129727",
                    "longitude": "99.9994864",
                    "reach_device": 0
                },
                {
                    "province_code": "13",
                    "province_name": "ปทุมธานี   ",
                    "latitude": "14.0207997",
                    "longitude": "100.5228494",
                    "reach_device": 0
                },
                {
                    "province_code": "18",
                    "province_name": "ชัยนาท   ",
                    "latitude": "15.1852851",
                    "longitude": "100.1204413",
                    "reach_device": 0
                },
                {
                    "province_code": "21",
                    "province_name": "ระยอง   ",
                    "latitude": "12.7069119",
                    "longitude": "101.1809187",
                    "reach_device": 1
                },
                {
                    "province_code": "47",
                    "province_name": "สกลนคร   ",
                    "latitude": "17.1544652",
                    "longitude": "104.1325606",
                    "reach_device": 0
                },
                {
                    "province_code": "63",
                    "province_name": "ตาก   ",
                    "latitude": "16.8842269",
                    "longitude": "99.1231478",
                    "reach_device": 0
                },
                {
                    "province_code": "93",
                    "province_name": "พัทลุง   ",
                    "latitude": "7.6169494",
                    "longitude": "100.0706333",
                    "reach_device": 0
                },
                {
                    "province_code": "19",
                    "province_name": "สระบุรี",
                    "latitude": "14.5281393",
                    "longitude": "100.9076211",
                    "reach_device": 0
                },
                {
                    "province_code": "33",
                    "province_name": "ศรีสะเกษ   ",
                    "latitude": "15.1202058",
                    "longitude": "104.3193875",
                    "reach_device": 0
                },
                {
                    "province_code": "44",
                    "province_name": "มหาสารคาม   ",
                    "latitude": "16.1548626",
                    "longitude": "103.3040615",
                    "reach_device": 0
                },
                {
                    "province_code": "50",
                    "province_name": "เชียงใหม่   ",
                    "latitude": "18.8372842",
                    "longitude": "98.9691778",
                    "reach_device": 0
                },
                {
                    "province_code": "53",
                    "province_name": "อุตรดิตถ์   ",
                    "latitude": "17.6297625",
                    "longitude": "100.0937709",
                    "reach_device": 1
                },
                {
                    "province_code": "56",
                    "province_name": "พะเยา   ",
                    "latitude": "19.1912606",
                    "longitude": "99.876879",
                    "reach_device": 0
                },
                {
                    "province_code": "61",
                    "province_name": "อุทัยธานี   ",
                    "latitude": "15.373362",
                    "longitude": "100.0344366",
                    "reach_device": 0
                },
                {
                    "province_code": "64",
                    "province_name": "สุโขทัย   ",
                    "latitude": "17.0057456",
                    "longitude": "99.8242644",
                    "reach_device": 0
                },
                {
                    "province_code": "70",
                    "province_name": "ราชบุรี   ",
                    "latitude": "13.528759",
                    "longitude": "99.8111155",
                    "reach_device": 0
                },
                {
                    "province_code": "80",
                    "province_name": "นครศรีธรรมราช   ",
                    "latitude": "8.4185082",
                    "longitude": "99.9612938",
                    "reach_device": 0
                },
                {
                    "province_code": "83",
                    "province_name": "ภูเก็ต   ",
                    "latitude": "7.8901141",
                    "longitude": "98.3964434",
                    "reach_device": 0
                },
                {
                    "province_code": "84",
                    "province_name": "สุราษฎร์ธานี   ",
                    "latitude": "9.1323042",
                    "longitude": "99.3300335",
                    "reach_device": 1
                },
                {
                    "province_code": "92",
                    "province_name": "ตรัง   ",
                    "latitude": "7.5578483",
                    "longitude": "99.6092894",
                    "reach_device": 0
                },
                {
                    "province_code": "94",
                    "province_name": "ปัตตานี   ",
                    "latitude": "6.867817",
                    "longitude": "101.2480108",
                    "reach_device": 0
                },
                {
                    "province_code": "96",
                    "province_name": "นราธิวาส   ",
                    "latitude": "6.4303443",
                    "longitude": "101.799545",
                    "reach_device": 0
                },
                {
                    "province_code": "14",
                    "province_name": "พระนครศรีอยุธยา   ",
                    "latitude": "14.3418352",
                    "longitude": "100.5679019",
                    "reach_device": 2
                },
                {
                    "province_code": "35",
                    "province_name": "ยโสธร   ",
                    "latitude": "15.8021972",
                    "longitude": "104.1374875",
                    "reach_device": 0
                },
                {
                    "province_code": "36",
                    "province_name": "ชัยภูมิ   ",
                    "latitude": "15.8080698",
                    "longitude": "102.030778",
                    "reach_device": 0
                },
                {
                    "province_code": "51",
                    "province_name": "ลำพูน   ",
                    "latitude": "18.5788749",
                    "longitude": "99.0047979",
                    "reach_device": 0
                },
                {
                    "province_code": "57",
                    "province_name": "เชียงราย   ",
                    "latitude": "19.9203034",
                    "longitude": "99.8117656",
                    "reach_device": 1
                },
                {
                    "province_code": "71",
                    "province_name": "กาญจนบุรี   ",
                    "latitude": "14.0040947",
                    "longitude": "99.5469059",
                    "reach_device": 0
                },
                {
                    "province_code": "76",
                    "province_name": "เพชรบุรี   ",
                    "latitude": "13.1107004",
                    "longitude": "99.9441363",
                    "reach_device": 0
                },
                {
                    "province_code": "81",
                    "province_name": "กระบี่   ",
                    "latitude": "8.0591343",
                    "longitude": "98.9152295",
                    "reach_device": 0
                },
                {
                    "province_code": "12",
                    "province_name": "นนทบุรี   ",
                    "latitude": "13.8602685",
                    "longitude": "100.5125561",
                    "reach_device": 8
                },
                {
                    "province_code": "17",
                    "province_name": "สิงห์บุรี   ",
                    "latitude": "14.8967613",
                    "longitude": "100.408933",
                    "reach_device": 0
                },
                {
                    "province_code": "24",
                    "province_name": "ฉะเชิงเทรา   ",
                    "latitude": "13.687705",
                    "longitude": "101.0679491",
                    "reach_device": 2
                },
                {
                    "province_code": "26",
                    "province_name": "นครนายก   ",
                    "latitude": "14.2121195",
                    "longitude": "101.199244",
                    "reach_device": 0
                },
                {
                    "province_code": "27",
                    "province_name": "สระแก้ว   ",
                    "latitude": "13.7967562",
                    "longitude": "102.1362962",
                    "reach_device": 0
                },
                {
                    "province_code": "34",
                    "province_name": "อุบลราชธานี   ",
                    "latitude": "15.2767697",
                    "longitude": "104.8021057",
                    "reach_device": 0
                },
                {
                    "province_code": "43",
                    "province_name": "หนองคาย   ",
                    "latitude": "17.8679759",
                    "longitude": "102.7423901",
                    "reach_device": 0
                },
                {
                    "province_code": "60",
                    "province_name": "นครสวรรค์   ",
                    "latitude": "15.6905327",
                    "longitude": "100.1116717",
                    "reach_device": 0
                },
                {
                    "province_code": "65",
                    "province_name": "พิษณุโลก   ",
                    "latitude": "16.8262202",
                    "longitude": "100.2579273",
                    "reach_device": 0
                },
                {
                    "province_code": "74",
                    "province_name": "สมุทรสาคร   ",
                    "latitude": "13.5473841",
                    "longitude": "100.2710567",
                    "reach_device": 0
                },
                {
                    "province_code": "86",
                    "province_name": "ชุมพร   ",
                    "latitude": "10.5215575",
                    "longitude": "99.1903987",
                    "reach_device": 0
                },
                {
                    "province_code": "90",
                    "province_name": "สงขลา   ",
                    "latitude": "7.2052572",
                    "longitude": "100.5946716",
                    "reach_device": 1
                },
                {
                    "province_code": "11",
                    "province_name": "สมุทรปราการ   ",
                    "latitude": "13.5973714",
                    "longitude": "100.5944567",
                    "reach_device": 0
                },
                {
                    "province_code": "16",
                    "province_name": "ลพบุรี   ",
                    "latitude": "14.8006662",
                    "longitude": "100.6493253",
                    "reach_device": 1
                },
                {
                    "province_code": "25",
                    "province_name": "ปราจีนบุรี   ",
                    "latitude": "14.1097658",
                    "longitude": "101.3274739",
                    "reach_device": 0
                },
                {
                    "province_code": "41",
                    "province_name": "อุดรธานี   ",
                    "latitude": "17.4150855",
                    "longitude": "102.7851586",
                    "reach_device": 2
                },
                {
                    "province_code": "52",
                    "province_name": "ลำปาง   ",
                    "latitude": "18.274355",
                    "longitude": "99.5064793",
                    "reach_device": 0
                },
                {
                    "province_code": "95",
                    "province_name": "ยะลา   ",
                    "latitude": "6.5400649",
                    "longitude": "101.2790389",
                    "reach_device": 0
                },
                {
                    "province_code": "10",
                    "province_name": "กรุงเทพมหานคร   ",
                    "latitude": "13.7538189",
                    "longitude": "100.4996349",
                    "reach_device": 70
                },
                {
                    "province_code": "15",
                    "province_name": "อ่างทอง   ",
                    "latitude": "14.5888452",
                    "longitude": "100.4528061",
                    "reach_device": 0
                },
                {
                    "province_code": "22",
                    "province_name": "จันทบุรี   ",
                    "latitude": "12.6104079",
                    "longitude": "102.1006586",
                    "reach_device": 0
                },
                {
                    "province_code": "39",
                    "province_name": "หนองบัวลำภู   ",
                    "latitude": "17.2225598",
                    "longitude": "101.3053906",
                    "reach_device": 0
                },
                {
                    "province_code": "40",
                    "province_name": "ขอนแก่น   ",
                    "latitude": "16.4423433",
                    "longitude": "102.8337918",
                    "reach_device": 0
                },
                {
                    "province_code": "46",
                    "province_name": "กาฬสินธุ์   ",
                    "latitude": "16.4066339",
                    "longitude": "103.4779545",
                    "reach_device": 0
                },
                {
                    "province_code": "49",
                    "province_name": "มุกดาหาร   ",
                    "latitude": "16.5456152",
                    "longitude": "104.7238214",
                    "reach_device": 0
                },
                {
                    "province_code": "62",
                    "province_name": "กำแพงเพชร   ",
                    "latitude": "16.5550171",
                    "longitude": "99.5092956",
                    "reach_device": 0
                },
                {
                    "province_code": "66",
                    "province_name": "พิจิตร   ",
                    "latitude": "16.4197423",
                    "longitude": "100.3594536",
                    "reach_device": 0
                },
                {
                    "province_code": "73",
                    "province_name": "นครปฐม   ",
                    "latitude": "13.7667989",
                    "longitude": "100.0564742",
                    "reach_device": 0
                },
                {
                    "province_code": "82",
                    "province_name": "พังงา   ",
                    "latitude": "8.4260761",
                    "longitude": "98.5272303",
                    "reach_device": 0
                },
                {
                    "province_code": "85",
                    "province_name": "ระนอง   ",
                    "latitude": "9.9318504",
                    "longitude": "98.5952741",
                    "reach_device": 0
                },
                {
                    "province_code": "91",
                    "province_name": "สตูล   ",
                    "latitude": "6.6238873",
                    "longitude": "100.0649554",
                    "reach_device": 0
                },
                {
                    "province_code": "23",
                    "province_name": "ตราด   ",
                    "latitude": "12.243338",
                    "longitude": "102.5159198",
                    "reach_device": 0
                },
                {
                    "province_code": "30",
                    "province_name": "นครราชสีมา   ",
                    "latitude": "14.9723599",
                    "longitude": "102.0979841",
                    "reach_device": 0
                },
                {
                    "province_code": "31",
                    "province_name": "บุรีรัมย์   ",
                    "latitude": "14.9441768",
                    "longitude": "103.1043868",
                    "reach_device": 0
                },
                {
                    "province_code": "37",
                    "province_name": "อำนาจเจริญ   ",
                    "latitude": "15.9130803",
                    "longitude": "104.6068462",
                    "reach_device": 0
                },
                {
                    "province_code": "45",
                    "province_name": "ร้อยเอ็ด   ",
                    "latitude": "16.0531679",
                    "longitude": "103.6502522",
                    "reach_device": 0
                },
                {
                    "province_code": "48",
                    "province_name": "นครพนม   ",
                    "latitude": "17.4083358",
                    "longitude": "104.7764807",
                    "reach_device": 0
                },
                {
                    "province_code": "55",
                    "province_name": "น่าน   ",
                    "latitude": "18.7871571",
                    "longitude": "100.7357454",
                    "reach_device": 0
                },
                {
                    "province_code": "67",
                    "province_name": "เพชรบูรณ์   ",
                    "latitude": "16.4396398",
                    "longitude": "101.1526536",
                    "reach_device": 0
                },
                {
                    "province_code": "77",
                    "province_name": "ประจวบคีรีขันธ์   ",
                    "latitude": "11.81216",
                    "longitude": "99.7960062",
                    "reach_device": 0
                }
            ],
            "age": [
                {
                    "age": 5,
                    "reach_device": 0
                },
                {
                    "age": 7,
                    "reach_device": 0
                },
                {
                    "age": 8,
                    "reach_device": 0
                },
                {
                    "age": 10,
                    "reach_device": 0
                },
                {
                    "age": 11,
                    "reach_device": 0
                },
                {
                    "age": 12,
                    "reach_device": 0
                },
                {
                    "age": 13,
                    "reach_device": 0
                },
                {
                    "age": 14,
                    "reach_device": 0
                },
                {
                    "age": 15,
                    "reach_device": 0
                },
                {
                    "age": 16,
                    "reach_device": 0
                },
                {
                    "age": 17,
                    "reach_device": 0
                },
                {
                    "age": 18,
                    "reach_device": 0
                },
                {
                    "age": 19,
                    "reach_device": 0
                },
                {
                    "age": 20,
                    "reach_device": 0
                },
                {
                    "age": 21,
                    "reach_device": 0
                },
                {
                    "age": 22,
                    "reach_device": 0
                },
                {
                    "age": 23,
                    "reach_device": 1
                },
                {
                    "age": 24,
                    "reach_device": 1
                },
                {
                    "age": 25,
                    "reach_device": 1
                },
                {
                    "age": 26,
                    "reach_device": 1
                },
                {
                    "age": 27,
                    "reach_device": 1
                },
                {
                    "age": 28,
                    "reach_device": 0
                },
                {
                    "age": 29,
                    "reach_device": 0
                },
                {
                    "age": 30,
                    "reach_device": 0
                },
                {
                    "age": 31,
                    "reach_device": 0
                },
                {
                    "age": 32,
                    "reach_device": 0
                },
                {
                    "age": 33,
                    "reach_device": 2
                },
                {
                    "age": 34,
                    "reach_device": 0
                },
                {
                    "age": 35,
                    "reach_device": 2
                },
                {
                    "age": 36,
                    "reach_device": 0
                },
                {
                    "age": 37,
                    "reach_device": 1
                },
                {
                    "age": 38,
                    "reach_device": 1
                },
                {
                    "age": 39,
                    "reach_device": 2
                },
                {
                    "age": 40,
                    "reach_device": 0
                },
                {
                    "age": 41,
                    "reach_device": 1
                },
                {
                    "age": 42,
                    "reach_device": 0
                },
                {
                    "age": 43,
                    "reach_device": 1
                },
                {
                    "age": 44,
                    "reach_device": 1
                },
                {
                    "age": 45,
                    "reach_device": 0
                },
                {
                    "age": 46,
                    "reach_device": 0
                },
                {
                    "age": 47,
                    "reach_device": 1
                },
                {
                    "age": 48,
                    "reach_device": 0
                },
                {
                    "age": 49,
                    "reach_device": 1
                },
                {
                    "age": 50,
                    "reach_device": 0
                },
                {
                    "age": 51,
                    "reach_device": 0
                },
                {
                    "age": 52,
                    "reach_device": 1
                },
                {
                    "age": 53,
                    "reach_device": 0
                },
                {
                    "age": 54,
                    "reach_device": 1
                },
                {
                    "age": 55,
                    "reach_device": 0
                },
                {
                    "age": 56,
                    "reach_device": 0
                },
                {
                    "age": 57,
                    "reach_device": 0
                },
                {
                    "age": 58,
                    "reach_device": 0
                },
                {
                    "age": 59,
                    "reach_device": 0
                },
                {
                    "age": 60,
                    "reach_device": 1
                },
                {
                    "age": 61,
                    "reach_device": 0
                },
                {
                    "age": 62,
                    "reach_device": 0
                },
                {
                    "age": 63,
                    "reach_device": 1
                },
                {
                    "age": 64,
                    "reach_device": 1
                },
                {
                    "age": 65,
                    "reach_device": 0
                },
                {
                    "age": 66,
                    "reach_device": 1
                },
                {
                    "age": 67,
                    "reach_device": 0
                },
                {
                    "age": 68,
                    "reach_device": 0
                },
                {
                    "age": 69,
                    "reach_device": 0
                },
                {
                    "age": 70,
                    "reach_device": 0
                },
                {
                    "age": 71,
                    "reach_device": 0
                },
                {
                    "age": 72,
                    "reach_device": 0
                },
                {
                    "age": 73,
                    "reach_device": 0
                },
                {
                    "age": 74,
                    "reach_device": 0
                },
                {
                    "age": 75,
                    "reach_device": 0
                },
                {
                    "age": 76,
                    "reach_device": 0
                },
                {
                    "age": 78,
                    "reach_device": 0
                },
                {
                    "age": 80,
                    "reach_device": 0
                },
                {
                    "age": 84,
                    "reach_device": 0
                },
                {
                    "age": 88,
                    "reach_device": 0
                },
                {
                    "age": 91,
                    "reach_device": 0
                },
                {
                    "age": 92,
                    "reach_device": 0
                },
                {
                    "age": 93,
                    "reach_device": 0
                },
                {
                    "age": 94,
                    "reach_device": 0
                },
                {
                    "age": 95,
                    "reach_device": 0
                },
                {
                    "age": 96,
                    "reach_device": 0
                },
                {
                    "age": 100,
                    "reach_device": 0
                },
                {
                    "age": 105,
                    "reach_device": 0
                },
                {
                    "age": 107,
                    "reach_device": 0
                },
                {
                    "age": 108,
                    "reach_device": 0
                },
                {
                    "age": 109,
                    "reach_device": 0
                },
                {
                    "age": 110,
                    "reach_device": 0
                },
                {
                    "age": 111,
                    "reach_device": 0
                },
                {
                    "age": 112,
                    "reach_device": 0
                },
                {
                    "age": 113,
                    "reach_device": 0
                },
                {
                    "age": 119,
                    "reach_device": 0
                },
                {
                    "age": "ระบุไม่ได้",
                    "reach_device": 79
                }
            ],
            "total_devices": 109
        },
        {
            "channels_id": 273,
            "channel_name": "Thairath TV",
            "period": "00:20:01-00:21:00",
            "reach_device": 230,
            "gender": {
                "male": 31,
                "female": 22,
                "none": 177
            },
            "provinces": [
                {
                    "province_code": "42",
                    "province_name": "เลย   ",
                    "latitude": "17.4873658",
                    "longitude": "101.7179794",
                    "reach_device": 0
                },
                {
                    "province_code": "65",
                    "province_name": "พิษณุโลก   ",
                    "latitude": "16.8262202",
                    "longitude": "100.2579273",
                    "reach_device": 1
                },
                {
                    "province_code": "72",
                    "province_name": "สุพรรณบุรี   ",
                    "latitude": "14.5137891",
                    "longitude": "100.1279803",
                    "reach_device": 0
                },
                {
                    "province_code": "10",
                    "province_name": "กรุงเทพมหานคร   ",
                    "latitude": "13.7538189",
                    "longitude": "100.4996349",
                    "reach_device": 157
                },
                {
                    "province_code": "22",
                    "province_name": "จันทบุรี   ",
                    "latitude": "12.6104079",
                    "longitude": "102.1006586",
                    "reach_device": 0
                },
                {
                    "province_code": "26",
                    "province_name": "นครนายก   ",
                    "latitude": "14.2121195",
                    "longitude": "101.199244",
                    "reach_device": 0
                },
                {
                    "province_code": "27",
                    "province_name": "สระแก้ว   ",
                    "latitude": "13.7967562",
                    "longitude": "102.1362962",
                    "reach_device": 0
                },
                {
                    "province_code": "57",
                    "province_name": "เชียงราย   ",
                    "latitude": "19.9203034",
                    "longitude": "99.8117656",
                    "reach_device": 1
                },
                {
                    "province_code": "62",
                    "province_name": "กำแพงเพชร   ",
                    "latitude": "16.5550171",
                    "longitude": "99.5092956",
                    "reach_device": 0
                },
                {
                    "province_code": "74",
                    "province_name": "สมุทรสาคร   ",
                    "latitude": "13.5473841",
                    "longitude": "100.2710567",
                    "reach_device": 0
                },
                {
                    "province_code": "76",
                    "province_name": "เพชรบุรี   ",
                    "latitude": "13.1107004",
                    "longitude": "99.9441363",
                    "reach_device": 0
                },
                {
                    "province_code": "85",
                    "province_name": "ระนอง   ",
                    "latitude": "9.9318504",
                    "longitude": "98.5952741",
                    "reach_device": 0
                },
                {
                    "province_code": "93",
                    "province_name": "พัทลุง   ",
                    "latitude": "7.6169494",
                    "longitude": "100.0706333",
                    "reach_device": 0
                },
                {
                    "province_code": "16",
                    "province_name": "ลพบุรี   ",
                    "latitude": "14.8006662",
                    "longitude": "100.6493253",
                    "reach_device": 7
                },
                {
                    "province_code": "17",
                    "province_name": "สิงห์บุรี   ",
                    "latitude": "14.8967613",
                    "longitude": "100.408933",
                    "reach_device": 0
                },
                {
                    "province_code": "25",
                    "province_name": "ปราจีนบุรี   ",
                    "latitude": "14.1097658",
                    "longitude": "101.3274739",
                    "reach_device": 0
                },
                {
                    "province_code": "37",
                    "province_name": "อำนาจเจริญ   ",
                    "latitude": "15.9130803",
                    "longitude": "104.6068462",
                    "reach_device": 0
                },
                {
                    "province_code": "39",
                    "province_name": "หนองบัวลำภู   ",
                    "latitude": "17.2225598",
                    "longitude": "101.3053906",
                    "reach_device": 0
                },
                {
                    "province_code": "52",
                    "province_name": "ลำปาง   ",
                    "latitude": "18.274355",
                    "longitude": "99.5064793",
                    "reach_device": 0
                },
                {
                    "province_code": "60",
                    "province_name": "นครสวรรค์   ",
                    "latitude": "15.6905327",
                    "longitude": "100.1116717",
                    "reach_device": 0
                },
                {
                    "province_code": "73",
                    "province_name": "นครปฐม   ",
                    "latitude": "13.7667989",
                    "longitude": "100.0564742",
                    "reach_device": 0
                },
                {
                    "province_code": "90",
                    "province_name": "สงขลา   ",
                    "latitude": "7.2052572",
                    "longitude": "100.5946716",
                    "reach_device": 0
                },
                {
                    "province_code": "11",
                    "province_name": "สมุทรปราการ   ",
                    "latitude": "13.5973714",
                    "longitude": "100.5944567",
                    "reach_device": 0
                },
                {
                    "province_code": "12",
                    "province_name": "นนทบุรี   ",
                    "latitude": "13.8602685",
                    "longitude": "100.5125561",
                    "reach_device": 19
                },
                {
                    "province_code": "18",
                    "province_name": "ชัยนาท   ",
                    "latitude": "15.1852851",
                    "longitude": "100.1204413",
                    "reach_device": 0
                },
                {
                    "province_code": "24",
                    "province_name": "ฉะเชิงเทรา   ",
                    "latitude": "13.687705",
                    "longitude": "101.0679491",
                    "reach_device": 0
                },
                {
                    "province_code": "34",
                    "province_name": "อุบลราชธานี   ",
                    "latitude": "15.2767697",
                    "longitude": "104.8021057",
                    "reach_device": 0
                },
                {
                    "province_code": "41",
                    "province_name": "อุดรธานี   ",
                    "latitude": "17.4150855",
                    "longitude": "102.7851586",
                    "reach_device": 3
                },
                {
                    "province_code": "43",
                    "province_name": "หนองคาย   ",
                    "latitude": "17.8679759",
                    "longitude": "102.7423901",
                    "reach_device": 0
                },
                {
                    "province_code": "54",
                    "province_name": "แพร่   ",
                    "latitude": "18.1455312",
                    "longitude": "100.1382926",
                    "reach_device": 0
                },
                {
                    "province_code": "63",
                    "province_name": "ตาก   ",
                    "latitude": "16.8842269",
                    "longitude": "99.1231478",
                    "reach_device": 0
                },
                {
                    "province_code": "86",
                    "province_name": "ชุมพร   ",
                    "latitude": "10.5215575",
                    "longitude": "99.1903987",
                    "reach_device": 0
                },
                {
                    "province_code": "14",
                    "province_name": "พระนครศรีอยุธยา   ",
                    "latitude": "14.3418352",
                    "longitude": "100.5679019",
                    "reach_device": 2
                },
                {
                    "province_code": "19",
                    "province_name": "สระบุรี",
                    "latitude": "14.5281393",
                    "longitude": "100.9076211",
                    "reach_device": 0
                },
                {
                    "province_code": "36",
                    "province_name": "ชัยภูมิ   ",
                    "latitude": "15.8080698",
                    "longitude": "102.030778",
                    "reach_device": 0
                },
                {
                    "province_code": "51",
                    "province_name": "ลำพูน   ",
                    "latitude": "18.5788749",
                    "longitude": "99.0047979",
                    "reach_device": 0
                },
                {
                    "province_code": "70",
                    "province_name": "ราชบุรี   ",
                    "latitude": "13.528759",
                    "longitude": "99.8111155",
                    "reach_device": 0
                },
                {
                    "province_code": "23",
                    "province_name": "ตราด   ",
                    "latitude": "12.243338",
                    "longitude": "102.5159198",
                    "reach_device": 0
                },
                {
                    "province_code": "33",
                    "province_name": "ศรีสะเกษ   ",
                    "latitude": "15.1202058",
                    "longitude": "104.3193875",
                    "reach_device": 0
                },
                {
                    "province_code": "35",
                    "province_name": "ยโสธร   ",
                    "latitude": "15.8021972",
                    "longitude": "104.1374875",
                    "reach_device": 0
                },
                {
                    "province_code": "44",
                    "province_name": "มหาสารคาม   ",
                    "latitude": "16.1548626",
                    "longitude": "103.3040615",
                    "reach_device": 0
                },
                {
                    "province_code": "50",
                    "province_name": "เชียงใหม่   ",
                    "latitude": "18.8372842",
                    "longitude": "98.9691778",
                    "reach_device": 3
                },
                {
                    "province_code": "55",
                    "province_name": "น่าน   ",
                    "latitude": "18.7871571",
                    "longitude": "100.7357454",
                    "reach_device": 0
                },
                {
                    "province_code": "56",
                    "province_name": "พะเยา   ",
                    "latitude": "19.1912606",
                    "longitude": "99.876879",
                    "reach_device": 0
                },
                {
                    "province_code": "61",
                    "province_name": "อุทัยธานี   ",
                    "latitude": "15.373362",
                    "longitude": "100.0344366",
                    "reach_device": 0
                },
                {
                    "province_code": "64",
                    "province_name": "สุโขทัย   ",
                    "latitude": "17.0057456",
                    "longitude": "99.8242644",
                    "reach_device": 0
                },
                {
                    "province_code": "71",
                    "province_name": "กาญจนบุรี   ",
                    "latitude": "14.0040947",
                    "longitude": "99.5469059",
                    "reach_device": 0
                },
                {
                    "province_code": "81",
                    "province_name": "กระบี่   ",
                    "latitude": "8.0591343",
                    "longitude": "98.9152295",
                    "reach_device": 0
                },
                {
                    "province_code": "83",
                    "province_name": "ภูเก็ต   ",
                    "latitude": "7.8901141",
                    "longitude": "98.3964434",
                    "reach_device": 0
                },
                {
                    "province_code": "92",
                    "province_name": "ตรัง   ",
                    "latitude": "7.5578483",
                    "longitude": "99.6092894",
                    "reach_device": 0
                },
                {
                    "province_code": "94",
                    "province_name": "ปัตตานี   ",
                    "latitude": "6.867817",
                    "longitude": "101.2480108",
                    "reach_device": 0
                },
                {
                    "province_code": "96",
                    "province_name": "นราธิวาส   ",
                    "latitude": "6.4303443",
                    "longitude": "101.799545",
                    "reach_device": 0
                },
                {
                    "province_code": "13",
                    "province_name": "ปทุมธานี   ",
                    "latitude": "14.0207997",
                    "longitude": "100.5228494",
                    "reach_device": 0
                },
                {
                    "province_code": "15",
                    "province_name": "อ่างทอง   ",
                    "latitude": "14.5888452",
                    "longitude": "100.4528061",
                    "reach_device": 0
                },
                {
                    "province_code": "21",
                    "province_name": "ระยอง   ",
                    "latitude": "12.7069119",
                    "longitude": "101.1809187",
                    "reach_device": 1
                },
                {
                    "province_code": "40",
                    "province_name": "ขอนแก่น   ",
                    "latitude": "16.4423433",
                    "longitude": "102.8337918",
                    "reach_device": 1
                },
                {
                    "province_code": "46",
                    "province_name": "กาฬสินธุ์   ",
                    "latitude": "16.4066339",
                    "longitude": "103.4779545",
                    "reach_device": 0
                },
                {
                    "province_code": "47",
                    "province_name": "สกลนคร   ",
                    "latitude": "17.1544652",
                    "longitude": "104.1325606",
                    "reach_device": 1
                },
                {
                    "province_code": "49",
                    "province_name": "มุกดาหาร   ",
                    "latitude": "16.5456152",
                    "longitude": "104.7238214",
                    "reach_device": 0
                },
                {
                    "province_code": "53",
                    "province_name": "อุตรดิตถ์   ",
                    "latitude": "17.6297625",
                    "longitude": "100.0937709",
                    "reach_device": 0
                },
                {
                    "province_code": "66",
                    "province_name": "พิจิตร   ",
                    "latitude": "16.4197423",
                    "longitude": "100.3594536",
                    "reach_device": 0
                },
                {
                    "province_code": "82",
                    "province_name": "พังงา   ",
                    "latitude": "8.4260761",
                    "longitude": "98.5272303",
                    "reach_device": 0
                },
                {
                    "province_code": "91",
                    "province_name": "สตูล   ",
                    "latitude": "6.6238873",
                    "longitude": "100.0649554",
                    "reach_device": 0
                },
                {
                    "province_code": "95",
                    "province_name": "ยะลา   ",
                    "latitude": "6.5400649",
                    "longitude": "101.2790389",
                    "reach_device": 0
                },
                {
                    "province_code": "20",
                    "province_name": "ชลบุรี   ",
                    "latitude": "13.3619454",
                    "longitude": "100.9812561",
                    "reach_device": 1
                },
                {
                    "province_code": "30",
                    "province_name": "นครราชสีมา   ",
                    "latitude": "14.9723599",
                    "longitude": "102.0979841",
                    "reach_device": 0
                },
                {
                    "province_code": "31",
                    "province_name": "บุรีรัมย์   ",
                    "latitude": "14.9441768",
                    "longitude": "103.1043868",
                    "reach_device": 2
                },
                {
                    "province_code": "32",
                    "province_name": "สุรินทร์   ",
                    "latitude": "14.8822919",
                    "longitude": "103.4900911",
                    "reach_device": 1
                },
                {
                    "province_code": "45",
                    "province_name": "ร้อยเอ็ด   ",
                    "latitude": "16.0531679",
                    "longitude": "103.6502522",
                    "reach_device": 0
                },
                {
                    "province_code": "48",
                    "province_name": "นครพนม   ",
                    "latitude": "17.4083358",
                    "longitude": "104.7764807",
                    "reach_device": 1
                },
                {
                    "province_code": "67",
                    "province_name": "เพชรบูรณ์   ",
                    "latitude": "16.4396398",
                    "longitude": "101.1526536",
                    "reach_device": 1
                },
                {
                    "province_code": "75",
                    "province_name": "สมุทรสงคราม   ",
                    "latitude": "13.4129727",
                    "longitude": "99.9994864",
                    "reach_device": 1
                },
                {
                    "province_code": "77",
                    "province_name": "ประจวบคีรีขันธ์   ",
                    "latitude": "11.81216",
                    "longitude": "99.7960062",
                    "reach_device": 0
                },
                {
                    "province_code": "80",
                    "province_name": "นครศรีธรรมราช   ",
                    "latitude": "8.4185082",
                    "longitude": "99.9612938",
                    "reach_device": 1
                },
                {
                    "province_code": "84",
                    "province_name": "สุราษฎร์ธานี   ",
                    "latitude": "9.1323042",
                    "longitude": "99.3300335",
                    "reach_device": 0
                }
            ],
            "age": [
                {
                    "age": 1,
                    "reach_device": 0
                },
                {
                    "age": 5,
                    "reach_device": 0
                },
                {
                    "age": 6,
                    "reach_device": 0
                },
                {
                    "age": 7,
                    "reach_device": 0
                },
                {
                    "age": 8,
                    "reach_device": 0
                },
                {
                    "age": 9,
                    "reach_device": 0
                },
                {
                    "age": 10,
                    "reach_device": 1
                },
                {
                    "age": 11,
                    "reach_device": 0
                },
                {
                    "age": 12,
                    "reach_device": 0
                },
                {
                    "age": 13,
                    "reach_device": 0
                },
                {
                    "age": 14,
                    "reach_device": 0
                },
                {
                    "age": 15,
                    "reach_device": 1
                },
                {
                    "age": 16,
                    "reach_device": 0
                },
                {
                    "age": 17,
                    "reach_device": 0
                },
                {
                    "age": 18,
                    "reach_device": 0
                },
                {
                    "age": 19,
                    "reach_device": 1
                },
                {
                    "age": 20,
                    "reach_device": 2
                },
                {
                    "age": 21,
                    "reach_device": 0
                },
                {
                    "age": 22,
                    "reach_device": 0
                },
                {
                    "age": 23,
                    "reach_device": 0
                },
                {
                    "age": 24,
                    "reach_device": 3
                },
                {
                    "age": 25,
                    "reach_device": 0
                },
                {
                    "age": 26,
                    "reach_device": 2
                },
                {
                    "age": 27,
                    "reach_device": 3
                },
                {
                    "age": 28,
                    "reach_device": 2
                },
                {
                    "age": 29,
                    "reach_device": 3
                },
                {
                    "age": 30,
                    "reach_device": 1
                },
                {
                    "age": 31,
                    "reach_device": 4
                },
                {
                    "age": 32,
                    "reach_device": 2
                },
                {
                    "age": 33,
                    "reach_device": 0
                },
                {
                    "age": 34,
                    "reach_device": 1
                },
                {
                    "age": 35,
                    "reach_device": 2
                },
                {
                    "age": 36,
                    "reach_device": 1
                },
                {
                    "age": 37,
                    "reach_device": 0
                },
                {
                    "age": 38,
                    "reach_device": 1
                },
                {
                    "age": 39,
                    "reach_device": 1
                },
                {
                    "age": 40,
                    "reach_device": 0
                },
                {
                    "age": 41,
                    "reach_device": 2
                },
                {
                    "age": 42,
                    "reach_device": 2
                },
                {
                    "age": 43,
                    "reach_device": 0
                },
                {
                    "age": 44,
                    "reach_device": 0
                },
                {
                    "age": 45,
                    "reach_device": 1
                },
                {
                    "age": 46,
                    "reach_device": 1
                },
                {
                    "age": 47,
                    "reach_device": 1
                },
                {
                    "age": 48,
                    "reach_device": 0
                },
                {
                    "age": 49,
                    "reach_device": 0
                },
                {
                    "age": 50,
                    "reach_device": 1
                },
                {
                    "age": 51,
                    "reach_device": 1
                },
                {
                    "age": 52,
                    "reach_device": 2
                },
                {
                    "age": 53,
                    "reach_device": 1
                },
                {
                    "age": 54,
                    "reach_device": 2
                },
                {
                    "age": 55,
                    "reach_device": 0
                },
                {
                    "age": 56,
                    "reach_device": 0
                },
                {
                    "age": 57,
                    "reach_device": 2
                },
                {
                    "age": 58,
                    "reach_device": 0
                },
                {
                    "age": 59,
                    "reach_device": 1
                },
                {
                    "age": 60,
                    "reach_device": 1
                },
                {
                    "age": 61,
                    "reach_device": 0
                },
                {
                    "age": 62,
                    "reach_device": 0
                },
                {
                    "age": 63,
                    "reach_device": 0
                },
                {
                    "age": 64,
                    "reach_device": 0
                },
                {
                    "age": 65,
                    "reach_device": 2
                },
                {
                    "age": 66,
                    "reach_device": 0
                },
                {
                    "age": 67,
                    "reach_device": 1
                },
                {
                    "age": 68,
                    "reach_device": 0
                },
                {
                    "age": 69,
                    "reach_device": 0
                },
                {
                    "age": 70,
                    "reach_device": 0
                },
                {
                    "age": 71,
                    "reach_device": 0
                },
                {
                    "age": 72,
                    "reach_device": 0
                },
                {
                    "age": 73,
                    "reach_device": 0
                },
                {
                    "age": 74,
                    "reach_device": 0
                },
                {
                    "age": 75,
                    "reach_device": 0
                },
                {
                    "age": 76,
                    "reach_device": 0
                },
                {
                    "age": 77,
                    "reach_device": 0
                },
                {
                    "age": 78,
                    "reach_device": 0
                },
                {
                    "age": 79,
                    "reach_device": 0
                },
                {
                    "age": 80,
                    "reach_device": 0
                },
                {
                    "age": 81,
                    "reach_device": 0
                },
                {
                    "age": 84,
                    "reach_device": 0
                },
                {
                    "age": 86,
                    "reach_device": 0
                },
                {
                    "age": 88,
                    "reach_device": 0
                },
                {
                    "age": 90,
                    "reach_device": 0
                },
                {
                    "age": 91,
                    "reach_device": 0
                },
                {
                    "age": 92,
                    "reach_device": 0
                },
                {
                    "age": 93,
                    "reach_device": 0
                },
                {
                    "age": 95,
                    "reach_device": 0
                },
                {
                    "age": 96,
                    "reach_device": 0
                },
                {
                    "age": 98,
                    "reach_device": 0
                },
                {
                    "age": 100,
                    "reach_device": 0
                },
                {
                    "age": 101,
                    "reach_device": 0
                },
                {
                    "age": 105,
                    "reach_device": 0
                },
                {
                    "age": 106,
                    "reach_device": 0
                },
                {
                    "age": 107,
                    "reach_device": 0
                },
                {
                    "age": 108,
                    "reach_device": 0
                },
                {
                    "age": 109,
                    "reach_device": 0
                },
                {
                    "age": 110,
                    "reach_device": 0
                },
                {
                    "age": 111,
                    "reach_device": 0
                },
                {
                    "age": 112,
                    "reach_device": 0
                },
                {
                    "age": 113,
                    "reach_device": 0
                },
                {
                    "age": 119,
                    "reach_device": 1
                },
                {
                    "age": 102,
                    "reach_device": 0
                },
                {
                    "age": "ระบุไม่ได้",
                    "reach_device": 177
                }
            ],
            "total_devices": 243
        },
        {
            "channels_id": 277,
            "channel_name": "PPTV",
            "period": "00:20:01-00:21:00",
            "reach_device": 69,
            "gender": {
                "male": 5,
                "female": 2,
                "none": 62
            },
            "provinces": [
                {
                    "province_code": "24",
                    "province_name": "ฉะเชิงเทรา   ",
                    "latitude": "13.687705",
                    "longitude": "101.0679491",
                    "reach_device": 0
                },
                {
                    "province_code": "36",
                    "province_name": "ชัยภูมิ   ",
                    "latitude": "15.8080698",
                    "longitude": "102.030778",
                    "reach_device": 0
                },
                {
                    "province_code": "45",
                    "province_name": "ร้อยเอ็ด   ",
                    "latitude": "16.0531679",
                    "longitude": "103.6502522",
                    "reach_device": 0
                },
                {
                    "province_code": "48",
                    "province_name": "นครพนม   ",
                    "latitude": "17.4083358",
                    "longitude": "104.7764807",
                    "reach_device": 1
                },
                {
                    "province_code": "51",
                    "province_name": "ลำพูน   ",
                    "latitude": "18.5788749",
                    "longitude": "99.0047979",
                    "reach_device": 0
                },
                {
                    "province_code": "80",
                    "province_name": "นครศรีธรรมราช   ",
                    "latitude": "8.4185082",
                    "longitude": "99.9612938",
                    "reach_device": 0
                },
                {
                    "province_code": "84",
                    "province_name": "สุราษฎร์ธานี   ",
                    "latitude": "9.1323042",
                    "longitude": "99.3300335",
                    "reach_device": 1
                },
                {
                    "province_code": "86",
                    "province_name": "ชุมพร   ",
                    "latitude": "10.5215575",
                    "longitude": "99.1903987",
                    "reach_device": 0
                },
                {
                    "province_code": "11",
                    "province_name": "สมุทรปราการ   ",
                    "latitude": "13.5973714",
                    "longitude": "100.5944567",
                    "reach_device": 0
                },
                {
                    "province_code": "12",
                    "province_name": "นนทบุรี   ",
                    "latitude": "13.8602685",
                    "longitude": "100.5125561",
                    "reach_device": 2
                },
                {
                    "province_code": "15",
                    "province_name": "อ่างทอง   ",
                    "latitude": "14.5888452",
                    "longitude": "100.4528061",
                    "reach_device": 0
                },
                {
                    "province_code": "19",
                    "province_name": "สระบุรี",
                    "latitude": "14.5281393",
                    "longitude": "100.9076211",
                    "reach_device": 0
                },
                {
                    "province_code": "40",
                    "province_name": "ขอนแก่น   ",
                    "latitude": "16.4423433",
                    "longitude": "102.8337918",
                    "reach_device": 2
                },
                {
                    "province_code": "41",
                    "province_name": "อุดรธานี   ",
                    "latitude": "17.4150855",
                    "longitude": "102.7851586",
                    "reach_device": 2
                },
                {
                    "province_code": "57",
                    "province_name": "เชียงราย   ",
                    "latitude": "19.9203034",
                    "longitude": "99.8117656",
                    "reach_device": 0
                },
                {
                    "province_code": "70",
                    "province_name": "ราชบุรี   ",
                    "latitude": "13.528759",
                    "longitude": "99.8111155",
                    "reach_device": 0
                },
                {
                    "province_code": "91",
                    "province_name": "สตูล   ",
                    "latitude": "6.6238873",
                    "longitude": "100.0649554",
                    "reach_device": 0
                },
                {
                    "province_code": "95",
                    "province_name": "ยะลา   ",
                    "latitude": "6.5400649",
                    "longitude": "101.2790389",
                    "reach_device": 0
                },
                {
                    "province_code": "13",
                    "province_name": "ปทุมธานี   ",
                    "latitude": "14.0207997",
                    "longitude": "100.5228494",
                    "reach_device": 0
                },
                {
                    "province_code": "17",
                    "province_name": "สิงห์บุรี   ",
                    "latitude": "14.8967613",
                    "longitude": "100.408933",
                    "reach_device": 0
                },
                {
                    "province_code": "18",
                    "province_name": "ชัยนาท   ",
                    "latitude": "15.1852851",
                    "longitude": "100.1204413",
                    "reach_device": 0
                },
                {
                    "province_code": "21",
                    "province_name": "ระยอง   ",
                    "latitude": "12.7069119",
                    "longitude": "101.1809187",
                    "reach_device": 0
                },
                {
                    "province_code": "30",
                    "province_name": "นครราชสีมา   ",
                    "latitude": "14.9723599",
                    "longitude": "102.0979841",
                    "reach_device": 1
                },
                {
                    "province_code": "31",
                    "province_name": "บุรีรัมย์   ",
                    "latitude": "14.9441768",
                    "longitude": "103.1043868",
                    "reach_device": 2
                },
                {
                    "province_code": "32",
                    "province_name": "สุรินทร์   ",
                    "latitude": "14.8822919",
                    "longitude": "103.4900911",
                    "reach_device": 0
                },
                {
                    "province_code": "34",
                    "province_name": "อุบลราชธานี   ",
                    "latitude": "15.2767697",
                    "longitude": "104.8021057",
                    "reach_device": 0
                },
                {
                    "province_code": "43",
                    "province_name": "หนองคาย   ",
                    "latitude": "17.8679759",
                    "longitude": "102.7423901",
                    "reach_device": 1
                },
                {
                    "province_code": "63",
                    "province_name": "ตาก   ",
                    "latitude": "16.8842269",
                    "longitude": "99.1231478",
                    "reach_device": 0
                },
                {
                    "province_code": "67",
                    "province_name": "เพชรบูรณ์   ",
                    "latitude": "16.4396398",
                    "longitude": "101.1526536",
                    "reach_device": 0
                },
                {
                    "province_code": "16",
                    "province_name": "ลพบุรี   ",
                    "latitude": "14.8006662",
                    "longitude": "100.6493253",
                    "reach_device": 2
                },
                {
                    "province_code": "22",
                    "province_name": "จันทบุรี   ",
                    "latitude": "12.6104079",
                    "longitude": "102.1006586",
                    "reach_device": 0
                },
                {
                    "province_code": "37",
                    "province_name": "อำนาจเจริญ   ",
                    "latitude": "15.9130803",
                    "longitude": "104.6068462",
                    "reach_device": 0
                },
                {
                    "province_code": "39",
                    "province_name": "หนองบัวลำภู   ",
                    "latitude": "17.2225598",
                    "longitude": "101.3053906",
                    "reach_device": 0
                },
                {
                    "province_code": "54",
                    "province_name": "แพร่   ",
                    "latitude": "18.1455312",
                    "longitude": "100.1382926",
                    "reach_device": 1
                },
                {
                    "province_code": "62",
                    "province_name": "กำแพงเพชร   ",
                    "latitude": "16.5550171",
                    "longitude": "99.5092956",
                    "reach_device": 0
                },
                {
                    "province_code": "73",
                    "province_name": "นครปฐม   ",
                    "latitude": "13.7667989",
                    "longitude": "100.0564742",
                    "reach_device": 0
                },
                {
                    "province_code": "93",
                    "province_name": "พัทลุง   ",
                    "latitude": "7.6169494",
                    "longitude": "100.0706333",
                    "reach_device": 0
                },
                {
                    "province_code": "10",
                    "province_name": "กรุงเทพมหานคร   ",
                    "latitude": "13.7538189",
                    "longitude": "100.4996349",
                    "reach_device": 46
                },
                {
                    "province_code": "23",
                    "province_name": "ตราด   ",
                    "latitude": "12.243338",
                    "longitude": "102.5159198",
                    "reach_device": 0
                },
                {
                    "province_code": "50",
                    "province_name": "เชียงใหม่   ",
                    "latitude": "18.8372842",
                    "longitude": "98.9691778",
                    "reach_device": 0
                },
                {
                    "province_code": "55",
                    "province_name": "น่าน   ",
                    "latitude": "18.7871571",
                    "longitude": "100.7357454",
                    "reach_device": 0
                },
                {
                    "province_code": "85",
                    "province_name": "ระนอง   ",
                    "latitude": "9.9318504",
                    "longitude": "98.5952741",
                    "reach_device": 0
                },
                {
                    "province_code": "94",
                    "province_name": "ปัตตานี   ",
                    "latitude": "6.867817",
                    "longitude": "101.2480108",
                    "reach_device": 0
                },
                {
                    "province_code": "14",
                    "province_name": "พระนครศรีอยุธยา   ",
                    "latitude": "14.3418352",
                    "longitude": "100.5679019",
                    "reach_device": 0
                },
                {
                    "province_code": "20",
                    "province_name": "ชลบุรี   ",
                    "latitude": "13.3619454",
                    "longitude": "100.9812561",
                    "reach_device": 0
                },
                {
                    "province_code": "46",
                    "province_name": "กาฬสินธุ์   ",
                    "latitude": "16.4066339",
                    "longitude": "103.4779545",
                    "reach_device": 0
                },
                {
                    "province_code": "47",
                    "province_name": "สกลนคร   ",
                    "latitude": "17.1544652",
                    "longitude": "104.1325606",
                    "reach_device": 0
                },
                {
                    "province_code": "49",
                    "province_name": "มุกดาหาร   ",
                    "latitude": "16.5456152",
                    "longitude": "104.7238214",
                    "reach_device": 0
                },
                {
                    "province_code": "66",
                    "province_name": "พิจิตร   ",
                    "latitude": "16.4197423",
                    "longitude": "100.3594536",
                    "reach_device": 0
                },
                {
                    "province_code": "75",
                    "province_name": "สมุทรสงคราม   ",
                    "latitude": "13.4129727",
                    "longitude": "99.9994864",
                    "reach_device": 0
                },
                {
                    "province_code": "77",
                    "province_name": "ประจวบคีรีขันธ์   ",
                    "latitude": "11.81216",
                    "longitude": "99.7960062",
                    "reach_device": 0
                },
                {
                    "province_code": "82",
                    "province_name": "พังงา   ",
                    "latitude": "8.4260761",
                    "longitude": "98.5272303",
                    "reach_device": 0
                },
                {
                    "province_code": "26",
                    "province_name": "นครนายก   ",
                    "latitude": "14.2121195",
                    "longitude": "101.199244",
                    "reach_device": 0
                },
                {
                    "province_code": "27",
                    "province_name": "สระแก้ว   ",
                    "latitude": "13.7967562",
                    "longitude": "102.1362962",
                    "reach_device": 0
                },
                {
                    "province_code": "44",
                    "province_name": "มหาสารคาม   ",
                    "latitude": "16.1548626",
                    "longitude": "103.3040615",
                    "reach_device": 0
                },
                {
                    "province_code": "52",
                    "province_name": "ลำปาง   ",
                    "latitude": "18.274355",
                    "longitude": "99.5064793",
                    "reach_device": 0
                },
                {
                    "province_code": "53",
                    "province_name": "อุตรดิตถ์   ",
                    "latitude": "17.6297625",
                    "longitude": "100.0937709",
                    "reach_device": 0
                },
                {
                    "province_code": "61",
                    "province_name": "อุทัยธานี   ",
                    "latitude": "15.373362",
                    "longitude": "100.0344366",
                    "reach_device": 0
                },
                {
                    "province_code": "74",
                    "province_name": "สมุทรสาคร   ",
                    "latitude": "13.5473841",
                    "longitude": "100.2710567",
                    "reach_device": 0
                },
                {
                    "province_code": "76",
                    "province_name": "เพชรบุรี   ",
                    "latitude": "13.1107004",
                    "longitude": "99.9441363",
                    "reach_device": 1
                },
                {
                    "province_code": "81",
                    "province_name": "กระบี่   ",
                    "latitude": "8.0591343",
                    "longitude": "98.9152295",
                    "reach_device": 1
                },
                {
                    "province_code": "90",
                    "province_name": "สงขลา   ",
                    "latitude": "7.2052572",
                    "longitude": "100.5946716",
                    "reach_device": 0
                },
                {
                    "province_code": "92",
                    "province_name": "ตรัง   ",
                    "latitude": "7.5578483",
                    "longitude": "99.6092894",
                    "reach_device": 0
                },
                {
                    "province_code": "25",
                    "province_name": "ปราจีนบุรี   ",
                    "latitude": "14.1097658",
                    "longitude": "101.3274739",
                    "reach_device": 0
                },
                {
                    "province_code": "33",
                    "province_name": "ศรีสะเกษ   ",
                    "latitude": "15.1202058",
                    "longitude": "104.3193875",
                    "reach_device": 0
                },
                {
                    "province_code": "35",
                    "province_name": "ยโสธร   ",
                    "latitude": "15.8021972",
                    "longitude": "104.1374875",
                    "reach_device": 0
                },
                {
                    "province_code": "42",
                    "province_name": "เลย   ",
                    "latitude": "17.4873658",
                    "longitude": "101.7179794",
                    "reach_device": 0
                },
                {
                    "province_code": "56",
                    "province_name": "พะเยา   ",
                    "latitude": "19.1912606",
                    "longitude": "99.876879",
                    "reach_device": 0
                },
                {
                    "province_code": "60",
                    "province_name": "นครสวรรค์   ",
                    "latitude": "15.6905327",
                    "longitude": "100.1116717",
                    "reach_device": 0
                },
                {
                    "province_code": "64",
                    "province_name": "สุโขทัย   ",
                    "latitude": "17.0057456",
                    "longitude": "99.8242644",
                    "reach_device": 0
                },
                {
                    "province_code": "65",
                    "province_name": "พิษณุโลก   ",
                    "latitude": "16.8262202",
                    "longitude": "100.2579273",
                    "reach_device": 0
                },
                {
                    "province_code": "71",
                    "province_name": "กาญจนบุรี   ",
                    "latitude": "14.0040947",
                    "longitude": "99.5469059",
                    "reach_device": 0
                },
                {
                    "province_code": "72",
                    "province_name": "สุพรรณบุรี   ",
                    "latitude": "14.5137891",
                    "longitude": "100.1279803",
                    "reach_device": 0
                },
                {
                    "province_code": "83",
                    "province_name": "ภูเก็ต   ",
                    "latitude": "7.8901141",
                    "longitude": "98.3964434",
                    "reach_device": 0
                },
                {
                    "province_code": "96",
                    "province_name": "นราธิวาส   ",
                    "latitude": "6.4303443",
                    "longitude": "101.799545",
                    "reach_device": 0
                }
            ],
            "age": [
                {
                    "age": 5,
                    "reach_device": 0
                },
                {
                    "age": 6,
                    "reach_device": 0
                },
                {
                    "age": 7,
                    "reach_device": 0
                },
                {
                    "age": 8,
                    "reach_device": 0
                },
                {
                    "age": 9,
                    "reach_device": 0
                },
                {
                    "age": 10,
                    "reach_device": 1
                },
                {
                    "age": 11,
                    "reach_device": 0
                },
                {
                    "age": 12,
                    "reach_device": 0
                },
                {
                    "age": 13,
                    "reach_device": 0
                },
                {
                    "age": 14,
                    "reach_device": 0
                },
                {
                    "age": 15,
                    "reach_device": 0
                },
                {
                    "age": 16,
                    "reach_device": 0
                },
                {
                    "age": 17,
                    "reach_device": 0
                },
                {
                    "age": 18,
                    "reach_device": 0
                },
                {
                    "age": 19,
                    "reach_device": 1
                },
                {
                    "age": 20,
                    "reach_device": 0
                },
                {
                    "age": 21,
                    "reach_device": 0
                },
                {
                    "age": 22,
                    "reach_device": 0
                },
                {
                    "age": 23,
                    "reach_device": 0
                },
                {
                    "age": 24,
                    "reach_device": 0
                },
                {
                    "age": 25,
                    "reach_device": 0
                },
                {
                    "age": 26,
                    "reach_device": 0
                },
                {
                    "age": 27,
                    "reach_device": 0
                },
                {
                    "age": 28,
                    "reach_device": 0
                },
                {
                    "age": 29,
                    "reach_device": 0
                },
                {
                    "age": 30,
                    "reach_device": 0
                },
                {
                    "age": 31,
                    "reach_device": 0
                },
                {
                    "age": 32,
                    "reach_device": 0
                },
                {
                    "age": 33,
                    "reach_device": 0
                },
                {
                    "age": 34,
                    "reach_device": 0
                },
                {
                    "age": 35,
                    "reach_device": 0
                },
                {
                    "age": 36,
                    "reach_device": 0
                },
                {
                    "age": 37,
                    "reach_device": 0
                },
                {
                    "age": 38,
                    "reach_device": 0
                },
                {
                    "age": 39,
                    "reach_device": 1
                },
                {
                    "age": 40,
                    "reach_device": 0
                },
                {
                    "age": 41,
                    "reach_device": 2
                },
                {
                    "age": 42,
                    "reach_device": 0
                },
                {
                    "age": 43,
                    "reach_device": 0
                },
                {
                    "age": 44,
                    "reach_device": 0
                },
                {
                    "age": 45,
                    "reach_device": 0
                },
                {
                    "age": 46,
                    "reach_device": 0
                },
                {
                    "age": 47,
                    "reach_device": 0
                },
                {
                    "age": 48,
                    "reach_device": 0
                },
                {
                    "age": 49,
                    "reach_device": 0
                },
                {
                    "age": 50,
                    "reach_device": 0
                },
                {
                    "age": 51,
                    "reach_device": 0
                },
                {
                    "age": 52,
                    "reach_device": 0
                },
                {
                    "age": 53,
                    "reach_device": 1
                },
                {
                    "age": 54,
                    "reach_device": 0
                },
                {
                    "age": 55,
                    "reach_device": 1
                },
                {
                    "age": 56,
                    "reach_device": 0
                },
                {
                    "age": 57,
                    "reach_device": 0
                },
                {
                    "age": 58,
                    "reach_device": 0
                },
                {
                    "age": 59,
                    "reach_device": 0
                },
                {
                    "age": 60,
                    "reach_device": 0
                },
                {
                    "age": 61,
                    "reach_device": 0
                },
                {
                    "age": 62,
                    "reach_device": 0
                },
                {
                    "age": 63,
                    "reach_device": 0
                },
                {
                    "age": 64,
                    "reach_device": 0
                },
                {
                    "age": 65,
                    "reach_device": 0
                },
                {
                    "age": 66,
                    "reach_device": 0
                },
                {
                    "age": 67,
                    "reach_device": 0
                },
                {
                    "age": 68,
                    "reach_device": 0
                },
                {
                    "age": 69,
                    "reach_device": 0
                },
                {
                    "age": 70,
                    "reach_device": 0
                },
                {
                    "age": 71,
                    "reach_device": 0
                },
                {
                    "age": 72,
                    "reach_device": 0
                },
                {
                    "age": 73,
                    "reach_device": 0
                },
                {
                    "age": 74,
                    "reach_device": 0
                },
                {
                    "age": 75,
                    "reach_device": 0
                },
                {
                    "age": 76,
                    "reach_device": 0
                },
                {
                    "age": 80,
                    "reach_device": 0
                },
                {
                    "age": 81,
                    "reach_device": 0
                },
                {
                    "age": 86,
                    "reach_device": 0
                },
                {
                    "age": 88,
                    "reach_device": 0
                },
                {
                    "age": 90,
                    "reach_device": 0
                },
                {
                    "age": 91,
                    "reach_device": 0
                },
                {
                    "age": 92,
                    "reach_device": 0
                },
                {
                    "age": 93,
                    "reach_device": 0
                },
                {
                    "age": 94,
                    "reach_device": 0
                },
                {
                    "age": 96,
                    "reach_device": 0
                },
                {
                    "age": 100,
                    "reach_device": 0
                },
                {
                    "age": 106,
                    "reach_device": 0
                },
                {
                    "age": 107,
                    "reach_device": 0
                },
                {
                    "age": 108,
                    "reach_device": 0
                },
                {
                    "age": 109,
                    "reach_device": 0
                },
                {
                    "age": 110,
                    "reach_device": 0
                },
                {
                    "age": 111,
                    "reach_device": 0
                },
                {
                    "age": 112,
                    "reach_device": 0
                },
                {
                    "age": 113,
                    "reach_device": 0
                },
                {
                    "age": 119,
                    "reach_device": 0
                },
                {
                    "age": "ระบุไม่ได้",
                    "reach_device": 62
                }
            ],
            "total_devices": 72
        }
    ],
    "result_code": "200",
    "result_desc": "success"
}