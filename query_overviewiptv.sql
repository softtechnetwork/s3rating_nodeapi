/*

CREATE : temp TABLE iptv */

CREATE TABLE #temp_table_iptv (id int primary key , devices_id int, tvchannels_id int, chip_code varchar(100) , view_seconds int , ip_address varchar(50) , startview_datetime datetime , created datetime ) /*
INSERT :
INSERT INTO temp table*/
INSERT INTO #temp_table_iptv
SELECT  rating_data_2022_2.*
FROM S3Application.dbo.rating_data_2022_2
WHERE startview_datetime >= '2022-02-20 00:00:01'
AND startview_datetime <= '2022-02-20 23:59:59' /* filter data before

CREATE temp*/
SELECT  * INTO #temp_data_iptv
FROM
(
	SELECT  tvchannels_id
	       ,devices_id
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 00:00:00' AND startview_datetime < '2022-02-20 00:30:00' THEN 1 END ) AS 'd_0_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 00:30:00' AND startview_datetime < '2022-02-20 01:00:00' THEN 1 END ) AS 'd_0_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 01:00:00' AND startview_datetime < '2022-02-20 01:30:00' THEN 1 END ) AS 'd_1_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 01:30:00' AND startview_datetime < '2022-02-20 02:00:00' THEN 1 END ) AS 'd_1_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 02:00:00' AND startview_datetime < '2022-02-20 02:30:00' THEN 1 END ) AS 'd_2_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 02:30:00' AND startview_datetime < '2022-02-20 03:00:00' THEN 1 END ) AS 'd_2_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 03:00:00' AND startview_datetime < '2022-02-20 03:30:00' THEN 1 END ) AS 'd_3_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 03:30:00' AND startview_datetime < '2022-02-20 04:00:00' THEN 1 END ) AS 'd_3_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 04:00:00' AND startview_datetime < '2022-02-20 04:30:00' THEN 1 END ) AS 'd_4_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 04:30:00' AND startview_datetime < '2022-02-20 05:00:00' THEN 1 END ) AS 'd_4_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 05:00:00' AND startview_datetime < '2022-02-20 05:30:00' THEN 1 END ) AS 'd_5_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 05:30:00' AND startview_datetime < '2022-02-20 06:00:00' THEN 1 END ) AS 'd_5_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 06:00:00' AND startview_datetime < '2022-02-20 06:30:00' THEN 1 END ) AS 'd_6_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 06:30:00' AND startview_datetime < '2022-02-20 07:00:00' THEN 1 END ) AS 'd_6_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 07:00:00' AND startview_datetime < '2022-02-20 07:30:00' THEN 1 END ) AS 'd_7_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 07:30:00' AND startview_datetime < '2022-02-20 08:00:00' THEN 1 END ) AS 'd_7_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 08:00:00' AND startview_datetime < '2022-02-20 08:30:00' THEN 1 END ) AS 'd_8_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 08:30:00' AND startview_datetime < '2022-02-20 09:00:00' THEN 1 END ) AS 'd_8_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 09:00:00' AND startview_datetime < '2022-02-20 09:30:00' THEN 1 END ) AS 'd_9_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 09:30:00' AND startview_datetime < '2022-02-20 10:00:00' THEN 1 END ) AS 'd_9_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 10:00:00' AND startview_datetime < '2022-02-20 10:30:00' THEN 1 END ) AS 'd_10_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 10:30:00' AND startview_datetime < '2022-02-20 11:00:00' THEN 1 END ) AS 'd_10_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 11:00:00' AND startview_datetime < '2022-02-20 11:30:00' THEN 1 END ) AS 'd_11_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 11:30:00' AND startview_datetime < '2022-02-20 12:00:00' THEN 1 END ) AS 'd_11_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 12:00:00' AND startview_datetime < '2022-02-20 12:30:00' THEN 1 END ) AS 'd_12_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 12:30:00' AND startview_datetime < '2022-02-20 13:00:00' THEN 1 END ) AS 'd_12_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 13:00:00' AND startview_datetime < '2022-02-20 13:30:00' THEN 1 END ) AS 'd_13_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 13:30:00' AND startview_datetime < '2022-02-20 14:00:00' THEN 1 END ) AS 'd_13_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 14:00:00' AND startview_datetime < '2022-02-20 14:30:00' THEN 1 END ) AS 'd_14_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 14:30:00' AND startview_datetime < '2022-02-20 15:00:00' THEN 1 END ) AS 'd_14_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 15:00:00' AND startview_datetime < '2022-02-20 15:30:00' THEN 1 END ) AS 'd_15_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 15:30:00' AND startview_datetime < '2022-02-20 16:00:00' THEN 1 END ) AS 'd_15_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 16:00:00' AND startview_datetime < '2022-02-20 16:30:00' THEN 1 END ) AS 'd_16_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 16:30:00' AND startview_datetime < '2022-02-20 17:00:00' THEN 1 END ) AS 'd_16_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 17:00:00' AND startview_datetime < '2022-02-20 17:30:00' THEN 1 END ) AS 'd_17_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 17:30:00' AND startview_datetime < '2022-02-20 18:00:00' THEN 1 END ) AS 'd_17_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 18:00:00' AND startview_datetime < '2022-02-20 18:30:00' THEN 1 END ) AS 'd_18_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 18:30:00' AND startview_datetime < '2022-02-20 19:00:00' THEN 1 END ) AS 'd_18_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 19:00:00' AND startview_datetime < '2022-02-20 19:30:00' THEN 1 END ) AS 'd_19_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 19:30:00' AND startview_datetime < '2022-02-20 20:00:00' THEN 1 END ) AS 'd_19_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 20:00:00' AND startview_datetime < '2022-02-20 20:30:00' THEN 1 END ) AS 'd_20_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 20:30:00' AND startview_datetime < '2022-02-20 21:00:00' THEN 1 END ) AS 'd_20_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 21:00:00' AND startview_datetime < '2022-02-20 21:30:00' THEN 1 END ) AS 'd_21_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 21:30:00' AND startview_datetime < '2022-02-20 22:00:00' THEN 1 END ) AS 'd_21_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 22:00:00' AND startview_datetime < '2022-02-20 22:30:00' THEN 1 END ) AS 'd_22_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 22:30:00' AND startview_datetime < '2022-02-20 23:00:00' THEN 1 END ) AS 'd_22_30'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 23:00:00' AND startview_datetime < '2022-02-20 23:30:00' THEN 1 END ) AS 'd_23_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-20 23:30:00' AND startview_datetime < '2022-02-21 00:00:00' THEN 1 END ) AS 'd_23_30'
	FROM S3Application.dbo.#temp_table_iptv
	WHERE startview_datetime >= '2022-02-20 00:00:01'
	AND startview_datetime <= '2022-02-20 23:59:59'
	AND tvchannels_id > 0
	GROUP BY  tvchannels_id
	         ,devices_id
) AS r1 /* crete: temp gender */
SELECT  * into #temp_gender
FROM
(
	SELECT  r2.devices_id AS dvid
	       ,r2.gender
	FROM
	(
		SELECT  distinct devices_id
		       ,(
		SELECT  top 1 gender
		FROM device_users AS dx
		WHERE device_users.devices_id = dx.devices_id ) AS gender
		FROM device_users
	) AS r2
	GROUP BY  r2.devices_id
	         ,r2.gender
) r1 /* temp gender*/ /* query : get overview iptv gender*/
SELECT  (CASE WHEN #temp_gender.gender IS NULL THEN 'none' WHEN #temp_gender.gender = '' THEN 'none1' WHEN #temp_gender.gender = '-' THEN 'none2' ELSE #temp_gender.gender END) AS gender
       ,COUNT(case WHEN d_0_0 >= 1 THEN 1 end)   AS 'field_0_0'
       ,COUNT(case WHEN d_0_30 >= 1 THEN 1 end)  AS 'field_0_30'
       ,COUNT(case WHEN d_1_0 >= 1 THEN 1 end)   AS 'field_1_0'
       ,COUNT(case WHEN d_1_30 >= 1 THEN 1 end)  AS 'field_1_30'
       ,COUNT(case WHEN d_2_0 >= 1 THEN 1 end)   AS 'field_2_0'
       ,COUNT(case WHEN d_2_30 >= 1 THEN 1 end)  AS 'field_2_30'
       ,COUNT(case WHEN d_3_0 >= 1 THEN 1 end)   AS 'field_3_0'
       ,COUNT(case WHEN d_3_30 >= 1 THEN 1 end)  AS 'field_3_30'
       ,COUNT(case WHEN d_4_0 >= 1 THEN 1 end)   AS 'field_4_0'
       ,COUNT(case WHEN d_4_30 >= 1 THEN 1 end)  AS 'field_4_30'
       ,COUNT(case WHEN d_5_0 >= 1 THEN 1 end)   AS 'field_5_0'
       ,COUNT(case WHEN d_5_30 >= 1 THEN 1 end)  AS 'field_5_30'
       ,COUNT(case WHEN d_6_0 >= 1 THEN 1 end)   AS 'field_6_0'
       ,COUNT(case WHEN d_6_30 >= 1 THEN 1 end)  AS 'field_6_30'
       ,COUNT(case WHEN d_7_0 >= 1 THEN 1 end)   AS 'field_7_0'
       ,COUNT(case WHEN d_7_30 >= 1 THEN 1 end)  AS 'field_7_30'
       ,COUNT(case WHEN d_8_0 >= 1 THEN 1 end)   AS 'field_8_0'
       ,COUNT(case WHEN d_8_30 >= 1 THEN 1 end)  AS 'field_8_30'
       ,COUNT(case WHEN d_9_0 >= 1 THEN 1 end)   AS 'field_9_0'
       ,COUNT(case WHEN d_9_30 >= 1 THEN 1 end)  AS 'field_9_30'
       ,COUNT(case WHEN d_10_0 >= 1 THEN 1 end)  AS 'field_10_0'
       ,COUNT(case WHEN d_10_30 >= 1 THEN 1 end) AS 'field_10_30'
       ,COUNT(case WHEN d_11_0 >= 1 THEN 1 end)  AS 'field_11_0'
       ,COUNT(case WHEN d_11_30 >= 1 THEN 1 end) AS 'field_11_30'
       ,COUNT(case WHEN d_12_0 >= 1 THEN 1 end)  AS 'field_12_0'
       ,COUNT(case WHEN d_12_30 >= 1 THEN 1 end) AS 'field_12_30'
       ,COUNT(case WHEN d_13_0 >= 1 THEN 1 end)  AS 'field_13_0'
       ,COUNT(case WHEN d_13_30 >= 1 THEN 1 end) AS 'field_13_30'
       ,COUNT(case WHEN d_14_0 >= 1 THEN 1 end)  AS 'field_14_0'
       ,COUNT(case WHEN d_14_30 >= 1 THEN 1 end) AS 'field_14_30'
       ,COUNT(case WHEN d_15_0 >= 1 THEN 1 end)  AS 'field_15_0'
       ,COUNT(case WHEN d_15_30 >= 1 THEN 1 end) AS 'field_15_30'
       ,COUNT(case WHEN d_16_0 >= 1 THEN 1 end)  AS 'field_16_0'
       ,COUNT(case WHEN d_16_30 >= 1 THEN 1 end) AS 'field_16_30'
       ,COUNT(case WHEN d_17_0 >= 1 THEN 1 end)  AS 'field_17_0'
       ,COUNT(case WHEN d_17_30 >= 1 THEN 1 end) AS 'field_17_30'
       ,COUNT(case WHEN d_18_0 >= 1 THEN 1 end)  AS 'field_18_0'
       ,COUNT(case WHEN d_18_30 >= 1 THEN 1 end) AS 'field_18_30'
       ,COUNT(case WHEN d_19_0 >= 1 THEN 1 end)  AS 'field_19_0'
       ,COUNT(case WHEN d_19_30 >= 1 THEN 1 end) AS 'field_19_30'
       ,COUNT(case WHEN d_20_0 >= 1 THEN 1 end)  AS 'field_20_0'
       ,COUNT(case WHEN d_20_30 >= 1 THEN 1 end) AS 'field_20_30'
       ,COUNT(case WHEN d_21_0 >= 1 THEN 1 end)  AS 'field_21_0'
       ,COUNT(case WHEN d_21_30 >= 1 THEN 1 end) AS 'field_21_30'
       ,COUNT(case WHEN d_22_0 >= 1 THEN 1 end)  AS 'field_22_0'
       ,COUNT(case WHEN d_22_30 >= 1 THEN 1 end) AS 'field_22_30'
       ,COUNT(case WHEN d_23_0 >= 1 THEN 1 end)  AS 'field_23_0'
       ,COUNT(case WHEN d_23_30 >= 1 THEN 1 end) AS 'field_23_30'
       ,tvchannels_id
       ,COUNT(*)                                 AS total_row
FROM #temp_data_iptv
LEFT JOIN #temp_gender
ON #temp_data_iptv.devices_id = #temp_gender.dvid
GROUP BY  gender
         ,tvchannels_id
ORDER BY tvchannels_id asc
         ,#temp_gender.gender asc 
         
         
         /* overview gender iptv */
SELECT  tvchannels_id
       ,(CASE WHEN province_region IS NULL THEN 'none' WHEN province_region = '' THEN 'none1' WHEN province_region = '-' THEN 'none2' ELSE province_region END) AS province_region
       ,COUNT(case WHEN d_0_0 >= 1 THEN 1 end)   AS 'field_0_0'
       ,COUNT(case WHEN d_0_30 >= 1 THEN 1 end)  AS 'field_0_30'
       ,COUNT(case WHEN d_1_0 >= 1 THEN 1 end)   AS 'field_1_0'
       ,COUNT(case WHEN d_1_30 >= 1 THEN 1 end)  AS 'field_1_30'
       ,COUNT(case WHEN d_2_0 >= 1 THEN 1 end)   AS 'field_2_0'
       ,COUNT(case WHEN d_2_30 >= 1 THEN 1 end)  AS 'field_2_30'
       ,COUNT(case WHEN d_3_0 >= 1 THEN 1 end)   AS 'field_3_0'
       ,COUNT(case WHEN d_3_30 >= 1 THEN 1 end)  AS 'field_3_30'
       ,COUNT(case WHEN d_4_0 >= 1 THEN 1 end)   AS 'field_4_0'
       ,COUNT(case WHEN d_4_30 >= 1 THEN 1 end)  AS 'field_4_30'
       ,COUNT(case WHEN d_5_0 >= 1 THEN 1 end)   AS 'field_5_0'
       ,COUNT(case WHEN d_5_30 >= 1 THEN 1 end)  AS 'field_5_30'
       ,COUNT(case WHEN d_6_0 >= 1 THEN 1 end)   AS 'field_6_0'
       ,COUNT(case WHEN d_6_30 >= 1 THEN 1 end)  AS 'field_6_30'
       ,COUNT(case WHEN d_7_0 >= 1 THEN 1 end)   AS 'field_7_0'
       ,COUNT(case WHEN d_7_30 >= 1 THEN 1 end)  AS 'field_7_30'
       ,COUNT(case WHEN d_8_0 >= 1 THEN 1 end)   AS 'field_8_0'
       ,COUNT(case WHEN d_8_30 >= 1 THEN 1 end)  AS 'field_8_30'
       ,COUNT(case WHEN d_9_0 >= 1 THEN 1 end)   AS 'field_9_0'
       ,COUNT(case WHEN d_9_30 >= 1 THEN 1 end)  AS 'field_9_30'
       ,COUNT(case WHEN d_10_0 >= 1 THEN 1 end)  AS 'field_10_0'
       ,COUNT(case WHEN d_10_30 >= 1 THEN 1 end) AS 'field_10_30'
       ,COUNT(case WHEN d_11_0 >= 1 THEN 1 end)  AS 'field_11_0'
       ,COUNT(case WHEN d_11_30 >= 1 THEN 1 end) AS 'field_11_30'
       ,COUNT(case WHEN d_12_0 >= 1 THEN 1 end)  AS 'field_12_0'
       ,COUNT(case WHEN d_12_30 >= 1 THEN 1 end) AS 'field_12_30'
       ,COUNT(case WHEN d_13_0 >= 1 THEN 1 end)  AS 'field_13_0'
       ,COUNT(case WHEN d_13_30 >= 1 THEN 1 end) AS 'field_13_30'
       ,COUNT(case WHEN d_14_0 >= 1 THEN 1 end)  AS 'field_14_0'
       ,COUNT(case WHEN d_14_30 >= 1 THEN 1 end) AS 'field_14_30'
       ,COUNT(case WHEN d_15_0 >= 1 THEN 1 end)  AS 'field_15_0'
       ,COUNT(case WHEN d_15_30 >= 1 THEN 1 end) AS 'field_15_30'
       ,COUNT(case WHEN d_16_0 >= 1 THEN 1 end)  AS 'field_16_0'
       ,COUNT(case WHEN d_16_30 >= 1 THEN 1 end) AS 'field_16_30'
       ,COUNT(case WHEN d_17_0 >= 1 THEN 1 end)  AS 'field_17_0'
       ,COUNT(case WHEN d_17_30 >= 1 THEN 1 end) AS 'field_17_30'
       ,COUNT(case WHEN d_18_0 >= 1 THEN 1 end)  AS 'field_18_0'
       ,COUNT(case WHEN d_18_30 >= 1 THEN 1 end) AS 'field_18_30'
       ,COUNT(case WHEN d_19_0 >= 1 THEN 1 end)  AS 'field_19_0'
       ,COUNT(case WHEN d_19_30 >= 1 THEN 1 end) AS 'field_19_30'
       ,COUNT(case WHEN d_20_0 >= 1 THEN 1 end)  AS 'field_20_0'
       ,COUNT(case WHEN d_20_30 >= 1 THEN 1 end) AS 'field_20_30'
       ,COUNT(case WHEN d_21_0 >= 1 THEN 1 end)  AS 'field_21_0'
       ,COUNT(case WHEN d_21_30 >= 1 THEN 1 end) AS 'field_21_30'
       ,COUNT(case WHEN d_22_0 >= 1 THEN 1 end)  AS 'field_22_0'
       ,COUNT(case WHEN d_22_30 >= 1 THEN 1 end) AS 'field_22_30'
       ,COUNT(case WHEN d_23_0 >= 1 THEN 1 end)  AS 'field_23_0'
       ,COUNT(case WHEN d_23_30 >= 1 THEN 1 end) AS 'field_23_30'
FROM #temp_data_iptv
LEFT JOIN device_addresses
ON #temp_data_iptv.devices_id = device_addresses.devices_id
GROUP BY  province_region
         ,#temp_data_iptv.tvchannels_id



/* =============== get : iptv ================================== */
SELECT  tvchannels_id
       ,region_code
       ,provinces.province_name
       ,COUNT(case WHEN d_0_0 >= 1 THEN 1 end)   AS 'field_0_0'
       ,COUNT(case WHEN d_0_30 >= 1 THEN 1 end)  AS 'field_0_30'
       ,COUNT(case WHEN d_1_0 >= 1 THEN 1 end)   AS 'field_1_0'
       ,COUNT(case WHEN d_1_30 >= 1 THEN 1 end)  AS 'field_1_30'
       ,COUNT(case WHEN d_2_0 >= 1 THEN 1 end)   AS 'field_2_0'
       ,COUNT(case WHEN d_2_30 >= 1 THEN 1 end)  AS 'field_2_30'
       ,COUNT(case WHEN d_3_0 >= 1 THEN 1 end)   AS 'field_3_0'
       ,COUNT(case WHEN d_3_30 >= 1 THEN 1 end)  AS 'field_3_30'
       ,COUNT(case WHEN d_4_0 >= 1 THEN 1 end)   AS 'field_4_0'
       ,COUNT(case WHEN d_4_30 >= 1 THEN 1 end)  AS 'field_4_30'
       ,COUNT(case WHEN d_5_0 >= 1 THEN 1 end)   AS 'field_5_0'
       ,COUNT(case WHEN d_5_30 >= 1 THEN 1 end)  AS 'field_5_30'
       ,COUNT(case WHEN d_6_0 >= 1 THEN 1 end)   AS 'field_6_0'
       ,COUNT(case WHEN d_6_30 >= 1 THEN 1 end)  AS 'field_6_30'
       ,COUNT(case WHEN d_7_0 >= 1 THEN 1 end)   AS 'field_7_0'
       ,COUNT(case WHEN d_7_30 >= 1 THEN 1 end)  AS 'field_7_30'
       ,COUNT(case WHEN d_8_0 >= 1 THEN 1 end)   AS 'field_8_0'
       ,COUNT(case WHEN d_8_30 >= 1 THEN 1 end)  AS 'field_8_30'
       ,COUNT(case WHEN d_9_0 >= 1 THEN 1 end)   AS 'field_9_0'
       ,COUNT(case WHEN d_9_30 >= 1 THEN 1 end)  AS 'field_9_30'
       ,COUNT(case WHEN d_10_0 >= 1 THEN 1 end)  AS 'field_10_0'
       ,COUNT(case WHEN d_10_30 >= 1 THEN 1 end) AS 'field_10_30'
       ,COUNT(case WHEN d_11_0 >= 1 THEN 1 end)  AS 'field_11_0'
       ,COUNT(case WHEN d_11_30 >= 1 THEN 1 end) AS 'field_11_30'
       ,COUNT(case WHEN d_12_0 >= 1 THEN 1 end)  AS 'field_12_0'
       ,COUNT(case WHEN d_12_30 >= 1 THEN 1 end) AS 'field_12_30'
       ,COUNT(case WHEN d_13_0 >= 1 THEN 1 end)  AS 'field_13_0'
       ,COUNT(case WHEN d_13_30 >= 1 THEN 1 end) AS 'field_13_30'
       ,COUNT(case WHEN d_14_0 >= 1 THEN 1 end)  AS 'field_14_0'
       ,COUNT(case WHEN d_14_30 >= 1 THEN 1 end) AS 'field_14_30'
       ,COUNT(case WHEN d_15_0 >= 1 THEN 1 end)  AS 'field_15_0'
       ,COUNT(case WHEN d_15_30 >= 1 THEN 1 end) AS 'field_15_30'
       ,COUNT(case WHEN d_16_0 >= 1 THEN 1 end)  AS 'field_16_0'
       ,COUNT(case WHEN d_16_30 >= 1 THEN 1 end) AS 'field_16_30'
       ,COUNT(case WHEN d_17_0 >= 1 THEN 1 end)  AS 'field_17_0'
       ,COUNT(case WHEN d_17_30 >= 1 THEN 1 end) AS 'field_17_30'
       ,COUNT(case WHEN d_18_0 >= 1 THEN 1 end)  AS 'field_18_0'
       ,COUNT(case WHEN d_18_30 >= 1 THEN 1 end) AS 'field_18_30'
       ,COUNT(case WHEN d_19_0 >= 1 THEN 1 end)  AS 'field_19_0'
       ,COUNT(case WHEN d_19_30 >= 1 THEN 1 end) AS 'field_19_30'
       ,COUNT(case WHEN d_20_0 >= 1 THEN 1 end)  AS 'field_20_0'
       ,COUNT(case WHEN d_20_30 >= 1 THEN 1 end) AS 'field_20_30'
       ,COUNT(case WHEN d_21_0 >= 1 THEN 1 end)  AS 'field_21_0'
       ,COUNT(case WHEN d_21_30 >= 1 THEN 1 end) AS 'field_21_30'
       ,COUNT(case WHEN d_22_0 >= 1 THEN 1 end)  AS 'field_22_0'
       ,COUNT(case WHEN d_22_30 >= 1 THEN 1 end) AS 'field_22_30'
       ,COUNT(case WHEN d_23_0 >= 1 THEN 1 end)  AS 'field_23_0'
       ,COUNT(case WHEN d_23_30 >= 1 THEN 1 end) AS 'field_23_30'
FROM #temp_data_iptv
LEFT JOIN device_addresses
ON #temp_data_iptv.devices_id = device_addresses.devices_id
INNER JOIN provinces
ON provinces.province_code = device_addresses.region_code AND device_addresses.province_region IS NOT NULL AND device_addresses.province_region != '' AND device_addresses.province_region != '-' AND region_code != '' AND region_code is not null AND province_name is not null AND region_code > 0
GROUP BY  #temp_data_iptv.tvchannels_id
         ,provinces.province_name
         ,region_code