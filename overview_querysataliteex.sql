CREATE TABLE #temp_table_satalite (id int primary key , devices_id int, channels_id int, ship_code varchar(100) , frq varchar(50) , sym varchar(50) , pol varchar(50) , server_id varchar(50) , vdo_pid varchar(50) , ado_pid varchar(50) , view_seconds int , ip_address varchar(50) , startview_datetime datetime , channel_ascii_code int , created datetime )
INSERT INTO #temp_table_satalite
SELECT  rating_data_2022_2.*
FROM rating_data_2022_2
WHERE startview_datetime >= '2022-02-19 00:00:01'
AND startview_datetime <= '2022-02-19 23:59:59'
SELECT  channels.id                                                                                                        AS channels_id
       ,merge_s3remote_id
       ,r1.hour                                                                                                            AS hour
       ,r1.MINUTE                                                                                                          AS minute
       ,CONVERT(varchar(12),merge_s3remote_id) + '_'+(CONVERT(varchar(12),r1.hour) + '_' + CONVERT(varchar(12),r1.MINUTE)) AS h_m
       ,COUNT(case WHEN r1.d_142296 >= 1 THEN 1 end)                                                                       AS 'field_142296'
       ,COUNT(case WHEN r1.d_142297 >= 1 THEN 1 end)                                                                       AS 'field_142297'
       ,COUNT(case WHEN r1.d_142298 >= 1 THEN 1 end)                                                                       AS 'field_142298'
       ,COUNT(case WHEN r1.d_142299 >= 1 THEN 1 end)                                                                       AS 'field_142299'
       ,COUNT(case WHEN r1.d_142300 >= 1 THEN 1 end)                                                                       AS 'field_142300'
       ,COUNT(case WHEN r1.d_142301 >= 1 THEN 1 end)                                                                       AS 'field_142301'
       ,COUNT(case WHEN r1.d_142302 >= 1 THEN 1 end)                                                                       AS 'field_142302'
       ,COUNT(case WHEN r1.d_142303 >= 1 THEN 1 end)                                                                       AS 'field_142303'
       ,COUNT(case WHEN r1.d_142304 >= 1 THEN 1 end)                                                                       AS 'field_142304'
       ,COUNT(case WHEN r1.d_142305 >= 1 THEN 1 end)                                                                       AS 'field_142305'
       ,COUNT(case WHEN r1.d_142306 >= 1 THEN 1 end)                                                                       AS 'field_142306'
       ,COUNT(case WHEN r1.d_142307 >= 1 THEN 1 end)                                                                       AS 'field_142307'
       ,COUNT(case WHEN r1.d_142308 >= 1 THEN 1 end)                                                                       AS 'field_142308'
       ,COUNT(case WHEN r1.d_142309 >= 1 THEN 1 end)                                                                       AS 'field_142309'
       ,COUNT(case WHEN r1.d_142310 >= 1 THEN 1 end)                                                                       AS 'field_142310'
       ,COUNT(case WHEN r1.d_142311 >= 1 THEN 1 end)                                                                       AS 'field_142311'
       ,COUNT(case WHEN r1.d_142312 >= 1 THEN 1 end)                                                                       AS 'field_142312'
       ,COUNT(case WHEN r1.d_142313 >= 1 THEN 1 end)                                                                       AS 'field_142313'
       ,COUNT(case WHEN r1.d_142314 >= 1 THEN 1 end)                                                                       AS 'field_142314'
       ,COUNT(case WHEN r1.d_142315 >= 1 THEN 1 end)                                                                       AS 'field_142315'
       ,COUNT(case WHEN r1.d_142316 >= 1 THEN 1 end)                                                                       AS 'field_142316'
       ,COUNT(case WHEN r1.d_142317 >= 1 THEN 1 end)                                                                       AS 'field_142317'
       ,COUNT(case WHEN r1.d_142318 >= 1 THEN 1 end)                                                                       AS 'field_142318'
       ,COUNT(case WHEN r1.d_142319 >= 1 THEN 1 end)                                                                       AS 'field_142319'
       ,COUNT(case WHEN r1.d_142320 >= 1 THEN 1 end)                                                                       AS 'field_142320'
       ,COUNT(case WHEN r1.d_142321 >= 1 THEN 1 end)                                                                       AS 'field_142321'
       ,COUNT(case WHEN r1.d_142322 >= 1 THEN 1 end)                                                                       AS 'field_142322'
       ,COUNT(case WHEN r1.d_142323 >= 1 THEN 1 end)                                                                       AS 'field_142323'
       ,COUNT(case WHEN r1.d_142324 >= 1 THEN 1 end)                                                                       AS 'field_142324'
       ,COUNT(case WHEN r1.d_142325 >= 1 THEN 1 end)                                                                       AS 'field_142325'
       ,COUNT(case WHEN r1.d_142326 >= 1 THEN 1 end)                                                                       AS 'field_142326'
       ,COUNT(case WHEN r1.d_142327 >= 1 THEN 1 end)                                                                       AS 'field_142327'
       ,COUNT(case WHEN r1.d_142328 >= 1 THEN 1 end)                                                                       AS 'field_142328'
       ,COUNT(case WHEN r1.d_142329 >= 1 THEN 1 end)                                                                       AS 'field_142329'
       ,COUNT(case WHEN r1.d_142330 >= 1 THEN 1 end)                                                                       AS 'field_142330'
       ,COUNT(case WHEN r1.d_142331 >= 1 THEN 1 end)                                                                       AS 'field_142331'
       ,COUNT(case WHEN r1.d_142332 >= 1 THEN 1 end)                                                                       AS 'field_142332'
       ,COUNT(case WHEN r1.d_142333 >= 1 THEN 1 end)                                                                       AS 'field_142333'
       ,COUNT(case WHEN r1.d_142334 >= 1 THEN 1 end)                                                                       AS 'field_142334'
       ,COUNT(case WHEN r1.d_142335 >= 1 THEN 1 end)                                                                       AS 'field_142335'
       ,COUNT(case WHEN r1.d_142336 >= 1 THEN 1 end)                                                                       AS 'field_142336'
       ,COUNT(case WHEN r1.d_142337 >= 1 THEN 1 end)                                                                       AS 'field_142337'
FROM
(
	SELECT  DATEPART(HOUR,startview_datetime)                                                                                      AS hour
	       ,CAST(DATEPART(MINUTE,startview_datetime)/30 AS char(2))                                                                AS MINUTE
	       ,devices_id
	       ,COUNT(*)                                                                                                               AS count_allratingrecord
	       ,channels_id
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 00:00:00' AND startview_datetime < '2022-02-19 01:00:00' THEN 1 END ) AS 'd_142296'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 05:00:00' AND startview_datetime < '2022-02-19 05:30:00' THEN 1 END ) AS 'd_142297'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 05:30:00' AND startview_datetime < '2022-02-19 05:55:00' THEN 1 END ) AS 'd_142298'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 05:55:00' AND startview_datetime < '2022-02-19 06:00:00' THEN 1 END ) AS 'd_142299'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:00:00' AND startview_datetime < '2022-02-19 06:05:00' THEN 1 END ) AS 'd_142300'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:05:00' AND startview_datetime < '2022-02-19 06:15:00' THEN 1 END ) AS 'd_142301'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:15:00' AND startview_datetime < '2022-02-19 06:25:00' THEN 1 END ) AS 'd_142302'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:25:00' AND startview_datetime < '2022-02-19 06:30:00' THEN 1 END ) AS 'd_142303'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:30:00' AND startview_datetime < '2022-02-19 06:50:00' THEN 1 END ) AS 'd_142304'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:50:00' AND startview_datetime < '2022-02-19 07:00:00' THEN 1 END ) AS 'd_142305'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:00:00' AND startview_datetime < '2022-02-19 07:05:00' THEN 1 END ) AS 'd_142306'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:15:00' AND startview_datetime < '2022-02-19 07:20:00' THEN 1 END ) AS 'd_142307'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:20:00' AND startview_datetime < '2022-02-19 07:40:00' THEN 1 END ) AS 'd_142308'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:40:00' AND startview_datetime < '2022-02-19 08:00:00' THEN 1 END ) AS 'd_142309'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 08:00:00' AND startview_datetime < '2022-02-19 08:05:00' THEN 1 END ) AS 'd_142310'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 08:05:00' AND startview_datetime < '2022-02-19 08:30:00' THEN 1 END ) AS 'd_142311'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 08:30:00' AND startview_datetime < '2022-02-19 10:00:00' THEN 1 END ) AS 'd_142312'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 10:00:00' AND startview_datetime < '2022-02-19 10:30:00' THEN 1 END ) AS 'd_142313'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 10:30:00' AND startview_datetime < '2022-02-19 10:55:00' THEN 1 END ) AS 'd_142314'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 10:55:00' AND startview_datetime < '2022-02-19 11:00:00' THEN 1 END ) AS 'd_142315'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 11:00:00' AND startview_datetime < '2022-02-19 11:30:00' THEN 1 END ) AS 'd_142316'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 11:30:00' AND startview_datetime < '2022-02-19 12:00:00' THEN 1 END ) AS 'd_142317'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 12:00:00' AND startview_datetime < '2022-02-19 13:00:00' THEN 1 END ) AS 'd_142318'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 13:00:00' AND startview_datetime < '2022-02-19 13:45:00' THEN 1 END ) AS 'd_142319'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 13:45:00' AND startview_datetime < '2022-02-19 14:00:00' THEN 1 END ) AS 'd_142320'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 14:00:00' AND startview_datetime < '2022-02-19 14:05:00' THEN 1 END ) AS 'd_142321'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 14:05:00' AND startview_datetime < '2022-02-19 14:50:00' THEN 1 END ) AS 'd_142322'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 14:50:00' AND startview_datetime < '2022-02-19 15:00:00' THEN 1 END ) AS 'd_142323'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 15:00:00' AND startview_datetime < '2022-02-19 16:00:00' THEN 1 END ) AS 'd_142324'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 16:00:00' AND startview_datetime < '2022-02-19 16:05:00' THEN 1 END ) AS 'd_142325'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 16:05:00' AND startview_datetime < '2022-02-19 16:30:00' THEN 1 END ) AS 'd_142326'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 16:30:00' AND startview_datetime < '2022-02-19 17:00:00' THEN 1 END ) AS 'd_142327'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 17:00:00' AND startview_datetime < '2022-02-19 17:05:00' THEN 1 END ) AS 'd_142328'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 17:05:00' AND startview_datetime < '2022-02-19 17:30:00' THEN 1 END ) AS 'd_142329'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 17:30:00' AND startview_datetime < '2022-02-19 18:00:00' THEN 1 END ) AS 'd_142330'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 18:00:00' AND startview_datetime < '2022-02-19 20:15:00' THEN 1 END ) AS 'd_142331'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 20:15:00' AND startview_datetime < '2022-02-19 21:10:00' THEN 1 END ) AS 'd_142332'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 21:10:00' AND startview_datetime < '2022-02-19 22:00:00' THEN 1 END ) AS 'd_142333'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 22:00:00' AND startview_datetime < '2022-02-19 22:05:00' THEN 1 END ) AS 'd_142334'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 22:05:00' AND startview_datetime < '2022-02-19 23:00:00' THEN 1 END ) AS 'd_142335'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 23:00:00' AND startview_datetime < '2022-02-19 23:30:00' THEN 1 END ) AS 'd_142336'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 23:30:00' AND startview_datetime < '2022-02-19 23:59:00' THEN 1 END ) AS 'd_142337'
	FROM #temp_table_satalite
	WHERE startview_datetime >= '2022-02-19 00:00:01'
	AND startview_datetime <= '2022-02-19 23:59:59'
	AND channels_id > 0
	GROUP BY  channels_id
	         ,DATEPART(YEAR,startview_datetime)
	         ,DATEPART(MONTH,startview_datetime)
	         ,DATEPART(DAY,startview_datetime)
	         ,DATEPART(HOUR,startview_datetime)
	         ,DATEPART(MINUTE,startview_datetime)/30
	         ,devices_id
)r1
INNER JOIN channels
ON r1.channels_id = channels.id
WHERE channels.active = 1
GROUP BY  r1.hour
         ,r1.MINUTE
         ,channels.id
         ,channels.merge_s3remote_id
ORDER BY r1.hour
         ,r1.MINUTE /*
SELECT  into temp data */
SELECT  * INTO #temp_data
FROM
(
	SELECT  channels_id
	       ,devices_id
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 00:00:00' AND startview_datetime < '2022-02-19 01:00:00' THEN 1 END ) AS 'd_142296'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 05:00:00' AND startview_datetime < '2022-02-19 05:30:00' THEN 1 END ) AS 'd_142297'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 05:30:00' AND startview_datetime < '2022-02-19 05:55:00' THEN 1 END ) AS 'd_142298'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 05:55:00' AND startview_datetime < '2022-02-19 06:00:00' THEN 1 END ) AS 'd_142299'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:00:00' AND startview_datetime < '2022-02-19 06:05:00' THEN 1 END ) AS 'd_142300'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:05:00' AND startview_datetime < '2022-02-19 06:15:00' THEN 1 END ) AS 'd_142301'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:15:00' AND startview_datetime < '2022-02-19 06:25:00' THEN 1 END ) AS 'd_142302'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:25:00' AND startview_datetime < '2022-02-19 06:30:00' THEN 1 END ) AS 'd_142303'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:30:00' AND startview_datetime < '2022-02-19 06:50:00' THEN 1 END ) AS 'd_142304'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:50:00' AND startview_datetime < '2022-02-19 07:00:00' THEN 1 END ) AS 'd_142305'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:00:00' AND startview_datetime < '2022-02-19 07:05:00' THEN 1 END ) AS 'd_142306'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:15:00' AND startview_datetime < '2022-02-19 07:20:00' THEN 1 END ) AS 'd_142307'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:20:00' AND startview_datetime < '2022-02-19 07:40:00' THEN 1 END ) AS 'd_142308'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:40:00' AND startview_datetime < '2022-02-19 08:00:00' THEN 1 END ) AS 'd_142309'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 08:00:00' AND startview_datetime < '2022-02-19 08:05:00' THEN 1 END ) AS 'd_142310'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 08:05:00' AND startview_datetime < '2022-02-19 08:30:00' THEN 1 END ) AS 'd_142311'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 08:30:00' AND startview_datetime < '2022-02-19 10:00:00' THEN 1 END ) AS 'd_142312'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 10:00:00' AND startview_datetime < '2022-02-19 10:30:00' THEN 1 END ) AS 'd_142313'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 10:30:00' AND startview_datetime < '2022-02-19 10:55:00' THEN 1 END ) AS 'd_142314'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 10:55:00' AND startview_datetime < '2022-02-19 11:00:00' THEN 1 END ) AS 'd_142315'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 11:00:00' AND startview_datetime < '2022-02-19 11:30:00' THEN 1 END ) AS 'd_142316'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 11:30:00' AND startview_datetime < '2022-02-19 12:00:00' THEN 1 END ) AS 'd_142317'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 12:00:00' AND startview_datetime < '2022-02-19 13:00:00' THEN 1 END ) AS 'd_142318'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 13:00:00' AND startview_datetime < '2022-02-19 13:45:00' THEN 1 END ) AS 'd_142319'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 13:45:00' AND startview_datetime < '2022-02-19 14:00:00' THEN 1 END ) AS 'd_142320'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 14:00:00' AND startview_datetime < '2022-02-19 14:05:00' THEN 1 END ) AS 'd_142321'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 14:05:00' AND startview_datetime < '2022-02-19 14:50:00' THEN 1 END ) AS 'd_142322'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 14:50:00' AND startview_datetime < '2022-02-19 15:00:00' THEN 1 END ) AS 'd_142323'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 15:00:00' AND startview_datetime < '2022-02-19 16:00:00' THEN 1 END ) AS 'd_142324'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 16:00:00' AND startview_datetime < '2022-02-19 16:05:00' THEN 1 END ) AS 'd_142325'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 16:05:00' AND startview_datetime < '2022-02-19 16:30:00' THEN 1 END ) AS 'd_142326'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 16:30:00' AND startview_datetime < '2022-02-19 17:00:00' THEN 1 END ) AS 'd_142327'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 17:00:00' AND startview_datetime < '2022-02-19 17:05:00' THEN 1 END ) AS 'd_142328'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 17:05:00' AND startview_datetime < '2022-02-19 17:30:00' THEN 1 END ) AS 'd_142329'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 17:30:00' AND startview_datetime < '2022-02-19 18:00:00' THEN 1 END ) AS 'd_142330'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 18:00:00' AND startview_datetime < '2022-02-19 20:15:00' THEN 1 END ) AS 'd_142331'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 20:15:00' AND startview_datetime < '2022-02-19 21:10:00' THEN 1 END ) AS 'd_142332'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 21:10:00' AND startview_datetime < '2022-02-19 22:00:00' THEN 1 END ) AS 'd_142333'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 22:00:00' AND startview_datetime < '2022-02-19 22:05:00' THEN 1 END ) AS 'd_142334'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 22:05:00' AND startview_datetime < '2022-02-19 23:00:00' THEN 1 END ) AS 'd_142335'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 23:00:00' AND startview_datetime < '2022-02-19 23:30:00' THEN 1 END ) AS 'd_142336'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 23:30:00' AND startview_datetime < '2022-02-19 23:59:00' THEN 1 END ) AS 'd_142337'
	FROM #temp_table_satalite
	WHERE startview_datetime >= '2022-02-19 00:00:01'
	AND startview_datetime <= '2022-02-19 23:59:59'
	AND channels_id > 0
	GROUP BY  channels_id
	         ,devices_id
) AS r1


/* report : เลือกช่วงเวลา ตัวอย่างทุก 18 นาที */

SELECT  channels.id                                                                                                        AS channels_id
       ,merge_s3remote_id
       ,r1.hour                                                                                                            AS hour
       ,r1.MINUTE                                                                                                          AS minute
       ,CONVERT(varchar(12),merge_s3remote_id) + '_'+(CONVERT(varchar(12),r1.hour) + '_' + CONVERT(varchar(24),r1.MINUTE)) AS h_m
       ,COUNT(case WHEN r1.d_142296 >= 1 THEN 1 end)                                                                       AS 'field_142296'
       ,COUNT(case WHEN r1.d_142297 >= 1 THEN 1 end)                                                                       AS 'field_142297'
       ,COUNT(case WHEN r1.d_142298 >= 1 THEN 1 end)                                                                       AS 'field_142298'
       ,COUNT(case WHEN r1.d_142299 >= 1 THEN 1 end)                                                                       AS 'field_142299'
       ,COUNT(case WHEN r1.d_142300 >= 1 THEN 1 end)                                                                       AS 'field_142300'
       ,COUNT(case WHEN r1.d_142301 >= 1 THEN 1 end)                                                                       AS 'field_142301'
       ,COUNT(case WHEN r1.d_142302 >= 1 THEN 1 end)                                                                       AS 'field_142302'
       ,COUNT(case WHEN r1.d_142303 >= 1 THEN 1 end)                                                                       AS 'field_142303'
       ,COUNT(case WHEN r1.d_142304 >= 1 THEN 1 end)                                                                       AS 'field_142304'
       ,COUNT(case WHEN r1.d_142305 >= 1 THEN 1 end)                                                                       AS 'field_142305'
       ,COUNT(case WHEN r1.d_142306 >= 1 THEN 1 end)                                                                       AS 'field_142306'
       ,COUNT(case WHEN r1.d_142307 >= 1 THEN 1 end)                                                                       AS 'field_142307'
       ,COUNT(case WHEN r1.d_142308 >= 1 THEN 1 end)                                                                       AS 'field_142308'
       ,COUNT(case WHEN r1.d_142309 >= 1 THEN 1 end)                                                                       AS 'field_142309'
       ,COUNT(case WHEN r1.d_142310 >= 1 THEN 1 end)                                                                       AS 'field_142310'
       ,COUNT(case WHEN r1.d_142311 >= 1 THEN 1 end)                                                                       AS 'field_142311'
       ,COUNT(case WHEN r1.d_142312 >= 1 THEN 1 end)                                                                       AS 'field_142312'
       ,COUNT(case WHEN r1.d_142313 >= 1 THEN 1 end)                                                                       AS 'field_142313'
       ,COUNT(case WHEN r1.d_142314 >= 1 THEN 1 end)                                                                       AS 'field_142314'
       ,COUNT(case WHEN r1.d_142315 >= 1 THEN 1 end)                                                                       AS 'field_142315'
       ,COUNT(case WHEN r1.d_142316 >= 1 THEN 1 end)                                                                       AS 'field_142316'
       ,COUNT(case WHEN r1.d_142317 >= 1 THEN 1 end)                                                                       AS 'field_142317'
       ,COUNT(case WHEN r1.d_142318 >= 1 THEN 1 end)                                                                       AS 'field_142318'
       ,COUNT(case WHEN r1.d_142319 >= 1 THEN 1 end)                                                                       AS 'field_142319'
       ,COUNT(case WHEN r1.d_142320 >= 1 THEN 1 end)                                                                       AS 'field_142320'
       ,COUNT(case WHEN r1.d_142321 >= 1 THEN 1 end)                                                                       AS 'field_142321'
       ,COUNT(case WHEN r1.d_142322 >= 1 THEN 1 end)                                                                       AS 'field_142322'
       ,COUNT(case WHEN r1.d_142323 >= 1 THEN 1 end)                                                                       AS 'field_142323'
       ,COUNT(case WHEN r1.d_142324 >= 1 THEN 1 end)                                                                       AS 'field_142324'
       ,COUNT(case WHEN r1.d_142325 >= 1 THEN 1 end)                                                                       AS 'field_142325'
       ,COUNT(case WHEN r1.d_142326 >= 1 THEN 1 end)                                                                       AS 'field_142326'
       ,COUNT(case WHEN r1.d_142327 >= 1 THEN 1 end)                                                                       AS 'field_142327'
       ,COUNT(case WHEN r1.d_142328 >= 1 THEN 1 end)                                                                       AS 'field_142328'
       ,COUNT(case WHEN r1.d_142329 >= 1 THEN 1 end)                                                                       AS 'field_142329'
       ,COUNT(case WHEN r1.d_142330 >= 1 THEN 1 end)                                                                       AS 'field_142330'
       ,COUNT(case WHEN r1.d_142331 >= 1 THEN 1 end)                                                                       AS 'field_142331'
       ,COUNT(case WHEN r1.d_142332 >= 1 THEN 1 end)                                                                       AS 'field_142332'
       ,COUNT(case WHEN r1.d_142333 >= 1 THEN 1 end)                                                                       AS 'field_142333'
       ,COUNT(case WHEN r1.d_142334 >= 1 THEN 1 end)                                                                       AS 'field_142334'
       ,COUNT(case WHEN r1.d_142335 >= 1 THEN 1 end)                                                                       AS 'field_142335'
       ,COUNT(case WHEN r1.d_142336 >= 1 THEN 1 end)                                                                       AS 'field_142336'
       ,COUNT(case WHEN r1.d_142337 >= 1 THEN 1 end)                                                                       AS 'field_142337'
FROM
(
	SELECT  DATEPART(HOUR,startview_datetime)                                                                                      AS hour
		   ,CAST(DATEPART(MINUTE,dateadd(minute,(datediff(minute,0,startview_datetime)/18)*18,0)) AS char(2)) as MINUTE
	       ,devices_id
	       ,COUNT(*)                                                                                                               AS count_allratingrecord
	       ,channels_id
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 00:00:00' AND startview_datetime < '2022-02-19 01:00:00' THEN 1 END ) AS 'd_142296'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 05:00:00' AND startview_datetime < '2022-02-19 05:30:00' THEN 1 END ) AS 'd_142297'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 05:30:00' AND startview_datetime < '2022-02-19 05:55:00' THEN 1 END ) AS 'd_142298'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 05:55:00' AND startview_datetime < '2022-02-19 06:00:00' THEN 1 END ) AS 'd_142299'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:00:00' AND startview_datetime < '2022-02-19 06:05:00' THEN 1 END ) AS 'd_142300'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:05:00' AND startview_datetime < '2022-02-19 06:15:00' THEN 1 END ) AS 'd_142301'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:15:00' AND startview_datetime < '2022-02-19 06:25:00' THEN 1 END ) AS 'd_142302'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:25:00' AND startview_datetime < '2022-02-19 06:30:00' THEN 1 END ) AS 'd_142303'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:30:00' AND startview_datetime < '2022-02-19 06:50:00' THEN 1 END ) AS 'd_142304'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 06:50:00' AND startview_datetime < '2022-02-19 07:00:00' THEN 1 END ) AS 'd_142305'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:00:00' AND startview_datetime < '2022-02-19 07:05:00' THEN 1 END ) AS 'd_142306'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:15:00' AND startview_datetime < '2022-02-19 07:20:00' THEN 1 END ) AS 'd_142307'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:20:00' AND startview_datetime < '2022-02-19 07:40:00' THEN 1 END ) AS 'd_142308'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 07:40:00' AND startview_datetime < '2022-02-19 08:00:00' THEN 1 END ) AS 'd_142309'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 08:00:00' AND startview_datetime < '2022-02-19 08:05:00' THEN 1 END ) AS 'd_142310'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 08:05:00' AND startview_datetime < '2022-02-19 08:30:00' THEN 1 END ) AS 'd_142311'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 08:30:00' AND startview_datetime < '2022-02-19 10:00:00' THEN 1 END ) AS 'd_142312'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 10:00:00' AND startview_datetime < '2022-02-19 10:30:00' THEN 1 END ) AS 'd_142313'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 10:30:00' AND startview_datetime < '2022-02-19 10:55:00' THEN 1 END ) AS 'd_142314'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 10:55:00' AND startview_datetime < '2022-02-19 11:00:00' THEN 1 END ) AS 'd_142315'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 11:00:00' AND startview_datetime < '2022-02-19 11:30:00' THEN 1 END ) AS 'd_142316'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 11:30:00' AND startview_datetime < '2022-02-19 12:00:00' THEN 1 END ) AS 'd_142317'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 12:00:00' AND startview_datetime < '2022-02-19 13:00:00' THEN 1 END ) AS 'd_142318'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 13:00:00' AND startview_datetime < '2022-02-19 13:45:00' THEN 1 END ) AS 'd_142319'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 13:45:00' AND startview_datetime < '2022-02-19 14:00:00' THEN 1 END ) AS 'd_142320'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 14:00:00' AND startview_datetime < '2022-02-19 14:05:00' THEN 1 END ) AS 'd_142321'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 14:05:00' AND startview_datetime < '2022-02-19 14:50:00' THEN 1 END ) AS 'd_142322'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 14:50:00' AND startview_datetime < '2022-02-19 15:00:00' THEN 1 END ) AS 'd_142323'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 15:00:00' AND startview_datetime < '2022-02-19 16:00:00' THEN 1 END ) AS 'd_142324'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 16:00:00' AND startview_datetime < '2022-02-19 16:05:00' THEN 1 END ) AS 'd_142325'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 16:05:00' AND startview_datetime < '2022-02-19 16:30:00' THEN 1 END ) AS 'd_142326'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 16:30:00' AND startview_datetime < '2022-02-19 17:00:00' THEN 1 END ) AS 'd_142327'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 17:00:00' AND startview_datetime < '2022-02-19 17:05:00' THEN 1 END ) AS 'd_142328'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 17:05:00' AND startview_datetime < '2022-02-19 17:30:00' THEN 1 END ) AS 'd_142329'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 17:30:00' AND startview_datetime < '2022-02-19 18:00:00' THEN 1 END ) AS 'd_142330'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 18:00:00' AND startview_datetime < '2022-02-19 20:15:00' THEN 1 END ) AS 'd_142331'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 20:15:00' AND startview_datetime < '2022-02-19 21:10:00' THEN 1 END ) AS 'd_142332'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 21:10:00' AND startview_datetime < '2022-02-19 22:00:00' THEN 1 END ) AS 'd_142333'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 22:00:00' AND startview_datetime < '2022-02-19 22:05:00' THEN 1 END ) AS 'd_142334'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 22:05:00' AND startview_datetime < '2022-02-19 23:00:00' THEN 1 END ) AS 'd_142335'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 23:00:00' AND startview_datetime < '2022-02-19 23:30:00' THEN 1 END ) AS 'd_142336'
	       ,COUNT(CASE WHEN startview_datetime > '2022-02-19 23:30:00' AND startview_datetime < '2022-02-19 23:59:00' THEN 1 END ) AS 'd_142337'
	FROM #temp_table_satalite
	WHERE startview_datetime >= '2022-02-19 00:00:01'
	AND startview_datetime <= '2022-02-19 23:59:59'
	AND channels_id > 0
	GROUP BY  channels_id
	         ,DATEPART(YEAR,startview_datetime)
	         ,DATEPART(MONTH,startview_datetime)
	         ,DATEPART(DAY,startview_datetime)
	         ,DATEPART(HOUR,startview_datetime)
			 ,dateadd(minute,(datediff(minute,0,startview_datetime)/18)*18,0) 
	         ,devices_id
)r1
INNER JOIN channels
ON r1.channels_id = channels.id
WHERE channels.active = 1
and channels_id = 252
GROUP BY  r1.hour
         ,r1.MINUTE
         ,channels.id
         ,channels.merge_s3remote_id
ORDER BY r1.hour
         ,r1.MINUTE