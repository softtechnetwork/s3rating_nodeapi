/* =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = query :

CREATE temp gender =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = */
SELECT  * into #temp_gender
FROM
(
	SELECT  r2.devices_id AS dvid
	       ,r2.age
	       ,r2.gender
	FROM
	(
		SELECT  distinct devices_id
		       ,(
		SELECT  top 1 age
		FROM device_users AS dx
		WHERE device_users.devices_id = dx.devices_id ) AS age , (
		SELECT  top 1 gender
		FROM device_users AS dx
		WHERE device_users.devices_id = dx.devices_id ) AS gender
		FROM device_users
	) AS r2
	GROUP BY  r2.devices_id
	         ,r2.gender
	         ,r2.age
) r1 

/* =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = (eof) query :CREATE temp gender =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = */ /* =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = query : get temp gender =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = */
SELECT  (CASE WHEN #temp_gender.gender IS NULL THEN 'none' WHEN #temp_gender.gender = '' THEN 'none1' WHEN #temp_gender.gender = '-' THEN 'none2' ELSE #temp_gender.gender END) AS gender
       ,COUNT(case WHEN d_0_0 >= 1 THEN 1 end) AS 'field_0_0'
       ,tvchannels_id
       ,COUNT(*)                               AS total_row
FROM #temp_data_iptv
LEFT JOIN #temp_gender
ON #temp_data_iptv.devices_id = #temp_gender.dvid
GROUP BY  gender
         ,tvchannels_id
ORDER BY tvchannels_id asc
         ,#temp_gender.gender asc 
		 /* =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = (eof) query : get temp gender =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = */
/*======================= query : select into temp data ==================================  */
SELECT  * INTO #temp_data
FROM
(
	SELECT  channels_id
	       ,devices_id
	       ,COUNT(CASE WHEN startview_datetime > '2022-05-10 00:00:01' AND startview_datetime < '2022-05-10 23:59:59' THEN 1 END ) AS 'd_0_0'
	FROM rating_data_2022_5
	WHERE startview_datetime >= '2022-05-10 00:00:01'
	AND startview_datetime <= '2022-05-10 23:59:59'
	AND channels_id > 0
	GROUP BY  channels_id
	         ,devices_id
) AS r1

/*======================= query : temp gender age ==================================  */
SELECT  (CASE WHEN #temp_gender.age IS NULL THEN 'none' WHEN #temp_gender.age = 0 THEN 'none'  WHEN #temp_gender.age = '' THEN 'none1' WHEN #temp_gender.age = '-' THEN 'none2' ELSE #temp_gender.age END) AS gender
       ,merge_s3remote_id
       ,COUNT(case WHEN d_0_0 >= 1 THEN 1 end) AS 'field_0_0'
       ,channels_id
       ,COUNT(*)                               AS total_row
FROM #temp_data
INNER JOIN channels
ON #temp_data.channels_id = channels.id
LEFT JOIN #temp_gender
ON #temp_data.devices_id = #temp_gender.dvid
WHERE channels.active = 1
GROUP BY  gender
         ,channels_id
         ,channels.merge_s3remote_id
ORDER BY channels_id asc
         ,#temp_gender.age asc
/*======================= (eof) query : temp gender age ==================================  */

/*======================= query : temp data iptv ==================================  */

SELECT  * INTO #temp_data_iptv
FROM
(
	SELECT  tvchannels_id
	       ,devices_id
	       ,COUNT(CASE WHEN startview_datetime > '2022-05-10 00:00:01' AND startview_datetime < '2022-05-10 23:59:59' THEN 1 END ) AS 'd_0_0'
	FROM S3Application.dbo.rating_data_2022_5
	WHERE startview_datetime >= '2022-05-10 00:00:01'
	AND startview_datetime <= '2022-05-10 23:59:59'
	AND tvchannels_id > 0
	GROUP BY  tvchannels_id
	         ,devices_id
) AS r1


/*====================== query : temp gender age ipt ==================================  */

SELECT  (CASE WHEN #temp_gender.age = 0 THEN 0 WHEN #temp_gender.age IS NULL THEN 0 WHEN #temp_gender.age = '' THEN 0 WHEN #temp_gender.age = '-' THEN 0 ELSE #temp_gender.age END) AS age
       ,COUNT(case WHEN d_0_0 >= 1 THEN 1 end) AS 'field_0_0'
       ,tvchannels_id
       ,COUNT(*)                               AS total_row
FROM #temp_data_iptv
LEFT JOIN #temp_gender
ON #temp_data_iptv.devices_id = #temp_gender.dvid
GROUP BY  age
         ,tvchannels_id
ORDER BY tvchannels_id asc
         ,#temp_gender.age asc