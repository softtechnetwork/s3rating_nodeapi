SELECT  channels.id                                                                                                        AS channels_id
       ,merge_s3remote_id
       ,r1.hour                                                                                                            AS hour
       ,r1.MINUTE                                                                                                          AS minute
       ,CONVERT(varchar(12),merge_s3remote_id) + '_'+(CONVERT(varchar(12),r1.hour) + '_' + CONVERT(varchar(12),r1.MINUTE)) AS h_m
       ,COUNT(case WHEN r1.d_0_15 >= 1 THEN 1 end)                                                                         AS 'field_0_15'
       ,COUNT(case WHEN r1.d_0_16 >= 1 THEN 1 end)                                                                         AS 'field_0_16'
       ,COUNT(case WHEN r1.d_0_17 >= 1 THEN 1 end)                                                                         AS 'field_0_17'
       ,COUNT(case WHEN r1.d_0_18 >= 1 THEN 1 end)                                                                         AS 'field_0_18'
       ,COUNT(case WHEN r1.d_0_19 >= 1 THEN 1 end)                                                                         AS 'field_0_19'
       ,COUNT(case WHEN r1.d_0_20 >= 1 THEN 1 end)                                                                         AS 'field_0_20'
       ,COUNT(case WHEN r1.d_0_21 >= 1 THEN 1 end)                                                                         AS 'field_0_21'
       ,COUNT(case WHEN r1.d_0_22 >= 1 THEN 1 end)                                                                         AS 'field_0_22'
       ,COUNT(case WHEN r1.d_0_23 >= 1 THEN 1 end)                                                                         AS 'field_0_23'
       ,COUNT(case WHEN r1.d_0_24 >= 1 THEN 1 end)                                                                         AS 'field_0_24'
       ,COUNT(case WHEN r1.d_0_25 >= 1 THEN 1 end)                                                                         AS 'field_0_25'
       ,COUNT(case WHEN r1.d_0_26 >= 1 THEN 1 end)                                                                         AS 'field_0_26'
       ,COUNT(case WHEN r1.d_0_27 >= 1 THEN 1 end)                                                                         AS 'field_0_27'
       ,COUNT(case WHEN r1.d_0_28 >= 1 THEN 1 end)                                                                         AS 'field_0_28'
       ,COUNT(case WHEN r1.d_0_29 >= 1 THEN 1 end)                                                                         AS 'field_0_29'
FROM
(
	SELECT  DATEPART(HOUR,startview_datetime)                                                                                      AS hour
	       ,CAST(DATEPART(MINUTE,startview_datetime)/1 AS char(2))                                                                 AS MINUTE
	       ,devices_id
	       ,COUNT(*)                                                                                                               AS count_allratingrecord
	       ,channels_id
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:15:00' AND startview_datetime < '2022-06-02 00:16:00' THEN 1 END ) AS 'd_0_15'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:16:00' AND startview_datetime < '2022-06-02 00:17:00' THEN 1 END ) AS 'd_0_16'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:17:00' AND startview_datetime < '2022-06-02 00:18:00' THEN 1 END ) AS 'd_0_17'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:18:00' AND startview_datetime < '2022-06-02 00:19:00' THEN 1 END ) AS 'd_0_18'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:19:00' AND startview_datetime < '2022-06-02 00:20:00' THEN 1 END ) AS 'd_0_19'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:20:00' AND startview_datetime < '2022-06-02 00:21:00' THEN 1 END ) AS 'd_0_20'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:21:00' AND startview_datetime < '2022-06-02 00:22:00' THEN 1 END ) AS 'd_0_21'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:22:00' AND startview_datetime < '2022-06-02 00:23:00' THEN 1 END ) AS 'd_0_22'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:23:00' AND startview_datetime < '2022-06-02 00:24:00' THEN 1 END ) AS 'd_0_23'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:24:00' AND startview_datetime < '2022-06-02 00:25:00' THEN 1 END ) AS 'd_0_24'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:25:00' AND startview_datetime < '2022-06-02 00:26:00' THEN 1 END ) AS 'd_0_25'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:26:00' AND startview_datetime < '2022-06-02 00:27:00' THEN 1 END ) AS 'd_0_26'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:27:00' AND startview_datetime < '2022-06-02 00:28:00' THEN 1 END ) AS 'd_0_27'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:28:00' AND startview_datetime < '2022-06-02 00:29:00' THEN 1 END ) AS 'd_0_28'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 00:29:00' AND startview_datetime < '2022-06-02 00:30:00' THEN 1 END ) AS 'd_0_29'
	FROM rating_data_2022_6
	WHERE startview_datetime >= '2022-06-02 00:15:00'
	AND startview_datetime <= '2022-06-02 00:30:00'
	AND channels_id > 0
	AND channels_id = 257
	GROUP BY  channels_id
	         ,DATEPART(YEAR,startview_datetime)
	         ,DATEPART(MONTH,startview_datetime)
	         ,DATEPART(DAY,startview_datetime)
	         ,DATEPART(HOUR,startview_datetime)
	         ,DATEPART(MINUTE,startview_datetime)/1
	         ,devices_id
)r1
INNER JOIN channels
ON r1.channels_id = channels.id
WHERE channels.active = 1
GROUP BY  r1.hour
         ,r1.MINUTE
         ,channels.id
         ,channels.merge_s3remote_id
ORDER BY r1.hour
         ,r1.MINUTE