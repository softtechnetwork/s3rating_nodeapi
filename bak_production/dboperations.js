var config = require('./dbconfig');
var config_s3app = require('./dbconfig_s3app');
var config_pg = require('./dbconfig_pg');
const util = require('util');
var http = require('http');
var url = require('url') ;
var fs = require('fs');
const sql = require('mssql');
const {
    response
} = require('express');
const { redirect } = require('express/lib/response');
const { time } = require('console');
// const pgp = require('pg-promise')(); // load module : postgresql database 
const Pool = require('pg').Pool // load module : postgresql database 

/**
 * =====================
 * API : แสดงรายการคนดูตามจังหวัด
 * =====================
 */
async function get_provinceviewer(date, channel_id) {
    try {
        let pool = await sql.connect(config);

        var month = get_dateobject(date, "month");
        var year = get_dateobject(date, "year");
        var rating_data_daily_device_table = "rating_data_daily_devices_" + year + "_" + month;

        // case : เรียกข้อมูล rating ของวันนั้น
        let rating_data_daily = await pool.request()
            .input('input_parameter_date', sql.VarChar, date)
            .input('input_parameter_channel', sql.Int, channel_id)
            .query("SELECT * from rating_data_daily where  date = @input_parameter_date and channels_id = @input_parameter_channel");
        if (rating_data_daily != null) {
            let rating_data_daily_obj = rating_data_daily.recordsets[0];
            var rating_data_daily_id = JSON.parse(JSON.stringify(rating_data_daily_obj));

            if (rating_data_daily_id != null) {
                var rating_data_daily_id = rating_data_daily_id[0].id;

                let rating_data_dailydevice = await pool.request()
                    .input('input_parameter_id', sql.Int, rating_data_daily_id)
                    .query("select count(*) as all_devices , region_name,provinces.id as province_id  from " + rating_data_daily_device_table + " left join device_addresses on  " + rating_data_daily_device_table + ".devices_id  = device_addresses.devices_id inner join provinces on provinces.ipstack_province_name  = device_addresses.region_name  where rating_data_daily_id = @input_parameter_id  group by device_addresses.region_name , provinces.id  order by provinces.id asc");

                if (rating_data_dailydevice != null) {
                    return [{
                        "status": true,
                        data: rating_data_dailydevice.recordsets[0],
                        "result_code": "000",
                        "result_desc": "Success"
                    }];
                } else {
                    return [{
                        "status": false,
                        "result_code": "-005",
                        "result_desc": "not found data"
                    }];
                }
            }


        } else {
            return [{
                "status": false,
                "result_code": "-005",
                "result_desc": "not found data"
            }];
        }

    } catch (error) {
        return [{
            "status": false,
            "result_code": "-005",
            "result_desc": "not found data"
        }];
    }
}

async function get_deviceviewerbyprovince(date, channel_id, province_id) {
    try {
        let pool = await sql.connect(config);

        var month = get_dateobject(date, "month");
        var year = get_dateobject(date, "year");
        var rating_data_daily_device_table = "rating_data_daily_devices_" + year + "_" + month;

        // case : เรียกข้อมูล rating ของวันนั้น
        let rating_data_daily = await pool.request()
            .input('input_parameter_date', sql.VarChar, date)
            .input('input_parameter_channel', sql.Int, channel_id)
            .query("SELECT * from rating_data_daily where  date = @input_parameter_date and channels_id = @input_parameter_channel");
        if (rating_data_daily != null) {
            let rating_data_daily_obj = rating_data_daily.recordsets[0];
            var rating_data_daily_id = JSON.parse(JSON.stringify(rating_data_daily_obj));

            if (rating_data_daily_id != null) {
                var rating_data_daily_id = rating_data_daily_id[0].id;

                let rating_data_dailydevice = await pool.request()
                    .input('input_parameter_id', sql.Int, rating_data_daily_id)
                    .input('input_parameter_provinceid', sql.Int, province_id)
                    .query("select " + rating_data_daily_device_table + ".devices_id," + rating_data_daily_device_table + ".ship_code, region_name,device_addresses.latitude , device_addresses.longitude from " + rating_data_daily_device_table + " left join device_addresses on  " + rating_data_daily_device_table + ".devices_id  = device_addresses.devices_id inner join provinces on provinces.ipstack_province_name  = device_addresses.region_name  where rating_data_daily_id = @input_parameter_id and provinces.id = @input_parameter_provinceid ");

                if (rating_data_dailydevice != null) {
                    return [{
                        "status": true,
                        data: rating_data_dailydevice.recordsets[0],
                        "result_code": "000",
                        "result_desc": "Success"
                    }];
                } else {
                    return [{
                        "status": false,
                        "result_code": "-005",
                        "result_desc": "not found data"
                    }];
                }
            } else {
                return [{
                    "status": false,
                    "result_code": "-005",
                    "result_desc": "not found data"
                }];
            }


        }

    } catch (error) {
        return [{
            "status": false,
            "result_code": "-005",
            "result_desc": "not found data"
        }];
    }

}
async function get_deviceviewer_perminute(date, channels_id) {


    let pool = await sql.connect(config);

    var month = get_dateobject(date, "month");
    var year = get_dateobject(date, "year");
    var rating_data_table = "rating_data_" + year + "_" + month;

    let $query = build_query( "minute" , rating_data_table , channels_id , date);
    let rating_data_daily = await pool.request()
        .query($query);
    if (rating_data_daily != null) {
        let rating_data_daily_obj = rating_data_daily.recordsets;
        var rating_data_daily_id = JSON.parse(JSON.stringify(rating_data_daily_obj));
        if (rating_data_daily_id != null) {
            return [{
                "status": true,
                "data": rating_data_daily_obj,
                "result_code": "000",
                "result_desc": "Success"
            }];
        } else {
            return [{
                "status": false,
                "result_code": "-005",
                "result_desc": "not found data"
            }];
        }


    }
}

const get_tvprogram_recordset = async (channels_id  = null , date = null) => {
    let pool = await sql.connect(config);
  
    let $query_tvprogram= get_tvprogram( channels_id  , date);
    var obj;
    let tvprogram     = await pool.request()
    .query($query_tvprogram);
    let tvprogram_recordset = tvprogram.recordsets;
    
    if(tvprogram_recordset.length != 0){
        obj = JSON.parse(JSON.stringify(tvprogram_recordset));
        //channels   =  channels_obj;
        return [{
            "status": true,
            "data" : obj,
            "result_code": "000",
            "result_desc": "tvprogram item not found."
        }]
       
    }else{
        return [{
            "status": false,
            "result_code": "-005",
            "result_desc": "tvprogram item not found."
        }]
    }
    return obj;
  

}


function query_overviewtemptable($type , temptablename , date){
    let $query = "";
    if($type == "satalite"){
        $query+= `
            create table ${temptablename} (id int primary key ,devices_id  int,channels_id int,ship_code varchar(100)
            ,frq varchar(50)
            ,sym varchar(50)
            ,pol varchar(50)
            ,server_id  varchar(50)
            ,vdo_pid varchar(50)
            ,ado_pid varchar(50)
            ,view_seconds int
            ,ip_address varchar(50)
            ,startview_datetime datetime
            ,channel_ascii_code int
            ,created datetime
            )`;
    }else if($type == "satalite_insert"){
        let rating_data_table = get_ratingdatatable( date);
        $query +=`INSERT INTO ${temptablename}
         select ${rating_data_table}.* from ${rating_data_table} 
         WHERE startview_datetime >= '${date} 00:00:01'
         AND startview_datetime <= '${date} 23:59:59'`;
    }else if($type == "iptv"){
        $query+= `
            create table ${temptablename} (id int primary key ,devices_id  int,tvchannels_id int,chip_code varchar(100)
            ,view_seconds int
            ,ip_address varchar(50)
            ,startview_datetime datetime
            ,created datetime
            )`;
    }else if($type == "iptv_insert"){
        let rating_data_table = get_iptvratingdatatable( date);
        $query +=`INSERT INTO ${temptablename}
         select ${rating_data_table}.* from ${config_s3app.database+".dbo."}${rating_data_table} 
         WHERE startview_datetime >= '${date} 00:00:01'
         AND startview_datetime <= '${date} 23:59:59'`;
    }
    return $query;
}

function get_iptvratingdatatable(date){
    

    var month = get_dateobject(date, "month");
    var year = get_dateobject(date, "year");
    var rating_data_table =  "rating_data_" + year + "_" + month;

    return rating_data_table;

}
const create_temptblsatalite = async (pool , date  = null , config) => {

  
    let $query_temptablesatalite     =query_overviewtemptable( "satalite" , "#temp_table_satalite");
    let req = new sql.Request(pool);
    await req.batch($query_temptablesatalite);

    let $query_inserttablesatalite  = query_overviewtemptable("satalite_insert" , "#temp_table_satalite" , date)
    let rows = await req.batch($query_inserttablesatalite);
  
    if(rows.rowsAffected > 0){
        return [{
            "status": true,
            "req" : req,
            data: "#temp_table_satalite",
            "result_code": "000",
            "result_desc": "Success"
        }];
       
    }else{
        return [{
            "status": false,
            "result_code": "-005",
            "result_desc": "not found channel"
        }]
    }
    return channels;
  

}
const create_temptbliptv = async (pool , date  = null , config) => {

  
    let $query_temptableiptv     =query_overviewtemptable( "iptv" , "#temp_table_iptv");
   
    let req = new sql.Request(pool);
    await req.batch($query_temptableiptv);

    let $query_inserttableiptv  = query_overviewtemptable("iptv_insert" , "#temp_table_iptv" , date)
    
    let rows = await req.batch($query_inserttableiptv);
  
    if(rows.rowsAffected > 0){
        return [{
            "status": true,
            "req" : req,
            data: "#temp_table_iptv",
            "result_code": "000",
            "result_desc": "Success"
        }];
       
    }else{
        return [{
            "status": false,
            "result_code": "-005",
            "result_desc": "not found channel"
        }]
    }
    return channels;
  

}


async function get_overview_report(date , avg_minute){
    if(date.length != 0 && avg_minute.length != 0){
        let channels = await get_channels();
        if(channels[0].status != undefined){
            return channels;
        }else{
            let pool = await sql.connect(config);
            // case : tvprogram base time on tpbs ( ดึงข้อมูลช่วงเวลาอิงตามผังของ thaipbs )
            let tvprogram_recordset_obj_ = await get_tvprogram_recordset(252 , date);
            if(tvprogram_recordset_obj_[0].status == false){
                return tvprogram_recordset_obj_;
            }else{
                let tvprogram_recordset_obj = tvprogram_recordset_obj_[0].data;
                

                // case : overview report
           
                let tvprogram_basetime_satalitereport_data;
                let tvprogram_basetime_iptvreport_data;
                let rating_data_daily_satalite;
                let rating_data_daily_iptv;
                let tvprogram_basetime_ytreport_data;
                let satalite_gender;
                let iptv_gender;
                let province_region_satalite;
                let province_region_iptv;
                var month = get_dateobject(date, "month");
                var year = get_dateobject(date, "year");
                let rating_data_table  = get_ratingdatatable( date );
                let channel_daily_devices_summary_table = get_channeldailydevicetbl(date);
                    let temptableratingsatalite =  await create_temptblsatalite(pool ,  date  , config);
                    let temptableratingiptv     =  await create_temptbliptv(pool ,  date  , config);
                    var temptableratingdata     = temptableratingsatalite[0].data;
                    var temptableiptvdata       = temptableratingiptv[0].data;
             
                    // var tvprogram_recordset_obj = JSON.parse(JSON.stringify(tvprogram_recordset));
                    let $query_satalite_minute = build_query( "overview_minute" , temptableratingdata , null , date  , null , null, tvprogram_recordset_obj);
                    let $query_IPTV     = build_query( "overview_minute_s3app" , temptableiptvdata  , null , date , null , null , tvprogram_recordset_obj );
                    
                    let req_dropsatalite = new sql.Request(pool);
                    let req_dropiptv     = new sql.Request(pool);
                   
                   
                    rating_data_daily_satalite = await pool.request()
                    .query($query_satalite_minute.temptable);
                    rating_data_daily_iptv     = await pool.request()
                    .query($query_IPTV.temptable);
                    // case : drop old temp table and create new one
                    $var_temptable = `#temp_data`;
                    // case : write new query
                    let $query_satalite = build_query( "tvprogram_basetime_satalitereport" , rating_data_table ,  null , date , null , null , tvprogram_recordset_obj );
                    
                    // case : crete new temp table satalite
                    $query_createtemptable = "SELECT * INTO " + $var_temptable +" FROM ( "+ $query_satalite.temptable + " ) as r1";  // case : create new temporary table 
                   // console.log($query_createtemptable);
                    let req = new sql.Request(pool);
                    await req.batch($query_createtemptable);
                   
                    $query_tvprogrambaseonsatalite = ` SELECT merge_s3remote_id,channels_id ${$query_satalite.select_condition} FROM  #temp_data  inner join channels on #temp_data.channels_id = channels.id where channels.active = 1 group by channels_id,merge_s3remote_id`; // case : select temporary table data
                    // console.log($query_tvprogrambaseonsatalite);
                    // case :  create new temp gender
                    $var_temptable_gender ="#temp_gender"; 
                    $query_createtempgender = "select * into "+$var_temptable_gender+" from (select devices_id as dvid ,gender from "+channel_daily_devices_summary_table+" group by devices_id,gender) r1";  // case : create new temporary table 
                 
                    let req_tempgender = new sql.Request(pool);
                    await req_tempgender.batch($query_createtempgender);
                    // case  : (query) group by gender ( satalite )
                    let $query_satalite_gender = build_query( "groupby_gender_satalite" ,null , null , null , null , null , tvprogram_recordset_obj, null , $var_temptable );
                    let query_tempgender_satalite = await req_tempgender.batch($query_satalite_gender.temptable);
                    satalite_gender = query_tempgender_satalite.recordsets[0];
                   
                    // case : (query) group by province region satalite data
                    let $query_satalite_addr = build_query( "groupby_addr_satalite" ,null , null , null , null , null , tvprogram_recordset_obj, null , $var_temptable );
                   
                    let query_groupbyaddr_satalite = await req_tempgender.batch($query_satalite_addr.temptable);
                    province_region_satalite = query_groupbyaddr_satalite.recordsets[0];
            
            
                    // case :  (drop) temp satalite
                    let query_temptable = await req.batch($query_tvprogrambaseonsatalite);
                    
                    await req.batch( "drop table " + $var_temptable ); // case : drop temp table
                    tvprogram_basetime_satalitereport_data = query_temptable.recordset;
                    
                     // case : crete new temp table iptv
                     $var_temptable_iptv = `#temp_data_iptv`;
                     let $query_iptv = build_query( "tvprogram_basetime_iptvreport_s3app" , rating_data_table ,  null , date , null , null , tvprogram_recordset_obj );
                     //tvprogram_basetime_iptvreport_data = get_iptvratingdata( $query_iptv , $var_temptable_iptv , pool);
                     
                     $query_createtemptable_iptv = "SELECT * INTO " + $var_temptable_iptv +" FROM ( "+ $query_iptv.temptable + " ) as r1";  // case : create new temporary table 
                    
                     let reqs3app = new sql.Request(pool);
                     await reqs3app.batch($query_createtemptable_iptv);
                 
                     $query_tvprogrambaseiptv = ` SELECT tvchannels_id ${$query_iptv.select_condition} FROM  #temp_data_iptv group by tvchannels_id`; // case : select temporary table data
                     let query_temptable_iptv = await reqs3app.batch($query_tvprogrambaseiptv);
            
                    // case  : (query) group by gender ( iptv )
                    let $query_iptv_gender;
                    if(satalite_gender[0] != null){
                        let $tvchannels_id      =satalite_gender[0].merge_s3remote_id;
                        $query_iptv_gender = build_query( "groupby_gender_iptv" ,null , $tvchannels_id , null , null , null , tvprogram_recordset_obj, null , $var_temptable_iptv );
                        let query_tempgender_iptv = await req_tempgender.batch($query_iptv_gender.temptable);
                        iptv_gender = query_tempgender_iptv.recordsets[0];
                    }else{
                        iptv_gender = null;
                    }
                    
                    // case : (query) group by province region iptv data
                    if(province_region_satalite[0] != null){
                        let $tvchannels_id_groupaddr = province_region_satalite[0].merge_s3remote_id;
                        
                        let $query_iptv_addr = build_query( "groupby_addr_iptv" ,null , $tvchannels_id_groupaddr , null , null , null , tvprogram_recordset_obj, null , $var_temptable_iptv );
                     
                        let query_groupbyaddr_iptv = await req_tempgender.batch($query_iptv_addr.temptable);
                        province_region_iptv = query_groupbyaddr_iptv.recordsets[0];
                    }else{
                        province_region_iptv = null;
                    }
                 
            
                    // casae : (drop) temp gender
                    await req_tempgender.batch( "drop table " + $var_temptable_gender ); // case : drop temp table
            
                     // case :  (drop) temp iptv
                     await reqs3app.batch( "drop table " + $var_temptable_iptv ); // case : drop temp table
                     tvprogram_basetime_iptvreport_data = query_temptable_iptv.recordset;
            
                     // case : create new temp youtube view
                     $var_temptable_youtube = `#temp_data_youtube`;
                     let youtube_data_table = "youtube_rating_log_" + year + "_" + month;
                     let $query_youtube = build_query( "youtubeviewreport_satalite" , rating_data_table ,  null , date , null , null , tvprogram_recordset_obj ,  youtube_data_table);
                     //tvprogram_basetime_iptvreport_data = get_iptvratingdata( $query_youtube , $var_temptable_youtube , pool);
                     
                     $query_createtemptable_youtube = "SELECT * INTO " + $var_temptable_youtube +" FROM ( "+ $query_youtube.temptable + " ) as r1";  // case : create new temporary table 
                     let reqs3ratingyt = new sql.Request(pool);
                     await reqs3ratingyt.batch($query_createtemptable_youtube);
            
                     $query_tvprogrambaseyoutube = ` SELECT devices_id ${$query_youtube.select_condition} FROM  #temp_data_youtube group by devices_id`; // case : select temporary table data
                     let query_temptable_yt = await reqs3ratingyt.batch($query_tvprogrambaseyoutube);
                     await reqs3ratingyt.batch( "drop table " + $var_temptable_youtube ); // case : drop temp table
                     tvprogram_basetime_ytreport_data = query_temptable_yt.recordset;
            
                    
                     await req_dropsatalite.batch( "drop table " + temptableratingdata ); // case : drop temp table ratingdata
                     await req_dropiptv.batch( "drop table " + temptableiptvdata ); // case : drop temp table iptv
            
                

                var arr_data = set_arrdata( [] );
   
                if (rating_data_daily_satalite != null ) {
                    let rating_data_daily_satalite_obj = rating_data_daily_satalite.recordsets[0];
                    var rating_data_daily_satalite_ = JSON.parse(JSON.stringify(rating_data_daily_satalite_obj));
            
                    if (rating_data_daily_satalite_ != null) {

                        //  arr_data = set_satalitetotal_arr( arr_data , rating_data_daily_satalite_); // case : set satalite data
                        if(rating_data_daily_iptv != null){
                            let rating_data_daily_iptv_obj = rating_data_daily_iptv.recordsets[0];
                        
                            var rating_data_daily_iptv_ = JSON.parse(JSON.stringify(rating_data_daily_iptv_obj));
                            arr_data = merge_andsortallofthisfuck( rating_data_daily_satalite_ , rating_data_daily_iptv_);
                        }
                        else{
                            arr_data = rating_data_daily_satalite_;
                        }
                    
                     

                    
                        // let $filename =   await write_excelfile_overview( arr_data  , channels  , date, tvprogram_recordset_obj 
                        //     , tvprogram_basetime_satalitereport_data , tvprogram_basetime_iptvreport_data , channels_id
                        //     , tvprogram_basetime_ytreport_data , satalite_gender , iptv_gender 
                        //     , province_region_satalite , province_region_iptv );
                        
                        // const file = $filename;
                    
                        return   [{
                            "status": true,
                            "data": file,
                            "result_code": "000",
                            "result_desc": "Success"
                        }];
                        
                    
                    
                    } else {
                        return [{
                            "status": false,
                            "result_code": "-005",
                            "result_desc": "not found data"
                        }];
                    }


                }


            }
            
           
        }

      
    }else{
        return [{
            "status": false,
            "result_code": "-005",
            "result_desc": "startdatetime or enddatetime should not be empty."
        }];
    }

}


function get_ratingdatatable(date){
    var month = get_dateobject(date, "month");
    var year = get_dateobject(date, "year");
    var rating_data_table = "rating_data_" + year + "_" + month;

    return rating_data_table;
}


function get_channeldailydevicetbl(date){
    var month = get_dateobject(date, "month");
    var year = get_dateobject(date, "year");
    var channel_daily_table =  "channel_daily_devices_summary_"+year+"_"+month;
    return channel_daily_table;
}
async function write_excelfile_overview(arr_data  , channels  , date, tvprogram_recordset_obj 
    , tvprogram_basetime_satalitereport_data , tvprogram_basetime_iptvreport_data , channels_id
    , tvprogram_basetime_ytreport_data , satalite_gender , iptv_gender 
    , province_region_satalite , province_region_iptv){
    
        var excel = require('excel4node');
        // Create a new instance of a Workbook class
        var workbook = new excel.Workbook();

        // Add Worksheets to the workbook
        var worksheet = workbook.addWorksheet('dailyreport');
        // Create a reusable style
        var style = workbook.createStyle({
            font: {
                color: '#FF0800',
                size: 12
            },
            numberFormat: '$#,##0.00; ($#,##0.00); -'
        });
        var text_style_header = workbook.createStyle({
            font: {
                color: '#000000',
                size: 10
            },alignment: { 
                shrinkToFit: true, 
                wrapText: true
            },
            border: {
                left: {
                    style: 'thin',
                    color: 'black',
                },
                right: {
                    style: 'thin',
                    color: 'black',
                },
                top: {
                    style: 'thin',
                    color: 'black',
                },
                bottom: {
                    style: 'thin',
                    color: 'black',
                },
                outline: false,
            },
        });
        var text_style = workbook.createStyle({
            font: {
                color: '#000000',
                size: 10
            },alignment: { 
                shrinkToFit: true, 
                wrapText: true
            },
            border: {
                left: {
                    style: 'thin',
                    color: 'black',
                },
                right: {
                    style: 'thin',
                    color: 'black',
                },
                top: {
                    style: 'thin',
                    color: 'black',
                },
                bottom: {
                    style: 'thin',
                    color: 'black',
                },
                outline: false,
            },
        });

        // Set value of cell A1 to 100 as a number type styled with paramaters of style ( แถว , หลัก )
        worksheet  = create_excelheader(worksheet  , channels , text_style_header , null , date);


        let original_tvprogram_basetime_satalitereport_data = tvprogram_basetime_satalitereport_data_s;
        let original_tvprogram_basetime_iptv_data = tvprogram_basetime_iptvreport_data_s;
        // case : save log file
        let logsatalite    = JSON.stringify(original_tvprogram_basetime_satalitereport_data);
        var logsatalite_fn = 'logsatalite_' + Math.round(+new Date()/1000)+ ".json";
        fs.writeFileSync('./json/overview/'+logsatalite_fn, logsatalite);

        let logiptv    = JSON.stringify(original_tvprogram_basetime_iptv_data);
        var logiptv_fn = 'logiptv_' + Math.round(+new Date()/1000) + ".json";
        fs.writeFileSync('./json/overview/'+logiptv_fn, logiptv);
      
        
        let obj_satalite = require('./json/overview/'+logsatalite_fn);
        let obj_iptv     = require('./json/overview/'+logiptv_fn);
        
        var tvprogram_basetime_satalitereport_data = await find_channeldelete( "satalite" ,obj_satalite ); // list of top twenty digital tv
        var tvprogram_basetime_iptvreport_data     = await find_channeldelete( "iptv" ,obj_iptv );// list of top twenty digital tv
     
        let tvprogram_obj          = get_thistvprogram( tvprogram_basetime_satalitereport_data , tvprogram_basetime_iptvreport_data , channels_id); // case : get only tvprogrm of this channel 
       let merge_data_obj         =  merge_data(tvprogram_basetime_satalitereport_data , tvprogram_basetime_iptvreport_data ); // case : merge top20channel satalite and iptv
       
       let logtop20    = JSON.stringify(merge_data_obj);
       var logtop20_fn = 'logtop20' + Math.round(+new Date()/1000) + ".json";
       fs.writeFileSync('./json/overview/'+logtop20_fn, logtop20);
       
       let merge_data_obj_everychannel  =   merge_data(original_tvprogram_basetime_satalitereport_data , original_tvprogram_basetime_iptv_data ); 
      
      

        let sum_allbaseontime = sum_everychannel_baseontime(merge_data_obj_everychannel); //  sum everychannel base on time slot
        let sum_top20         = sum_everychannel_baseontime(merge_data_obj); // sum  top20  base on timeslot
       
        let merge_data_gender_obj  = merge_data_gender(satalite_gender , iptv_gender);

        let logprovincesatalite    = JSON.stringify(province_region_satalite);
        var logprovincesatalite_fn = 'logprovincesatalite_' + Math.round(+new Date()/1000) + ".json";
        fs.writeFileSync('./json/overview/'+logprovincesatalite_fn, logprovincesatalite);
        
        let logprovinceiptv    = JSON.stringify(province_region_iptv);
        var logprovinceiptv_fn = 'logprovinceiptv_' + Math.round(+new Date()/1000) + ".json";
        fs.writeFileSync('./json/overview/'+logprovinceiptv_fn, logprovinceiptv);
      
        
        let obj_provincesatalite = require('./json/overview/'+logprovincesatalite_fn);
        let obj_provinceiptv     = require('./json/overview/'+logprovinceiptv_fn);

        let merge_data_province_obj= merge_data(obj_provincesatalite , obj_provinceiptv, "element.province_region");

        workbook.write('./excel/overview/overviewreport_'+seconds_since_epoch(new Date()) +'.xlsx');
}

function create_excelheader(worksheet , channels , text_style_header , report = null , date = null){
    
    var year = get_dateobject(date, "year");
    worksheet.cell(2, 1).string("ปี").style(text_style_header);
    worksheet.cell(3, 1).number(year).style(text_style_header);

    worksheet.cell(4, 1).string("MONTH").style(text_style_header);
    worksheet.cell(4, 2).string("WKDAY / WKEND").style(text_style_header);
    worksheet.cell(4, 3).string("DAY").style(text_style_header);
    worksheet.cell(4, 4).string("DD/MM/YYYY").style(text_style_header);
    worksheet.cell(4, 5).string("Time").style(text_style_header);
    worksheet.cell(4, 6).string("จำนวนเครื่องรวม(เข้าถึง)").style(text_style_header);
    worksheet.cell(4, 7).string("จำนวนเครื่องเฉลี่ยตามเวลา").style(text_style_header);
    worksheet.cell(4, 8).string("เพศชาย").style(text_style_header);
    worksheet.cell(4, 9).string("เพศหญิง").style(text_style_header);
    worksheet.cell(4, 10).string("พื้นที่(ภาค)").style(text_style_header);
    worksheet.cell(4, 11).string("พื้นที่จังหวัด (เรียง 5 จังหวัดสูงสุด ต่ำสุด) ").style(text_style_header);
    worksheet.column(11).setWidth(30);

    return worksheet;

}
const get_channels = async (channels_id  = null) => {
    let pool = await sql.connect(config);
  
    let $query_channel     = build_query( "channel" ,null , channels_id , null , null );
    let pool_channels = await pool.request().query($query_channel);
    let channels_obj = pool_channels.recordsets[0];
    let channels;
    if(channels_obj.length != 0){
        channels = JSON.parse(JSON.stringify(channels_obj));
        //channels   =  channels_obj;
       
    }else{
        return [{
            "status": false,
            "result_code": "-005",
            "result_desc": "not found channel"
        }]
    }
    return channels;
  

}

async function get_deviceviewer_report(date, channels_id) {


    let pool = await sql.connect(config);
  
    // case :  channel
    let channels = await get_channels(channels_id);
    if(channels[0].status != undefined){
        return channels;
    }
    // case : thaipbs 
    let iptv_channel_id = get_iptvchannel_id( channels_id ); // case : get iptv channel id 
    
    var month = get_dateobject(date, "month");
    var year = get_dateobject(date, "year");
    var rating_data_table = "rating_data_" + year + "_" + month;
    var channel_daily_devices_summary_table = "channel_daily_devices_summary_"+year+"_"+month;

    let $query_tvprogram= get_tvprogram( channels_id  , date);
    var tvprogram_selectquery = "";

  

    let tvprogram     = await pool.request()
    .query($query_tvprogram);
    let tvprogram_recordset = tvprogram.recordsets;
    let tvprogram_basetime_satalitereport_data;
    let tvprogram_basetime_iptvreport_data;
    let rating_data_daily_satalite;
    let rating_data_daily_iptv;
    let tvprogram_basetime_ytreport_data;
    let satalite_gender;
    let iptv_gender;
    let province_region_satalite;
    let province_region_iptv;
    tvprogram_recordset_obj = "";
    if(tvprogram_recordset.length > 0){
        var tvprogram_recordset_obj = JSON.parse(JSON.stringify(tvprogram_recordset));
        let $query_satalite_minute = build_query( "minute" , rating_data_table , channels_id , date  , null , null, tvprogram_recordset_obj);
        let $query_IPTV     = build_query( "minute_s3app" , rating_data_table , iptv_channel_id , date , null , null , tvprogram_recordset_obj );
      

        rating_data_daily_satalite = await pool.request()
        .query($query_satalite_minute.temptable);
        rating_data_daily_iptv     = await pool.request()
        .query($query_IPTV.temptable);
        // case : drop old temp table and create new one
        $var_temptable = `#temp_data`;
        // case : write new query
        let $query_satalite = build_query( "tvprogram_basetime_satalitereport" , rating_data_table ,  null , date , null , null , tvprogram_recordset_obj );
        
        // case : crete new temp table satalite
        $query_createtemptable = "SELECT * INTO " + $var_temptable +" FROM ( "+ $query_satalite.temptable + " ) as r1";  // case : create new temporary table 
       // console.log($query_createtemptable);
        let req = new sql.Request(pool);
        await req.batch($query_createtemptable);
       
        $query_tvprogrambaseonsatalite = ` SELECT merge_s3remote_id,channels_id ${$query_satalite.select_condition} FROM  #temp_data  inner join channels on #temp_data.channels_id = channels.id where channels.active = 1 group by channels_id,merge_s3remote_id`; // case : select temporary table data
        // console.log($query_tvprogrambaseonsatalite);
        // case :  create new temp gender
        $var_temptable_gender ="#temp_gender"; 
        $query_createtempgender = "select * into "+$var_temptable_gender+" from (select devices_id as dvid ,gender from "+channel_daily_devices_summary_table+" group by devices_id,gender) r1";  // case : create new temporary table 
     
        let req_tempgender = new sql.Request(pool);
        await req_tempgender.batch($query_createtempgender);
        // case  : (query) group by gender ( satalite )
        let $query_satalite_gender = build_query( "groupby_gender_satalite" ,null , channels_id , null , null , null , tvprogram_recordset_obj, null , $var_temptable );
        let query_tempgender_satalite = await req_tempgender.batch($query_satalite_gender.temptable);
        satalite_gender = query_tempgender_satalite.recordsets[0];
       
        // case : (query) group by province region satalite data
        let $query_satalite_addr = build_query( "groupby_addr_satalite" ,null , channels_id , null , null , null , tvprogram_recordset_obj, null , $var_temptable );
       
        let query_groupbyaddr_satalite = await req_tempgender.batch($query_satalite_addr.temptable);
        province_region_satalite = query_groupbyaddr_satalite.recordsets[0];


        // case :  (drop) temp satalite
        let query_temptable = await req.batch($query_tvprogrambaseonsatalite);
        await req.batch( "drop table " + $var_temptable ); // case : drop temp table
        tvprogram_basetime_satalitereport_data = query_temptable.recordset;
        
         // case : crete new temp table iptv
         $var_temptable_iptv = `#temp_data_iptv`;
         let $query_iptv = build_query( "tvprogram_basetime_iptvreport_s3app" , rating_data_table ,  null , date , null , null , tvprogram_recordset_obj );
         //tvprogram_basetime_iptvreport_data = get_iptvratingdata( $query_iptv , $var_temptable_iptv , pool);
         
         $query_createtemptable_iptv = "SELECT * INTO " + $var_temptable_iptv +" FROM ( "+ $query_iptv.temptable + " ) as r1";  // case : create new temporary table 
      //  console.log($query_createtemptable_iptv);
         let reqs3app = new sql.Request(pool);
         await reqs3app.batch($query_createtemptable_iptv);
     
         $query_tvprogrambaseiptv = ` SELECT tvchannels_id ${$query_iptv.select_condition} FROM  #temp_data_iptv group by tvchannels_id`; // case : select temporary table data
        // console.log($query_tvprogrambaseiptv);
         let query_temptable_iptv = await reqs3app.batch($query_tvprogrambaseiptv);

        // case  : (query) group by gender ( iptv )
        let $query_iptv_gender;
        if(satalite_gender[0] != null){
            let $tvchannels_id      =satalite_gender[0].merge_s3remote_id;
            $query_iptv_gender = build_query( "groupby_gender_iptv" ,null , $tvchannels_id , null , null , null , tvprogram_recordset_obj, null , $var_temptable_iptv );
            let query_tempgender_iptv = await req_tempgender.batch($query_iptv_gender.temptable);
            iptv_gender = query_tempgender_iptv.recordsets[0];
        }else{
            iptv_gender = null;
        }
        
        // case : (query) group by province region iptv data
        if(province_region_satalite[0] != null){
            let $tvchannels_id_groupaddr = province_region_satalite[0].merge_s3remote_id;
            
            let $query_iptv_addr = build_query( "groupby_addr_iptv" ,null , $tvchannels_id_groupaddr , null , null , null , tvprogram_recordset_obj, null , $var_temptable_iptv );
         
            let query_groupbyaddr_iptv = await req_tempgender.batch($query_iptv_addr.temptable);
            province_region_iptv = query_groupbyaddr_iptv.recordsets[0];
        }else{
            province_region_iptv = null;
        }
     

        // casae : (drop) temp gender
        await req_tempgender.batch( "drop table " + $var_temptable_gender ); // case : drop temp table

         // case :  (drop) temp iptv
         await reqs3app.batch( "drop table " + $var_temptable_iptv ); // case : drop temp table
         tvprogram_basetime_iptvreport_data = query_temptable_iptv.recordset;

         // case : create new temp youtube view
         $var_temptable_youtube = `#temp_data_youtube`;
         let youtube_data_table = "youtube_rating_log_" + year + "_" + month;
         let $query_youtube = build_query( "youtubeviewreport_satalite" , rating_data_table ,  null , date , null , null , tvprogram_recordset_obj ,  youtube_data_table);
         //tvprogram_basetime_iptvreport_data = get_iptvratingdata( $query_youtube , $var_temptable_youtube , pool);
         
         $query_createtemptable_youtube = "SELECT * INTO " + $var_temptable_youtube +" FROM ( "+ $query_youtube.temptable + " ) as r1";  // case : create new temporary table 
         let reqs3ratingyt = new sql.Request(pool);
         await reqs3ratingyt.batch($query_createtemptable_youtube);

         $query_tvprogrambaseyoutube = ` SELECT devices_id ${$query_youtube.select_condition} FROM  #temp_data_youtube group by devices_id`; // case : select temporary table data
         let query_temptable_yt = await reqs3ratingyt.batch($query_tvprogrambaseyoutube);
         await reqs3ratingyt.batch( "drop table " + $var_temptable_youtube ); // case : drop temp table
         tvprogram_basetime_ytreport_data = query_temptable_yt.recordset;

       

    }
    var arr_data = set_arrdata( [] );
   
    if (rating_data_daily_satalite != null ) {
        let rating_data_daily_satalite_obj = rating_data_daily_satalite.recordsets[0];
        var rating_data_daily_satalite_ = JSON.parse(JSON.stringify(rating_data_daily_satalite_obj));
  
        if (rating_data_daily_satalite_ != null) {

            //  arr_data = set_satalitetotal_arr( arr_data , rating_data_daily_satalite_); // case : set satalite data
            if(rating_data_daily_iptv != null){
                let rating_data_daily_iptv_obj = rating_data_daily_iptv.recordsets[0];
             
                var rating_data_daily_iptv_ = JSON.parse(JSON.stringify(rating_data_daily_iptv_obj));
                arr_data = merge_andsortallofthisfuck( rating_data_daily_satalite_ , rating_data_daily_iptv_);
            }
            else{
                arr_data = rating_data_daily_satalite_;
            }
           
           

           
             let $filename =   await write_excelfile( arr_data  , channels , tvprogram_recordset_obj 
                , tvprogram_basetime_satalitereport_data , tvprogram_basetime_iptvreport_data , channels_id
                , tvprogram_basetime_ytreport_data , satalite_gender , iptv_gender 
                , province_region_satalite , province_region_iptv );
              
            const file = $filename;
          
            return   [{
                "status": true,
                "data": file,
                "result_code": "000",
                "result_desc": "Success"
            }];
              
           
           
        } else {
            return [{
                "status": false,
                "result_code": "-005",
                "result_desc": "not found data"
            }];
        }


    }
}
async function get_sataliteratingdata(){
    
}
async function get_iptvratingdata($query_iptv ,$var_temptable_iptv , pool ){
       
    $query_createtemptable_iptv = "SELECT * INTO " + $var_temptable_iptv +" FROM ( "+ $query_iptv.temptable + " ) as r1";  // case : create new temporary table 
    let reqs3app = new sql.Request(pool);
    await reqs3app.batch($query_createtemptable_iptv);

    $query_tvprogrambaseiptv = ` SELECT tvchannels_id ${$query_iptv.select_condition} FROM  #temp_data_iptv group by tvchannels_id`; // case : select temporary table data
    let query_temptable_iptv = await reqs3app.batch($query_tvprogrambaseiptv);
    await reqs3app.batch( "drop table " + $var_temptable_iptv ); // case : drop temp table
    let tvprogram_basetime_iptvreport_data = query_temptable_iptv.recordset;
    return tvprogram_basetime_iptvreport_data;
}
function set_satalitetotal_arr(arr_data , rating_data_daily_satalite_){
    rating_data_daily_satalite_.forEach(element => {
        element.forEach(value => {
           
            var _h = value.ชั่วโมงที่;
            var _m = parseInt(value.นาทีที่);
            var _v = parseInt(value.กล่องรับชม);
            _v = _v > 0 ? _v : 0;
            arr_data[_h][_m][2] += _v; 
            
            
        });
    });

    return arr_data;
}
function set_iptvtotal_arr(arr_data , rating_data_daily_iptv_){
    rating_data_daily_iptv_.forEach(element => {
        element.forEach(value => {
           
            var _h = value.ชั่วโมงที่;
            var _m = parseInt(value.นาทีที่);
            var _v = parseInt(value.กล่องรับชม);
            _v = _v > 0 ? _v : 0;
            arr_data[_h][_m][2] += _v; 
            
            
        });
    });

    return arr_data;
}
function set_arrdata(arr_data){
    for($time =0 ; $time<=23; $time ++){
     
        var text      = $time < 10 ? "0"+$time+":00-"+"0"+$time+":59" :  $time+":00-"+$time+":59";
        var arr_minute= [];
        for($minute =0;$minute<=59;$minute ++){
            arr_minute.push([ text , $minute , 0]);
        }
        arr_data.push(arr_minute);
    }

    return arr_data;
}
function get_iptvchannel_id(channels_id){
    let iptv_channel_id;
    if(channels_id == 252){ // case : tpbs
        iptv_channel_id = 64;
    }
    return iptv_channel_id;

}
function build_query(type, rating_data_table = null, channels_id = null, date = null  , report = null , select_condition = null , tvprogram_recordset_obj = null , youtube_data_table = null , temp_table = null) {
    let $query;
    $20channel_where  = list20tvchannel_ondemand();
    if (type == 'minute' ) {
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            
        
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
            select_condition +=   ` ,Count(case when r1.d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
            subquery_select_codition += `,COUNT(CASE WHEN startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}' THEN 1 END ) AS 'd_${element.id}'`;
    
        });


        $query    =  "SELECT channels.id as channels_id,merge_s3remote_id,r1.hour as hour ,r1.MINUTE as minute ,(CONVERT(varchar(12),r1.hour) + '_' + CONVERT(varchar(12),r1.MINUTE)) as h_m  "+
        select_condition+" FROM ( SELECT  DATEPART(HOUR, startview_datetime) as hour,DATEPART(MINUTE, startview_datetime) as MINUTE,devices_id,count(*) as count_allratingrecord ,channels_id"+subquery_select_codition+" FROM dbo."+rating_data_table+"   WHERE startview_datetime >= '"+date+" 00:00:01' AND startview_datetime <= '"+date+" 23:59:59' AND channels_id > 0  AND channels_id = "+channels_id+" GROUP BY  channels_id, DATEPART(YEAR, startview_datetime),DATEPART(MONTH, startview_datetime),DATEPART(DAY, startview_datetime),DATEPART(HOUR, startview_datetime),DATEPART(minute , startview_datetime),devices_id )r1 inner join channels on r1.channels_id = channels.id where channels.active = 1 group by r1.hour,r1.MINUTE,channels.id,channels.merge_s3remote_id   order by r1.hour , r1.MINUTE ";
       
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;

       
            
    }else if (type == 'overview_minute' ) {
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        //console.log(tvprogram_recordset_obj);
        tvprogram_recordset_obj[0].forEach(element => {
            
           // console.log(element);
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
            select_condition +=   ` ,Count(case when r1.d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
            subquery_select_codition += `,COUNT(CASE WHEN startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}' THEN 1 END ) AS 'd_${element.id}'`;
    
        });


        $query    =  "SELECT channels.id as channels_id,merge_s3remote_id,r1.hour as hour ,r1.MINUTE as minute ,CONVERT(varchar(12),merge_s3remote_id) + '_'+(CONVERT(varchar(12),r1.hour) + '_' + CONVERT(varchar(12),r1.MINUTE)) AS h_m  "+
        select_condition+" FROM ( SELECT  DATEPART(HOUR, startview_datetime) as hour,CAST(DATEPART(MINUTE, startview_datetime)/30 AS char(2)) as MINUTE,devices_id,count(*) as count_allratingrecord ,channels_id"+subquery_select_codition+" FROM "+rating_data_table+"   WHERE startview_datetime >= '"+date+" 00:00:01' AND startview_datetime <= '"+date+" 23:59:59' AND channels_id > 0  GROUP BY  channels_id, DATEPART(YEAR, startview_datetime),DATEPART(MONTH, startview_datetime),DATEPART(DAY, startview_datetime),DATEPART(HOUR, startview_datetime),DATEPART(MINUTE, startview_datetime)/30 ,devices_id )r1 inner join channels on r1.channels_id = channels.id where channels.active = 1 group by r1.hour,r1.MINUTE,channels.id,channels.merge_s3remote_id   order by r1.hour , r1.MINUTE ";
       // console.log($query);
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;

       
            
    }else if(type == 'overview_minute_s3app'){
        
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            
        
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
            select_condition +=   ` ,Count(case when r1.d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
            subquery_select_codition += `,COUNT(CASE WHEN startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}' THEN 1 END ) AS 'd_${element.id}'`;
    
        });

       
        $query    =  "SELECT r1.tvchannels_id,r1.hour as hour ,r1.MINUTE as minute,CONVERT(varchar(12),tvchannels_id) + '_'+(CONVERT(varchar(12),r1.hour) + '_' + CONVERT(varchar(12),r1.MINUTE)) AS h_m "+
        select_condition+" FROM ( SELECT  DATEPART(HOUR, startview_datetime) as hour,DATEPART(MINUTE, startview_datetime) / 30 as MINUTE,devices_id,count(*) as count_allratingrecord ,tvchannels_id"+subquery_select_codition+" FROM "+rating_data_table+" WHERE startview_datetime >= '"+date+" 00:00:01' AND startview_datetime <= '"+date+" 23:59:59' AND tvchannels_id > 0   GROUP BY  tvchannels_id, DATEPART(YEAR, startview_datetime),DATEPART(MONTH, startview_datetime),DATEPART(DAY, startview_datetime),DATEPART(HOUR, startview_datetime),DATEPART(minute , startview_datetime) / 30,devices_id )r1 group by r1.hour,r1.MINUTE,r1.tvchannels_id order by r1.hour , r1.MINUTE ";
     
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;
        
   
    }
    else if(type == 'minute_s3app'){
        
            let subquery_select_codition = "";
            let select_condition         = "";
            var count = 0;
            tvprogram_recordset_obj[0].forEach(element => {
                
            
                var start_time =  set_starttime( element.start_time );
                var end_time   =  set_endtime( element.end_time );
                select_condition +=   ` ,Count(case when r1.d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
                subquery_select_codition += `,COUNT(CASE WHEN startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}' THEN 1 END ) AS 'd_${element.id}'`;
        
            });


            $query    =  "SELECT r1.tvchannels_id,r1.hour as hour ,r1.MINUTE as minute,(CONVERT(varchar(12),r1.hour) + '_' + CONVERT(varchar(12),r1.MINUTE)) as h_m  "+
            select_condition+" FROM ( SELECT  DATEPART(HOUR, startview_datetime) as hour,DATEPART(MINUTE, startview_datetime) as MINUTE,devices_id,count(*) as count_allratingrecord ,tvchannels_id"+subquery_select_codition+" FROM "+config_s3app.database+".dbo."+rating_data_table+" WHERE startview_datetime >= '"+date+" 00:00:01' AND startview_datetime <= '"+date+" 23:59:59' AND tvchannels_id > 0  AND tvchannels_id = "+channels_id+" GROUP BY  tvchannels_id, DATEPART(YEAR, startview_datetime),DATEPART(MONTH, startview_datetime),DATEPART(DAY, startview_datetime),DATEPART(HOUR, startview_datetime),DATEPART(minute , startview_datetime),devices_id )r1 group by r1.hour,r1.MINUTE,r1.tvchannels_id order by r1.hour , r1.MINUTE ";
         
            var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
            return query_obj;
            
       
    }
    else if(type == "channel"){
        condition_where = "";
        if(channels_id != null ){
            condition_where += " where id  = " + channels_id;
        }
        $query = `select * from ${config.database}.dbo.channels  ${condition_where}`;
    }
    else if(type =="tvprogram_basetime"){
        $query = `SELECT  channels_id
        ,devices_id
        ,COUNT(*) AS count_allratingrecord
        FROM rating_data_2022_2
        WHERE startview_datetime >= '2022-02-23 00:00:01'
        AND startview_datetime <= '2022-02-23 23:59:59'
        GROUP BY  channels_id
                ,devices_id`;
    }else if(type =="tvprogram_basetime_satalitereport"){
      
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            
           
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
            select_condition +=   ` ,Count(case when d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
            subquery_select_codition += `,COUNT(CASE WHEN startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}' THEN 1 END ) AS 'd_${element.id}'`;
     
        });


        $query    =  `SELECT  channels_id,devices_id${subquery_select_codition} FROM ${rating_data_table} WHERE startview_datetime >= '${date} 00:00:01' AND startview_datetime <= '${date} 23:59:59' AND channels_id > 0  GROUP BY  channels_id,devices_id`;
     
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;


    }else if(type =="overview_basetime_satalitereport"){
        let subquery_select_codition = "";
        let select_period_cond       = "";
        let select_condition         = ` ,COUNT(case WHEN overview >= 1 THEN 1 end) AS 'overview' `;    
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            
           
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
           
            if(select_period_cond.length > 0){
                select_period_cond += " or ";
            }
            select_period_cond+=  ` (startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}') `;
           
     
        });
        subquery_select_codition += `,COUNT(CASE WHEN ${select_period_cond} THEN 1 END ) AS 'overview'`;

        $query    =  `SELECT  channels_id,devices_id${subquery_select_codition} FROM ${rating_data_table} WHERE startview_datetime >= '${date} 00:00:01' AND startview_datetime <= '${date} 23:59:59' AND channels_id > 0  GROUP BY  channels_id,devices_id`;
     
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;
        
    }else if(type =="overview_basetime_iptvreport"){
        let subquery_select_codition = "";
        let select_period_cond       = "";
        let select_condition         = ` ,COUNT(case WHEN overview >= 1 THEN 1 end) AS 'overview' `;    
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            
           
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
           
            if(select_period_cond.length > 0){
                select_period_cond += " or ";
            }
            select_period_cond+=  ` (startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}') `;
           
     
        });
        subquery_select_codition += `,COUNT(CASE WHEN ${select_period_cond} THEN 1 END ) AS 'overview'`;

        $query    =  `SELECT  tvchannels_id,devices_id${subquery_select_codition} FROM ${config_s3app.database}.dbo.${rating_data_table} WHERE startview_datetime >= '${date} 00:00:01' AND startview_datetime <= '${date} 23:59:59' AND tvchannels_id > 0  GROUP BY  tvchannels_id,devices_id`;
      
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;
        
    }else if(type =="tvprogram_basetime_iptvreport_s3app"){
     
        // $20channel_where  = list20tvchannel_ondemand("iptv");
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
        
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
            select_condition +=   ` ,Count(case when d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
            subquery_select_codition += `,COUNT(CASE WHEN startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}' THEN 1 END ) AS 'd_${element.id}'`;
     
        });

        // AND (${$20channel_where})
        $query    =  `SELECT  tvchannels_id,devices_id${subquery_select_codition} FROM ${config_s3app.database}.dbo.${rating_data_table} WHERE startview_datetime >= '${date} 00:00:01' AND startview_datetime <= '${date} 23:59:59' AND tvchannels_id > 0  GROUP BY  tvchannels_id,devices_id`;
       //  console.log($query);
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;


    }else if(type =="youtubeviewreport_satalite"){
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            
           
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
            select_condition +=   ` ,Count(case when d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
            subquery_select_codition += `,COUNT(CASE WHEN startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}' THEN 1 END ) AS 'd_${element.id}'`;
     
        });


        $query    =  `SELECT  devices_id${subquery_select_codition} FROM ${youtube_data_table} WHERE startview_datetime >= '${date} 00:00:01' AND startview_datetime <= '${date} 23:59:59' GROUP BY devices_id`;

        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;
    }else if(type == "overview_groupby_gender_satalite"){
        let subquery_select_codition = "";
        var count = 0;
        let select_condition         = ` ,COUNT(case WHEN overview >= 1 THEN 1 end) AS 'overview' `;    
        var count = 0;
        let select_period_cond = "";
        tvprogram_recordset_obj[0].forEach(element => {
            
           
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
           
            if(select_period_cond.length > 0){
                select_period_cond += " or ";
            }
            select_period_cond+=  ` (startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}') `;
           
     
        });
        subquery_select_codition += `,COUNT(CASE WHEN ${select_period_cond} THEN 1 END ) AS 'overview'`;

        $query = "select     (CASE WHEN #temp_gender.gender IS NULL  THEN 'none' WHEN #temp_gender.gender = ''  THEN 'none1'  WHEN #temp_gender.gender = '-'  THEN 'none2'   ELSE #temp_gender.gender END)  as gender, merge_s3remote_id"+select_condition+",channels_id, count(*) as total_row from "+temp_table+" INNER JOIN channels  ON "+temp_table+".channels_id = channels.id left join #temp_gender on "+temp_table+".devices_id = #temp_gender.dvid group by gender  , channels_id ,channels.merge_s3remote_id order by channels_id asc , #temp_gender.gender asc" ;

        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;

    }
    else if(type == "groupby_gender_satalite"){
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            select_condition +=   ` ,Count(case when d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
        });
        $where =  "";
        if(channels_id != null){
            $where += " where channels_id = " + channels_id +" ";
        }
        $query = "select     (CASE WHEN #temp_gender.gender IS NULL  THEN 'none' WHEN #temp_gender.gender = ''  THEN 'none1'  WHEN #temp_gender.gender = '-'  THEN 'none2'   ELSE #temp_gender.gender END)  as gender, merge_s3remote_id"+select_condition+",channels_id, count(*) as total_row from "+temp_table+" INNER JOIN channels  ON #temp_data.channels_id = channels.id left join #temp_gender on "+temp_table+".devices_id = #temp_gender.dvid  " + $where + " group by  gender  , channels_id ,channels.merge_s3remote_id order by channels_id asc , #temp_gender.gender asc" ;

        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;

    }else if(type == "groupby_gender_iptv"){
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            select_condition +=   ` ,Count(case when d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
        });
        $where =  "";
        if(channels_id != null){
            $where += " where tvchannels_id = " + channels_id +" ";
        }
        $query = "select   (CASE WHEN #temp_gender.gender IS NULL  THEN 'none' WHEN #temp_gender.gender = ''  THEN 'none1'  WHEN #temp_gender.gender = '-'  THEN 'none2'   ELSE #temp_gender.gender END)  as gender"+select_condition+",tvchannels_id, count(*) as total_row from "+temp_table+" left join #temp_gender on "+temp_table+".devices_id = #temp_gender.dvid "+$where+" group by  gender  , tvchannels_id order by tvchannels_id asc , #temp_gender.gender asc" ;
        
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;

    }
    else if(type == "overview_groupby_addr_satalite"){
       let subquery_select_codition = "";
        var count = 0;
        let select_condition         = ` ,COUNT(case WHEN overview >= 1 THEN 1 end) AS 'overview' `;    
        var count = 0;
        let select_period_cond = "";
        tvprogram_recordset_obj[0].forEach(element => {
            
           
            var start_time =  set_starttime( element.start_time );
            var end_time   =  set_endtime( element.end_time );
           
            if(select_period_cond.length > 0){
                select_period_cond += " or ";
            }
            select_period_cond+=  ` (startview_datetime > '${date} ${start_time}' AND startview_datetime < '${date} ${end_time}') `;
           
     
        });
        subquery_select_codition += `,COUNT(CASE WHEN ${select_period_cond} THEN 1 END ) AS 'overview'`;

        $query = " select merge_s3remote_id,channels_id,(CASE WHEN province_region IS NULL  THEN 'none' WHEN province_region = ''  THEN 'none1'  WHEN province_region = '-'  THEN 'none2'   ELSE province_region END)  as province_region " + select_condition + " from "+temp_table+" left join device_addresses on "+temp_table+".devices_id = device_addresses.devices_id INNER JOIN channels  ON "+temp_table+".channels_id = channels.id  group by channels_id,province_region,"+temp_table+".channels_id , merge_s3remote_id"
      
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;
    }
    else if(type == "groupby_addr_satalite"){
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            select_condition +=   ` ,Count(case when d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
        });
             
        $where =  "";
        if(channels_id != null){
            $where +=" where "+temp_table+".channels_id = " + channels_id + " ";
        }

        $query = " select merge_s3remote_id,channels_id,(CASE WHEN province_region IS NULL  THEN 'none' WHEN province_region = ''  THEN 'none1'  WHEN province_region = '-'  THEN 'none2'   ELSE province_region END)  as province_region " + select_condition + " from "+temp_table+" left join device_addresses on "+temp_table+".devices_id = device_addresses.devices_id INNER JOIN channels  ON "+temp_table+".channels_id = channels.id "+$where+" group by province_region,"+temp_table+".channels_id , merge_s3remote_id"
      
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;
    }else if(type == "groupby_addr_iptv"){
        let subquery_select_codition = "";
        let select_condition         = "";
        var count = 0;
        tvprogram_recordset_obj[0].forEach(element => {
            select_condition +=   ` ,Count(case when d_${element.id} >= 1 then 1 end) as 'field_${element.id}'`;    
        });
        $where =  "";
        if(channels_id != null){
            $where = "where "+temp_table+".tvchannels_id = " + channels_id + " ";
        }

        $query = "select tvchannels_id,(CASE WHEN province_region IS NULL  THEN 'none' WHEN province_region = ''  THEN 'none1'  WHEN province_region = '-'  THEN 'none2'   ELSE province_region END)  as province_region" + select_condition + " from "+temp_table+" left join device_addresses on "+temp_table+".devices_id = device_addresses.devices_id  "+$where+" group by province_region,"+temp_table+".tvchannels_id"
       
        var query_obj = { 'temptable': $query , 'select_condition' : select_condition}
        return query_obj;
    }
    return $query;

}
function set_starttime(start_time){
    var start =set_timezone(start_time, "Asia/Jakarta");
    var st_h  = start.getUTCHours();
    st_h =  st_h < 10 ? "0"+st_h : st_h;
    var st_m  = start.getUTCMinutes();
    st_m =  st_m < 10 ? "0"+st_m : st_m;
    var st_c  = start.getUTCSeconds();
    st_c =  st_c < 10 ? "0"+st_c : st_c;

    return st_h + ":" + st_m + ":" + st_c;
}

function set_endtime(end_time){
    var end =set_timezone(end_time, "Asia/Jakarta");
    var end_h  = end.getUTCHours();
    end_h =  end_h < 10 ? "0"+end_h : end_h;
    var end_m  = end.getUTCMinutes();
    end_m =  end_m < 10 ? "0"+end_m : end_m;
    var end_s = end.getUTCSeconds();
    end_s =  end_s < 10 ? "0"+end_s : end_s;
    return end_h + ":" + end_m + ":" + end_s;
}

    /* all function below for postgresql database */
    async function get_deviceviewerbehaviour(devices_id, datereport) {

        // database instance;
        const pool = new Pool(config_pg);

        // pool.query('SELECT * FROM devices_viewerbehavior ORDER BY id ASC', (error, results) => {})
        const result = await pool.query('SELECT * FROM devices_viewerbehavior where devices_id = $1 ORDER BY id ASC', [devices_id]);
        if (!result || !result.rows || !result.rows.length) return {
            "status": false,
            "result_code": "-005",
            "result_desc": "not found data"
        };
        return {
            "status": true,
            data: result.rows,
            "result_code": "000",
            "result_desc": "Success"
        };

    }
    function get_keybyvalue_minute(object, value , type = null) {
        if(type != null){
            return Object.keys(object).find(key => object[key].h_m == value);
        }else{
            return Object.keys(object).find(key => object[key].h_m == value);
        }
    }

    function get_keybyvalue_gender(object, value , type = null) {
        return Object.keys(object).find(key => object[key].gender == value);
    }
    function get_keybyvalue_region(object, value , type = null) {
  
        return Object.keys(object).find(key => object[key].province_region == value);

        
    }

    function get_keybyvalue(object, value , type = null) {
        if(type != null){
            if(type =='element.province_region'){
                return Object.keys(object).find(key => object[key].province_region == value);
            }else{
            return Object.keys(object).find(key => object[key].tvchannels_id == value);
            }
        }else{
         
                return Object.keys(object).find(key => object[key].channels_id == value);
            
        }
    }

    function merge_andsortallofthisfuck(tvprogram_basetime_satalitereport_data  ,tvprogram_basetime_iptvreport_data , channels_id){
        var key = 0;
        var merge_data = [];
        // case : summarize two object
        
        tvprogram_basetime_satalitereport_data.forEach(element => {
           
            let h_m =  element.h_m;
         
            let iptv_key = get_keybyvalue_minute( tvprogram_basetime_iptvreport_data ,h_m  , "iptv");
            if(iptv_key > 0){
                element =  sum_objectbykey( element , tvprogram_basetime_iptvreport_data[iptv_key]);
            }
            merge_data.push(element);
           
            ++ key;
          
        })

        
        

        return merge_data;
    }
    function get_thistvprogram(tvprogram_basetime_satalitereport_data  ,tvprogram_basetime_iptvreport_data , channels_id){
        var key = 0;
        var merge_data = [];
     
        // case : summarize two object
        tvprogram_basetime_satalitereport_data.forEach(element => {
            let tvchannel_id =  element.merge_s3remote_id;
         
            let iptv_key = get_keybyvalue( tvprogram_basetime_iptvreport_data ,tvchannel_id  , "iptv");
            if(iptv_key >= 0 && iptv_key != undefined){
                element =  sum_objectbykey( element , tvprogram_basetime_iptvreport_data[iptv_key]);
            }
            merge_data.push(element);
            ++ key;
          
        })
        // case : sort object
        let tvprogram_sortable = tvprogram_sortables( merge_data , channels_id);
        return tvprogram_sortable;
    }

    function merge_data(tvprogram_basetime_satalitereport_data  ,tvprogram_basetime_iptvreport_data , keyspecific =null){
        var key = 0;
        var merge_data = [];
        let key_;
        // case : summarize two object
       
        var c = 0;

        

        tvprogram_basetime_satalitereport_data.forEach(element => {
         
            if(keyspecific == null){ key_ =  element.merge_s3remote_id; }else { key_ = eval(keyspecific); }
            let findbytype =  keyspecific == null ? "iptv" : keyspecific;
           
            let iptv_key = get_keybyvalue( tvprogram_basetime_iptvreport_data ,key_  , findbytype);
            
            if(iptv_key >= 0 && iptv_key != undefined){
               // ++ c;
                element =  sum_objectbykey( element , tvprogram_basetime_iptvreport_data[iptv_key]);
            }
            merge_data.push(element);
            
            ++ key;
          
        })

        
      
        return merge_data;
    }

    function merge_data_original(original_tvprogram_basetime_satalitereport_data  ,original_tvprogram_basetime_iptv_data , keyspecific =null){
        var key = 0;
        var merge_data = [];
        let key_;
        // case : summarize two object
        original_tvprogram_basetime_satalitereport_data.forEach(element => {
         
            if(keyspecific == null){ key_ =  element.merge_s3remote_id; }else { key_ = eval(keyspecific); }
            let findbytype =  keyspecific == null ? "iptv" : keyspecific;
           
            let iptv_key = get_keybyvalue( original_tvprogram_basetime_iptv_data ,key_  , findbytype);
           
            if(iptv_key >= 0 && iptv_key != undefined){
                element =  sum_objectbykey( element , original_tvprogram_basetime_iptv_data[iptv_key]);
            }
            merge_data.push(element);
            
            ++ key;
          
        })
        
        return merge_data;
    }

    function merge_data_gender(tvprogram_basetime_satalitereport_data  ,tvprogram_basetime_iptvreport_data ){
        var key = 0;
        var merge_data = [];
        // case : summarize two object
      
        tvprogram_basetime_satalitereport_data.forEach(element => {
            
            let gender =  element.gender;
            let iptv_key = get_keybyvalue_gender( tvprogram_basetime_iptvreport_data ,gender  , "iptv");
            if(iptv_key >= 0 && iptv_key != undefined){
                element =  sum_objectbykey( element , tvprogram_basetime_iptvreport_data[iptv_key]);
            }
            merge_data.push(element);
            
            ++ key;
          
        })
        return merge_data;
    }

    function tvprogram_sortables(merge_data , channels_id){
        let thischannel_tvprogramkey = get_keybyvalue( merge_data ,channels_id );
        var tvprogram_obj = merge_data[thischannel_tvprogramkey];
       
        if(typeof merge_data[thischannel_tvprogramkey] != undefined) { delete merge_data[thischannel_tvprogramkey].channels_id;} // case : delete this object key
        if(typeof merge_data[thischannel_tvprogramkey] != undefined) { delete merge_data[thischannel_tvprogramkey].merge_s3remote_id};
        if(typeof merge_data[thischannel_tvprogramkey] != undefined) { delete merge_data[thischannel_tvprogramkey].tvchannels_id};
        const tvprogram_sortable = Object.fromEntries(
            Object.entries(tvprogram_obj).sort(([,a],[,b]) => b - a)
        );

        return tvprogram_sortable;
    }

    function list20tvchannel_ondemand(type = null){

        
            // 463 ch3
            // 525 altv
            // 271 ch9 ( mcot hd)
            // 413 ch5
            // 416 ch7
            // 251 nbt
            // 268 channel8
            // 264 work point
            // 272 one 31
            // 277 PPTV
            // 270 Mono 29
            // 275 Amarin TV
            // 263 Nation
            // 257 TNN 
            // 273 Thairath TV
            // 266 GMM25 ( 71)
            // 259 JKN 18
            // 265 True 4u
            // 568 T Sport 
            // 252 Thai PBS
            

        $where_in = "";
        if(type == null){ // case : null = satalite
            $where_in =  " channels_id = 463 or  channels_id = 525  or  channels_id = 271  or  channels_id = 413  or  channels_id = 416 ";
            $where_in += " or  channels_id = 251    or channels_id = 268  ";
            $where_in += " or channels_id = 264  or channels_id = 272  or channels_id = 277   or channels_id = 270 or channels_id = 275   ";
            $where_in += " or channels_id = 263 or channels_id = 257 or channels_id = 273  or channels_id = 266  or channels_id = 259  ";
            $where_in += " or channels_id = 265  or channels_id = 568 or channels_id = 252";
        }else{
            // 
            $where_in  = " tvchannels_id = 66 or tvchannels_id = 65 or  tvchannels_id = 62  or  tvchannels_id = 67 or  tvchannels_id = 63 ";
            $where_in += " or tvchannels_id = 3 or tvchannels_id = 2 or  tvchannels_id = 48  or  tvchannels_id = 76 or  tvchannels_id = 73 ";
            $where_in += " or tvchannels_id = 74 or tvchannels_id = 39 or  tvchannels_id = 45  or  tvchannels_id = 69  or  tvchannels_id = 71"; 
            $where_in += " or  tvchannels_id = 70 or  tvchannels_id = 68   or  tvchannels_id = 64  or tvchannels_id = 139 or tvchannels_id=225 ";
        }
        return $where_in;
    }

    function tvdigitalchannel_ondemand(type = null , useby = null){
       
            
        var arr;
        if(type == "iptv"){
            
            // case : iptv
            if(useby == null){
                arr = [66 ,225, 65 , 62 , 67 ,63 , 3 , 2 , 48 , 76 , 73 , 74 , 39 ,45 , 69,71 , 70 ,68 , 64 , 139];
            }else{

            }
        }else{
            // case : satalite
            if(useby == null){
                arr = [252, 525,463 , 413  ,416, 271 , 268 , 251 , 264 , 272 , 277 , 270 , 275 , 263 , 257 , 273 , 266 , 259 , 265, 568  ];
            }else{
                // 252 Thai PBS
                // 525 altv
                // 463 ch3
                // 413 ch5
                // 416 ch7
                // 271 ch9 ( mcot hd)
                // 268 channel8
                // 251 nbt
                // 264 work point
                // 272 one 31
                // 277 PPTV
                // 270 Mono 29
                // 275 Amarin TV
                // 263 Nation
                // 257 TNN 16 ใส่เป็น TNN
                // 273 Thairath TV
                // 266 GMM25 ( 71)
                // 259 JKN 18
                // 265 True 4u
                // 568 T-SPORTS 7 
            
                arr = ["THAI PBS", "ALTV", "CH-3" , "CH-5"  ,"CH-7", "MCOT-HD" , "CH-8" , "NBT" , "WORKPOINT" , "ONE" , "PPTV" , "MONO29" , "AMARIN TV"
                 , "NATION TV" , "TNN" , "THAIRATH TV" 
                , "GMM25" , "JKN18" , "TRUE4U", "T SPORTS"];
            }
        }
        return arr;

    }
    function sum_objectbykey(...objs) {
        return objs.reduce((a, b) => {
          for (let k in b) {
              
              if(k != 'tvchannel_id' && k!= 'channels_id' && k != 'merge_s3remote_id' ){
                if( k == 'h_m'  || k=='hour' || k =='minute' || k =='gender' || k == 'total_row' || k == 'province_region' ){
                  
                    if (b.hasOwnProperty(k)) { a[k] = b[k] } ;
                }else{
                    if (b.hasOwnProperty(k))
                    a[k] = (a[k] || 0) + b[k];
                }
              }else{
                if (b.hasOwnProperty(k))
                a[k] = b[k];
              }
          }
          return a;
        }, {});
      }
    
      function set_iptvdefaultObjifzero(originaldata , channel_arr){
            var checkarray = [];
            originaldata.forEach(function(ec, idx, obj) { // case : get array value not exist on channel array
                if(channel_arr.includes(ec.tvchannels_id )){
                    checkarray.push(ec.tvchannels_id );
                }
                
            });
            let difference = channel_arr.filter(x => !checkarray.includes(x));

            // console.log(originaldata);
            if(difference.length > 0){
                let defaultobj  = Object.keys(originaldata[0]);
                console.log(difference);
                difference.forEach(function(no, ix, oj) {
                    const obj = {};
                    for (const key of defaultobj) {
                        if(key == 'tvchannels_id'){ obj[key] = no;}else {obj[key] = 0; }
                    }
                    originaldata.push(obj);
                    
                });
            }
            return originaldata;
      }
    async function find_channeldelete(type , data){
        let channel_arr =  tvdigitalchannel_ondemand(type);
        
        let originaldata = data;
        if(type == "iptv"){
            originaldata =  set_iptvdefaultObjifzero(originaldata , channel_arr);
        }
        
        let newdata = data;
        // check original data for null value
        

        newdata.forEach(function(e, index, object) {
            if(type != "iptv"){

                if(!channel_arr.includes(e.channels_id )){
                   
                   delete originaldata[index];
                }
            }else{
                if(type =="iptv"){
                  
                    if(!channel_arr.includes(e.tvchannels_id)){
                       
                        delete originaldata[index];
                    }
                }
            }
       });
       let neworiginaldata = originaldata.filter(function( element ) {
           
            return element != undefined;
        });
        
       
        return neworiginaldata;
 
    }
    function sum_everychannel_baseontime(array){

       
        var newArr = [];

        array.forEach(function(e, index, object) {
            for ( var property in array[index] ) {
              
                if(newArr[property]==undefined && property !='tvchannels_id' && property != 'channels_id' && property != 'merge_s3remote_id'){
                    newArr[property] = 0;
                    newArr[property] += parseInt(array[index][property]);
                }else{
                    if(property !='tvchannels_id' && property != 'channels_id' && property != 'merge_s3remote_id'){
                    newArr[property] += parseInt(array[index][property]);
                    }
                }
               
              }
          
        });
    
        return newArr;
    }
   async function write_excelfile(arr_data , channels  ,tvprogram_recordset_obj , tvprogram_basetime_satalitereport_data_s , tvprogram_basetime_iptvreport_data_s , channels_id , tvprogram_basetime_ytreport_data = null , satalite_gender = null , iptv_gender = null ,province_region_satalite = null  , province_region_iptv = null) {
       
       
        let original_tvprogram_basetime_satalitereport_data = tvprogram_basetime_satalitereport_data_s;
        let original_tvprogram_basetime_iptv_data = tvprogram_basetime_iptvreport_data_s;
        // case : save log file
        let logsatalite    = JSON.stringify(original_tvprogram_basetime_satalitereport_data);
        var logsatalite_fn = 'logsatalite_' + Math.round(+new Date()/1000)+ ".json";
        fs.writeFileSync('./json/daily/'+logsatalite_fn, logsatalite);

        let logiptv    = JSON.stringify(original_tvprogram_basetime_iptv_data);
        var logiptv_fn = 'logiptv_' + Math.round(+new Date()/1000) + ".json";
        fs.writeFileSync('./json/daily/'+logiptv_fn, logiptv);
      
        
        let obj_satalite = require('./json/daily/'+logsatalite_fn);
        let obj_iptv     = require('./json/daily/'+logiptv_fn);
        
        var tvprogram_basetime_satalitereport_data = await find_channeldelete( "satalite" ,obj_satalite ); // list of top twenty digital tv
        var tvprogram_basetime_iptvreport_data     = await find_channeldelete( "iptv" ,obj_iptv );// list of top twenty digital tv
        
        // let logtop20satalite    = JSON.stringify(tvprogram_basetime_satalitereport_data);
        // var logtop20satalite_ = 'logtop20satalite' + Math.round(+new Date()/1000) + ".json";
        // fs.writeFileSync('./json/daily/'+logtop20satalite_, logtop20satalite);
        let logtop20iiptv    = JSON.stringify(tvprogram_basetime_iptvreport_data);
        var logtop20iiptv_ = 'logtop20iiptv' + Math.round(+new Date()/1000) + ".json";
        fs.writeFileSync('./json/daily/'+logtop20iiptv_, logtop20iiptv);

        let tvprogram_obj          = get_thistvprogram( tvprogram_basetime_satalitereport_data , tvprogram_basetime_iptvreport_data , channels_id); // case : get only tvprogrm of this channel 
       let merge_data_obj         =  merge_data(tvprogram_basetime_satalitereport_data , tvprogram_basetime_iptvreport_data ); // case : merge top20channel satalite and iptv
       
       let logtop20    = JSON.stringify(merge_data_obj);
       var logtop20_fn = 'logtop20' + Math.round(+new Date()/1000) + ".json";
       fs.writeFileSync('./json/daily/'+logtop20_fn, logtop20);
       
       let merge_data_obj_everychannel  =   merge_data(original_tvprogram_basetime_satalitereport_data , original_tvprogram_basetime_iptv_data ); 
      
      

        let sum_allbaseontime = sum_everychannel_baseontime(merge_data_obj_everychannel); //  sum everychannel base on time slot
        let sum_top20         = sum_everychannel_baseontime(merge_data_obj); // sum  top20  base on timeslot
       
        let merge_data_gender_obj  = merge_data_gender(satalite_gender , iptv_gender);

        let logprovincesatalite    = JSON.stringify(province_region_satalite);
        var logprovincesatalite_fn = 'logprovincesatalite_' + Math.round(+new Date()/1000) + ".json";
        fs.writeFileSync('./json/daily/'+logprovincesatalite_fn, logprovincesatalite);
        
        let logprovinceiptv    = JSON.stringify(province_region_iptv);
        var logprovinceiptv_fn = 'logprovinceiptv_' + Math.round(+new Date()/1000) + ".json";
        fs.writeFileSync('./json/daily/'+logprovinceiptv_fn, logprovinceiptv);
      
        
        let obj_provincesatalite = require('./json/daily/'+logprovincesatalite_fn);
        let obj_provinceiptv     = require('./json/daily/'+logprovinceiptv_fn);

        let merge_data_province_obj= merge_data(obj_provincesatalite , obj_provinceiptv, "element.province_region");
      
    
        var excel = require('excel4node');
        // Create a new instance of a Workbook class
        var workbook = new excel.Workbook();

        // Add Worksheets to the workbook
        var worksheet = workbook.addWorksheet('dailyreport');
        // Create a reusable style
        var style = workbook.createStyle({
            font: {
                color: '#FF0800',
                size: 12
            },
            numberFormat: '$#,##0.00; ($#,##0.00); -'
        });
        var text_style_header = workbook.createStyle({
            font: {
                color: '#000000',
                size: 10
            },alignment: { 
                shrinkToFit: true, 
                wrapText: true
            },
            border: {
                left: {
                    style: 'thin',
                    color: 'black',
                },
                right: {
                    style: 'thin',
                    color: 'black',
                },
                top: {
                    style: 'thin',
                    color: 'black',
                },
                bottom: {
                    style: 'thin',
                    color: 'black',
                },
                outline: false,
            },
        });
        var text_style = workbook.createStyle({
            font: {
                color: '#000000',
                size: 10
            },alignment: { 
                shrinkToFit: true, 
                wrapText: true
            },
            border: {
                left: {
                    style: 'thin',
                    color: 'black',
                },
                right: {
                    style: 'thin',
                    color: 'black',
                },
                top: {
                    style: 'thin',
                    color: 'black',
                },
                bottom: {
                    style: 'thin',
                    color: 'black',
                },
                outline: false,
            },
        });

        // Set value of cell A1 to 100 as a number type styled with paramaters of style ( แถว , หลัก )
        worksheet.cell(1, 1).string("ข้อมูลรายวัน"+channels[0].channel_name).style(text_style_header);

        // worksheet.cell(3, 1).string("** เฉลี่ยต่อเวลานำเสนอวัดเฉพาะการเข้าถึงใหม่ ณ นาที นั้น").style(text_style_header);
        worksheet.cell(4, 1).string("MONTH").style(text_style_header);
        worksheet.cell(4, 2).string("WKDAY / WKEND").style(text_style_header);
        worksheet.cell(4, 3).string("DAY NAME").style(text_style_header);
        worksheet.cell(4, 4).string("DAY").style(text_style_header);
        worksheet.cell(4, 5).string("Start").style(text_style_header);
        worksheet.cell(4, 6).string("End").style(text_style_header);
        worksheet.cell(4, 7).string("Description").style(text_style_header);
        worksheet.column(7).setWidth(30);
        worksheet.cell(4, 8).string("ปริมาณการนำเสนอ").style(text_style_header);
        worksheet.cell(4, 9).string("Rank").style(text_style_header);
        worksheet.cell(4, 10).string("เข้าถึงรวม").style(text_style_header);
        worksheet.cell(4, 11).string("PEAK(เวลาที่พีค)").style(text_style_header);
        worksheet.cell(4, 12).string("PEAK(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 13).string("เฉลี่ยต่อเวลานำเสนอ").style(text_style_header);
        worksheet.cell(4, 13).string("เพศชาย(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 15).string("เพศชาย(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 14).string("เพศหญิง(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 17).string("เพศหญิง(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 15).string("ระบุไม่ได้(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 19).string("ระบุไม่ได้(ต่อเวลานำเสนอ)").style(text_style_header);
     
        // ["กลาง" , 0],
        // ["ตะวันออก" , 0],
        // ["ตะวันออกเฉียงเหนือ" , 0],
        // ["เหนือ" , 0],
        // ["ใต้" , 0],
        // ["ตะวันตก"  , 0],
        // ["ระบุไม่ได้"  , 0],
        // ["กรุงเทพ"  , 0]
        worksheet.cell(4, 16).string("พื้นที่กรุงเทพ(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 21).string("พื้นที่กรุงเทพ(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 17).string("พื้นที่ภาคกลาง(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 23).string("พื้นที่ภาคกลาง(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 18).string("พื้นที่ภาคตะวันออก(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 25).string("พื้นที่ภาคตะวันออก(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 19).string("พื้นที่ภาคตะวันออกเฉียงเหนือ(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 27).string("พื้นที่ภาคตะวันออกเฉียงเหนือ(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 20).string("พื้นที่ภาคเหนือ(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 29).string("พื้นที่ภาคเหนือ(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 21).string("พื้นที่ภาคใต้(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 31).string("พื้นที่ภาคใต้(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 22).string("พื้นที่ภาคตะวันตก(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 33).string("พื้นที่ภาคตะวันตก(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 23).string("ระบุไม่ได้(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 35).string("ระบุไม่ได้(ต่อเวลานำเสนอ)").style(text_style_header);

        worksheet.cell(4, 24).string("PSI TOTAL(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 37).string("PSI TOTAL(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 25).string("OTHER TV(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 39).string("OTHER TV(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 26).string("จำนวนคนที่ดู youtube ทั้งหมดในช่วงเวลานี้(จำนวนเครื่อง)").style(text_style_header);
        // worksheet.cell(4, 41).string("จำนวนคนที่ดู youtube ทั้งหมดในช่วงเวลานี้(ต่อเวลานำเสนอ)").style(text_style_header);
        worksheet.cell(4, 27).string("เรียง 20 ช่องดิจิทัล").style(text_style_header);

        // merge_data_obj
        $column_header_channel_start = 28;
        $channel_array = tvdigitalchannel_ondemand("satalite" , "header");
        for (let i = 0; i < $channel_array.length; i++) {
            worksheet.cell(4, $column_header_channel_start).string($channel_array[i]+"(จำนวนเครื่อง)").style(text_style_header);
            ++ $column_header_channel_start;
            // worksheet.cell(4, $column_header_channel_start).string($channel_array[i]+"(ต่อเวลานำเสนอ)").style(text_style_header);
            // ++ $column_header_channel_start;
        }
        // tvprogram_recordset_obj
        $row = 5;
      
        tvprogram_recordset_obj.forEach(element => {
            element.forEach(value => {
               
      
                 // declare variable : day name
                var dayname = get_dayname( value.date );
                 // case :   set month short name
                var shortmonthname = get_monthshortname( value.date);
                worksheet.cell($row, 1).string( shortmonthname ).style(text_style);
                // case :   set weekend weekday 
                var wkendwkday = "";
                if(dayname == "Sat" || dayname == "Sun"){
                    wkendwkday = "WKEND";
                }else{
                    wkendwkday = "WKDAY";
                }
                worksheet.cell($row, 2).string( wkendwkday ).style(text_style);
                // case :   set day name
                worksheet.cell($row, 3).string( dayname ).style(text_style);
                // case :   set date
                var dt = new Date(value.date);
                var date = dt.getDate() +  "/"  + (dt.getMonth() + 1) +  "/"  + dt.getFullYear();
                worksheet.cell($row, 4).string( date ).style(text_style);
             
                // case :   set start
                
                var start =set_timezone(value.start_time, "Asia/Jakarta");
              
                var st_h  = start.getUTCHours();
                st_h =  st_h < 10 ? "0"+st_h : st_h;
                var st_m  = start.getUTCMinutes();
                st_m =  st_m < 10 ? "0"+st_m : st_m;

                start =  st_h + ":" + st_m;
               
                worksheet.cell($row, 5).string( start ).style(text_style);
                // case :   set end
                var end =set_timezone(value.end_time, "Asia/Jakarta");                
                var end_h  = end.getUTCHours();
                end_h =  end_h < 10 ? "0"+end_h : end_h;
                var end_m  = end.getUTCMinutes();
                end_m =  end_m < 10 ? "0"+end_m : end_m;
                end =  end_h + ":" + end_m;
                worksheet.cell($row, 6).string( end ).style(text_style);
                // case :   set description
                var program_name = value.name;
                worksheet.cell($row, 7).string( program_name ).style(text_style);
                // case :   ปริมาณนำเสนอ
       
                var time_diff        = find_timediff(value.end_time , value.start_time);
               
                var value_minutediff = value.minute_diff + "นาที";
                worksheet.cell($row, 8).string( time_diff ).style(text_style);
                // case :   rank
                var objkey_value =  `tvprogram_obj.field_${value.id}`;
                var no = set_no(tvprogram_obj , value);
                let order_no    =  get_orderno(value , merge_data_obj , channels_id);
                worksheet.cell($row, 9 ).number( order_no ).style(text_style);
                // case : เข้าถึงรวม
             
                worksheet.cell($row, 10 ).number( eval(objkey_value) ).style(text_style);
                
                // case :  peak
                var peak   =  `maxObj.field_${value.id}`;
                var maxObj =   get_peak(value , arr_data);
                worksheet.cell($row, 11 ).string( maxObj.hour +":"+maxObj.minute).style(text_style);
                worksheet.cell($row, 12 ).number( eval(peak)).style(text_style);
                
                // case : average
                $field =`field_${value.id}`;
                var avg = get_avg( value , arr_data , $field );
                // worksheet.cell($row, 13 ).number( avg).style(text_style); // เฉลี่ยต่อเวลานำเสนอ
                
                // case :  เพศ
                $total_viewer = eval(objkey_value);
                $gender      = get_totalgender(merge_data_gender_obj , $field);
                $gender_male   = $gender.male > 0 ? $gender.male : 0;
                $gender_female = $gender.female > 0 ? $gender.female : 0;
                $gender_none   = $gender.none > 0 ? $gender.none : 0;

                let _minutediff = value.minute_diff > 0 ?  value.minute_diff  : 0;
                
                $avg_male             = ( 100 * $gender_male ) / $total_viewer;
                $avg_female           = ( 100 * $gender_female ) / $total_viewer;
                $avg_none             = ( 100 * $gender_none ) / $total_viewer;
               
                let $avg_male_comparetime;
                let $avg_female_comparetime;
                let $avg_none_comparetime;
                
                if(_minutediff > 0){
                  
                    
                    // $avg_male_comparetime = convertHMS($avg_male_comparetime);
                    $avg_male_comparetime = $gender_male > 0 ? $gender_male / _minutediff : 0;
                    $avg_male_comparetime = round_xdecimalplace($avg_male_comparetime , 2);
                    
                  
                    $avg_female_comparetime = $gender_female > 0 ? $gender_female / _minutediff : 0;
                    $avg_female_comparetime = round_xdecimalplace($avg_female_comparetime , 2);

                   
                    $avg_none_comparetime = $gender_none > 0 ? $gender_none / _minutediff : 0;
                    $avg_none_comparetime = round_xdecimalplace($avg_none_comparetime , 2);
                }else{
                    $avg_male_comparetime =  0;$avg_female_comparetime =  0;$avg_none_comparetime =  0;
                }
                worksheet.cell($row, 13 ).number( $gender_male ).style(text_style);
                // worksheet.cell($row, 15 ).number( $avg_male_comparetime).style(text_style);
                worksheet.cell($row, 14 ).number( $gender_female).style(text_style);
                // worksheet.cell($row, 17 ).number( $avg_female_comparetime).style(text_style);
                worksheet.cell($row, 15 ).number( $gender_none).style(text_style);
                // worksheet.cell($row, 19 ).number( $avg_none_comparetime).style(text_style);
                // case :  ภาค
                $region      = get_totalregion(merge_data_province_obj , $field );
              
                $region_text = "";
                $region_bk = "";
                $region_ct = ""; // กลาง
                $region_e = ""; // ตะวันออก
                $region_nt = ""; // ตะวันออกเฉียงเหนือ
                $region_n = ""; // เหนือ
               
                $region_s = ""; // ใต้
                $region_w = ""; // ตะวันตก
                $region_none= ""; // ระบุไม่ได้
                if($region.length > 0){
                    $region_bk  = $region[7][1];
                    $region_ct  = $region[0][1];
                    $region_e   = $region[1][1];
                    $region_nt  = $region[2][1];
                    $region_n  = $region[3][1];
                    $region_s  = $region[4][1];
                    $region_w  = $region[5][1];
                    $region_none  = $region[6][1];

                    // $avg_bk            = get_avgregion($region_bk , _minutediff , $total_viewer);
                    // $avg_ct            = get_avgregion($region_ct , _minutediff , $total_viewer);
                    // $avg_e             = get_avgregion($region_e , _minutediff , $total_viewer);
                    // $avg_nt            = get_avgregion($region_nt , _minutediff , $total_viewer);
                    // $avg_n             = get_avgregion($region_n , _minutediff , $total_viewer);
                    // $avg_s             = get_avgregion($region_s , _minutediff , $total_viewer);
                    // $avg_w             = get_avgregion($region_w , _minutediff , $total_viewer);
                    // $avg_none          = get_avgregion($region_none , _minutediff , $total_viewer);
                    $avg_bk            = $region_bk > 0 ? $region_bk / _minutediff : 0;
                    $avg_ct            = $region_ct > 0 ? $region_ct / _minutediff : 0;
                    $avg_e             = $region_e > 0 ? $region_e / _minutediff : 0;
                    $avg_nt            = $region_nt > 0 ? $region_nt / _minutediff : 0;
                    $avg_n             = $region_n > 0 ? $region_n / _minutediff : 0;
                    $avg_s             = $region_s > 0 ? $region_s / _minutediff : 0;
                    $avg_w             = $region_w > 0 ? $region_w / _minutediff : 0;
                    $avg_none          = $region_none > 0 ? $region_none / _minutediff : 0;
                    
                 
                    worksheet.cell($row, 16 ).number( $region_bk).style(text_style);
                    // worksheet.cell($row, 21 ).number( round_xdecimalplace($avg_bk , 2) ).style(text_style);

                    worksheet.cell($row, 17 ).number( $region_ct).style(text_style);
                    // worksheet.cell($row, 23 ).number( round_xdecimalplace($avg_ct , 2) ).style(text_style);

                    worksheet.cell($row, 18 ).number( $region_e).style(text_style);
                    // worksheet.cell($row, 25 ).number( round_xdecimalplace($avg_e , 2)).style(text_style);

                    worksheet.cell($row, 19 ).number( $region_nt).style(text_style);
                    // worksheet.cell($row, 27 ).number( round_xdecimalplace($avg_nt, 2) ).style(text_style);

                    worksheet.cell($row, 20 ).number( $region_n).style(text_style);
                    // worksheet.cell($row, 29 ).number( round_xdecimalplace($avg_n, 2)).style(text_style);

                    worksheet.cell($row, 21 ).number( $region_s).style(text_style);
                    // worksheet.cell($row, 31 ).number( round_xdecimalplace($avg_s , 2)).style(text_style);

                    worksheet.cell($row, 22 ).number( $region_w).style(text_style);
                    // worksheet.cell($row, 33 ).number( round_xdecimalplace($avg_w , 2)).style(text_style);

                    worksheet.cell($row, 23 ).number( $region_none).style(text_style);
                    // worksheet.cell($row, 35 ).number( round_xdecimalplace($avg_none , 2)).style(text_style);
                }
               
                
               
                
                // case : PSI TOTAL
                let $key_psitotal      =  get_keybyvalue( tvprogram_basetime_satalitereport_data , channels_id);
                // console.log("channel id :  " +  channels_id +  " key : " + $key_psitotal);
                // console.log(tvprogram_basetime_satalitereport_data[$key_psitotal]);
                let $psitotal          = `sum_allbaseontime['${$field}']`;
                $psitotal = eval($psitotal);
                worksheet.cell($row, 24 ).number( $psitotal  ).style(text_style);
                let avg_psitotal = $psitotal / _minutediff;
                // worksheet.cell($row, 37 ).number( round_xdecimalplace(avg_psitotal , 2)  ).style(text_style);
                // case : OTHER TV 
               // let $key_iptv      =  get_keybyvalue( tvprogram_basetime_iptvreport_data , channels_id);
                let tvchannel_id =  tvprogram_basetime_satalitereport_data[$key_psitotal].merge_s3remote_id;
                var $sumtop_20 = `sum_top20['${$field}']`;
                $sumtop_20     =  eval($sumtop_20);
                let $other_tv  = parseInt($psitotal) - parseInt($sumtop_20);

                worksheet.cell($row, 25 ).number( $other_tv  ).style(text_style);
                let avg_other_tv = $other_tv / _minutediff;
                // worksheet.cell($row, 39 ).number( round_xdecimalplace(avg_other_tv , 2)  ).style(text_style);

                // worksheet.cell($row, 38 ).string( ";"  ).style(text_style);
                
                // case : ONLINE (YOUTUBE)
                let yt_total = 0;
                if(tvprogram_basetime_ytreport_data != null){
                    yt_total = set_sum( tvprogram_basetime_ytreport_data ,$field);
                    yt_total = parseInt(yt_total) > 0  ? parseInt(yt_total) : 0;
                    
                }
                worksheet.cell($row, 26 ).number( yt_total  ).style(text_style);
                let avg_yt = yt_total / _minutediff;
                // worksheet.cell($row, 41 ).number( round_xdecimalplace(avg_yt , 2)  ).style(text_style);
                // worksheet.cell($row, 40 ).string( ";"  ).style(text_style);
                // case : เรียง 20 ช่องดิจิทัล
                $channel_array_content = tvdigitalchannel_ondemand("satalite" , null); // # find top20 channel from satalite channel id
                $diff_fromothertv      = $psitotal - $other_tv;
                $col_start  = 28;
                 for($x =0;$x < $channel_array_content.length; $x ++){
                    
                     $channel_id_top20 = $channel_array_content[$x];
                  
                    let $key_found     =  get_keybyvalue( merge_data_obj , $channel_id_top20);
                    //console.log("channel_id:"+$channel_id_top20+ " key:" + $key_found);
                    if($key_found >= 0 ){
                       
                        var $v =  `merge_data_obj[${$key_found}].field_${value.id}`;
                        $v     = eval($v);
                        $v     = $v > 0  ? $v : 0;
                       // $get_avgtop20  = get_avgtop20( $v , _minutediff , $diff_fromothertv);
                       $get_avgtop20 = $v > 0 ? $v /  _minutediff  : 0;
                       // merge_data_obj[$key_found]
                        worksheet.cell($row, $col_start ).number( $v).style(text_style);
                        ++ $col_start;
                        // worksheet.cell($row, $col_start ).number( round_xdecimalplace($get_avgtop20 , 2)  ).style(text_style);
                        // ++ $col_start;
                    }else{
                        worksheet.cell($row, $col_start ).number( 0  ).style(text_style);
                        ++ $col_start;
                        // worksheet.cell($row, $col_start ).number( 0  ).style(text_style);
                        // ++ $col_start;
                    }
                  
                 }
                // worksheet.cell($row, 27 ).number( order_no  ).style(text_style);
                ++ $row;
            });
            
           

           
        });

        let $filename  = 'dailyreport_'+seconds_since_epoch(new Date()) +'.xlsx';
        workbook.write('./excel/daily/' + $filename);
        return $filename;
    }
    function convertHMS(value) {
        const sec = parseInt(value, 10); // convert value to number if it's string
        let hours   = Math.floor(sec / 3600); // get hours
        let minutes = Math.floor((sec - (hours * 3600)) / 60); // get minutes
        let seconds = sec - (hours * 3600) - (minutes * 60); //  get seconds
        // add 0 if value < 10; Example: 2 => 02
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds+""; // Return is HH : MM : SS
    }

    function get_avgregion($region ,  _minutediff , $total_viewer){
        let $avg_comparetime;
        $avg             = ( 100 * $region ) / $total_viewer;
        if($avg > 0){
            $avg_comparetime =  ( $avg * _minutediff ) /  100;
            $avg_comparetime =  round_xdecimalplace($avg_comparetime );

            $avg_comparetime = convertHMS( $avg_comparetime );
        }else{
            $avg_comparetime = 0;
        }

        return $avg_comparetime;
    }

    function get_avgtop20($top20 ,  _minutediff , $total_viewer){
        let $avg_top20;
        $avg             = ( 100 * $top20 ) / $total_viewer;
        if($avg > 0){
            $avg_top20 =  ( $avg * _minutediff ) /  100;
            $avg_top20 =  round_xdecimalplace($avg_top20 );

            $avg_top20 = convertHMS( $avg_top20 );
        }else{
            $avg_top20 = 0;
        }

        return $avg_top20;
    }

    function round_xdecimalplace(value, precision) {
  

      var multiplier = Math.pow(10, precision || 0);
      return Math.round(value * multiplier) / multiplier;
        // if(precision == 3){
        //     return Math.round((value + Number.EPSILON) * 1000) / 1000
        // }
        // let val = value > 0 ? parseFloat(value) : 0;
        // return val.toFixed(2);
    }
    function find_timediff(date_future , date_now){
        let date_future_ = new Date(date_future);
        let date_now_    = new Date(date_now);
        
        var seconds = Math.floor((date_future_ - (date_now_))/1000);
        var minutes = Math.floor(seconds/60);
        var hours = Math.floor(minutes/60);
        var days = Math.floor(hours/24);
        
        hours = hours-(days*24);
        minutes = minutes-(days*24*60)-(hours*60);
        seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60);

        hours =  hours < 10 ? "0"+hours : hours;
        minutes =  minutes < 10 ? "0"+minutes : minutes;
        seconds =  seconds < 10 ? "0"+seconds : seconds;

            return hours + ":" + minutes + ":"+seconds;
    }

    function get_totalgender(merge_data_gender_obj = null, $field = null){
       
        let $key_identify_gender_null     =  get_keybyvalue_gender( merge_data_gender_obj , 'none');
        let $key_identify_gender_empty    =  get_keybyvalue_gender( merge_data_gender_obj , 'none1');
        let $key_identify_gender_dat      =  get_keybyvalue_gender( merge_data_gender_obj , 'none2');
        let $key_identify_gender_female   =  get_keybyvalue_gender( merge_data_gender_obj , 'Female');
        let $key_identify_gender_male     =  get_keybyvalue_gender( merge_data_gender_obj , 'Male');
        $gender_none  = 0;
        $gender_female= 0;
        $gender_male  = 0;
     
        if($key_identify_gender_null != null){
            let $gender_null          = `merge_data_gender_obj[${$key_identify_gender_null}].${$field}`;
            $gender_none  +=  eval($gender_null);
        }
        if($key_identify_gender_empty != null){
            let $gender_empty         = `merge_data_gender_obj[${$key_identify_gender_empty}].${$field}`;
          
            $gender_none  +=  eval($gender_empty);
        }
        if($key_identify_gender_dat != null){
            let $gender_dat         = `merge_data_gender_obj[${$key_identify_gender_dat}].${$field}`;
            $gender_none  +=  eval($gender_dat);
        }
        if($key_identify_gender_female != null){
            let $gender_female_obj        = `merge_data_gender_obj[${$key_identify_gender_female}].${$field}`;
            $gender_female  +=  eval($gender_female_obj);
        }
        if($key_identify_gender_male != null){
            let $gender_male_obj        = `merge_data_gender_obj[${$key_identify_gender_male}].${$field}`;
            $gender_male  +=  eval($gender_male_obj);
        }
        
        return {'male':$gender_male, 'female' : $gender_female ,'none':$gender_none}

    }
    function get_totalregion(merge_data_province_obj , $field ){
       
        let $key_identify_region_null     =  get_keybyvalue_region( merge_data_province_obj , 'none');
        let $key_identify_region_empty    =  get_keybyvalue_region( merge_data_province_obj , 'none1');
        let $key_identify_region_dat      =  get_keybyvalue_region( merge_data_province_obj , 'none2');
        let $key_identify_region_bk       =  get_keybyvalue_region( merge_data_province_obj , 'Bangkok');
        let $key_identify_region_ct       =  get_keybyvalue_region( merge_data_province_obj , 'Central');
        let $key_identify_region_et       =  get_keybyvalue_region( merge_data_province_obj , 'Eastern');
        let $key_identify_region_ne       =  get_keybyvalue_region( merge_data_province_obj , 'NorthEastern');
        let $key_identify_region_nt       =  get_keybyvalue_region( merge_data_province_obj , 'Northern');
        let $key_identify_region_st       =  get_keybyvalue_region( merge_data_province_obj , 'Southern');
        let $key_identify_region_wt       =  get_keybyvalue_region( merge_data_province_obj , 'Western');
        
        $region_none  = 0;
        // $region_arr = [
        //     ["กลาง" , 0],
        //     ["ตะวันออก" , 0],
        //     ["ตะวันออกเฉียงเหนือ" , 0],
        //     ["เหนือ" , 0],
        //     ["ใต้" , 0],
        //     ["ตะวันตก"  , 0],
        //     ["ระบุไม่ได้"  , 0]
        // ];
        $region_arr = [
            ["กลาง" , 0],
            ["ตะวันออก" , 0],
            ["ตะวันออกเฉียงเหนือ" , 0],
            ["เหนือ" , 0],
            ["ใต้" , 0],
            ["ตะวันตก"  , 0],
            ["ระบุไม่ได้"  , 0],
            ["กรุงเทพ"  , 0]
        ];

       
        if($key_identify_region_null != null){
            let $region_null          = `merge_data_province_obj[${$key_identify_region_null}].${$field}`;
            $region_null     =  eval($region_null); 
             $region_arr[6][1] +=  parseInt($region_null)// undefined
            
            //$region_arr[0][1] +=  parseInt($region_null)// undefined
        }
        if($key_identify_region_empty != null){
            let $region_empty         = `merge_data_province_obj[${$key_identify_region_empty}].${$field}`;
          
            $region_empty     =  eval($region_empty); 
            $region_arr[6][1] +=  parseInt($region_empty)// undefined
            //$region_arr[0][1] +=  parseInt($region_empty)// undefined
        }
        if($key_identify_region_dat != null){
            let $region_dat         = `merge_data_province_obj[${$key_identify_region_dat}].${$field}`;
        
            $region_dat     =  eval($region_dat); 
            $region_arr[6][1] +=  parseInt($region_dat)// undefined
            //$region_arr[0][1] +=  parseInt($region_dat)// undefined
        }
        if($key_identify_region_bk != null){
            let $region_bk         = `merge_data_province_obj[${$key_identify_region_bk}].${$field}`;
            //$region_arr[0][1] =  eval($region_bk);  // Central
            $region_bk       =  eval($region_bk); 
            $region_arr[7][1] +=  parseInt($region_bk)// Bangkok
        }
        if($key_identify_region_ct != null){
            let $region_ct         = `merge_data_province_obj[${$key_identify_region_ct}].${$field}`;
            $region_ct       =  eval($region_ct); 
            $region_arr[0][1] +=  parseInt($region_ct)// Central
        }

        if($key_identify_region_et != null){
            let $region_et         = `merge_data_province_obj[${$key_identify_region_et}].${$field}`;
            //$region_arr[1][1] =  eval($region_et);  // eastern

            $region_et       =  eval($region_et); 
            $region_arr[1][1] +=  parseInt($region_et)// eastern
        }
        if($key_identify_region_ne != null){
            let $region_ne         = `merge_data_province_obj[${$key_identify_region_ne}].${$field}`;

            $region_ne       =  eval($region_ne); 
            $region_arr[2][1] +=  parseInt($region_ne)// NorthEastern
        }
        if($key_identify_region_nt != null){
            let $region_nt         = `merge_data_province_obj[${$key_identify_region_nt}].${$field}`;

            $region_nt       =  eval($region_nt); 
            $region_arr[3][1] +=  parseInt($region_nt)// nt
        }
        if($key_identify_region_st != null){
            let $region_st         = `merge_data_province_obj[${$key_identify_region_st}].${$field}`;
            $region_st       =  eval($region_st); 
            $region_arr[4][1] +=  parseInt($region_st)// st
        }
        if($key_identify_region_wt != null){
            let $region_wt         = `merge_data_province_obj[${$key_identify_region_wt}].${$field}`;
            $region_wt       =  eval($region_wt); 
            $region_arr[5][1] +=  parseInt($region_wt)// wt
        }
        
      
        
        return $region_arr;

    }
    function get_iptvtotal(tvchannel_id , tvprogram_basetime_iptvreport_data , $field){
        let $iptv_total;
        
        if(typeof tvchannel_id == 'number'){
        
            let $key_iptv = get_keybyvalue( tvprogram_basetime_iptvreport_data ,tvchannel_id  , "iptv");
            $iptv_total = `tvprogram_basetime_iptvreport_data[${$key_iptv}].${$field}`;
            $iptv_total = eval($iptv_total);
            $iptv_total = parseInt($iptv_total);
         
        }else{
           
            $iptv_total = 0;
        }

        return $iptv_total;
    }
    function get_sum(arr , key){
        var mk = "obj." + key;
      
        var sum = arr.reduce((total, obj) => eval(mk) + total,0);
        return sum;
        //return arr.reduce((accumulator, current) => accumulator + Number(eval(mk)), 0)
    }


    function get_avg(value , arr_data , $field){
        var sum = set_avg( arr_data , $field);
        var avg = sum > 0 ? (sum / value.minute_diff) : 0;
        avg = avg > 0  ? Math.round(avg) : 0;

        return avg;
    }
    function get_peak(value , arr_data){
        var max_field_compare =  `max.field_${value.id}`;
        var obj_field_compare =  `obj.field_${value.id}`;
        let maxObj = arr_data.reduce((max, obj) => (eval(max_field_compare) > eval(obj_field_compare)) ? max : obj);
       

        return maxObj;
    }
    function get_orderno(value , merge_data_obj ,channels_id){

        var var_merge_b = `b.field_${value.id}`;
        var var_merge_a = `a.field_${value.id}`;
        let sort_desc   =  merge_data_obj.sort((a, b) => eval(var_merge_b) - eval(var_merge_a));
        let order_no    =  get_keybyvalue( sort_desc , channels_id);
        order_no = parseInt(order_no) > 0 ? parseInt(order_no) + 1 : 0;

        return order_no;
    }

     function set_avg(arr , key){
        var mk = "obj." + key;
    
        var sum = arr.reduce((total, obj) => eval(mk) + total,0);
        return sum;
        //return arr.reduce((accumulator, current) => accumulator + Number(eval(mk)), 0)
    }

    function set_sum(arr , key){
        var mk = "obj." + key;
    
        var sum = arr.reduce((total, obj) => eval(mk) + total,0);
        return sum;
        //return arr.reduce((accumulator, current) => accumulator + Number(eval(mk)), 0)
    }
    function set_no(tvprogram_obj , value){
        objkey_number = 1;
        for(property in tvprogram_obj){
    
            if(property == "field_"+value.id){
                no = objkey_number;
            }
            ++ objkey_number;
        }

        return no;
    }
    function set_timezone(date , tzString){
        return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-us", {
            timeZone: tzString
        }));
          
          
    }

   
    function get_dayname(dateString){
        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var d = new Date(dateString);
        var dayName = days[d.getDay()];
        return dayName;
    }

    function get_monthshortname(d){
        const month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        var d =  new Date(d);
        let name = month[d.getMonth()];
        return name;
    }
    function get_tvprogram(channels_id , date){
        $query = `select  DATEDIFF(MINUTE, start_time , end_time) as minute_diff,* from channel_programs_baseon_date where channels_id =${channels_id} and date = '${date}'and active = 1 order by start_time asc `;
  
        return $query;
    }
    function seconds_since_epoch(d){ 
        return Math.floor( d / 1000 ); 
    }
    function get_dateobject(date, type) {
        var data;
        var dateobject = new Date(date);
        if (type == "month") {
            data = dateobject.getUTCMonth() + 1; //months from 1-12
        } else if (type == "day") {
            data = dateobject.getUTCDate();
        } else {
            data = dateobject.getUTCFullYear();
        }

        return data;
    }





    module.exports = {
        get_provinceviewer: get_provinceviewer,
        get_deviceviewerbyprovince: get_deviceviewerbyprovince,
        get_deviceviewerbehaviour: get_deviceviewerbehaviour,
        get_deviceviewer_perminute: get_deviceviewer_perminute,
        get_deviceviewer_report:get_deviceviewer_report,
        get_overview_report:get_overview_report

    }

           
 