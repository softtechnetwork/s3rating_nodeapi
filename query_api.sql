SELECT  channels.id                                                                                                        AS channels_id
       ,merge_s3remote_id
       ,r1.hour                                                                                                            AS hour
       ,r1.MINUTE                                                                                                          AS minute
       ,CONVERT(varchar(12),merge_s3remote_id) + '_'+(CONVERT(varchar(12),r1.hour) + '_' + CONVERT(varchar(12),r1.MINUTE)) AS h_m
       ,COUNT(case WHEN r1.d_22_0 >= 1 THEN 1 end)                                                                         AS 'field_22_0'
       ,COUNT(case WHEN r1.d_22_1 >= 1 THEN 1 end)                                                                         AS 'field_22_1'
       ,COUNT(case WHEN r1.d_22_2 >= 1 THEN 1 end)                                                                         AS 'field_22_2'
       ,COUNT(case WHEN r1.d_22_3 >= 1 THEN 1 end)                                                                         AS 'field_22_3'
       ,COUNT(case WHEN r1.d_22_4 >= 1 THEN 1 end)                                                                         AS 'field_22_4'
       ,COUNT(case WHEN r1.d_22_5 >= 1 THEN 1 end)                                                                         AS 'field_22_5'
       ,COUNT(case WHEN r1.d_22_6 >= 1 THEN 1 end)                                                                         AS 'field_22_6'
       ,COUNT(case WHEN r1.d_22_7 >= 1 THEN 1 end)                                                                         AS 'field_22_7'
FROM
(
	SELECT  DATEPART(HOUR,startview_datetime)                                                                                      AS hour
	       ,CAST(DATEPART(MINUTE,startview_datetime)/1 AS char(2))                                                                 AS MINUTE
	       ,devices_id
	       ,COUNT(*)                                                                                                               AS count_allratingrecord
	       ,channels_id
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 22:00:00' AND startview_datetime < '2022-06-02 22:01:00' THEN 1 END ) AS 'd_22_0'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 22:01:00' AND startview_datetime < '2022-06-02 22:02:00' THEN 1 END ) AS 'd_22_1'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 22:02:00' AND startview_datetime < '2022-06-02 22:03:00' THEN 1 END ) AS 'd_22_2'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 22:03:00' AND startview_datetime < '2022-06-02 22:04:00' THEN 1 END ) AS 'd_22_3'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 22:04:00' AND startview_datetime < '2022-06-02 22:05:00' THEN 1 END ) AS 'd_22_4'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 22:05:00' AND startview_datetime < '2022-06-02 22:06:00' THEN 1 END ) AS 'd_22_5'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 22:06:00' AND startview_datetime < '2022-06-02 22:07:00' THEN 1 END ) AS 'd_22_6'
	       ,COUNT(CASE WHEN startview_datetime > '2022-06-02 22:07:00' AND startview_datetime < '2022-06-02 22:08:00' THEN 1 END ) AS 'd_22_7'
	FROM rating_data_2022_6
	WHERE startview_datetime >= '2022-06-02 22:00:00'
	AND startview_datetime <= '2022-06-02 22:08:00'
	AND channels_id > 0
	AND channels_id = 257
	GROUP BY  channels_id
	         ,DATEPART(YEAR,startview_datetime)
	         ,DATEPART(MONTH,startview_datetime)
	         ,DATEPART(DAY,startview_datetime)
	         ,DATEPART(HOUR,startview_datetime)
	         ,DATEPART(MINUTE,startview_datetime)/1
	         ,devices_id
)r1
INNER JOIN channels
ON r1.channels_id = channels.id
WHERE channels.active = 1
GROUP BY  r1.hour
         ,r1.MINUTE
         ,channels.id
         ,channels.merge_s3remote_id
ORDER BY r1.hour
         ,r1.MINUTE